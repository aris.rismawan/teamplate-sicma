-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: localhost    Database: fms
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `databasechangelog`
--

DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangelog`
--

LOCK TABLES `databasechangelog` WRITE;
/*!40000 ALTER TABLE `databasechangelog` DISABLE KEYS */;
INSERT INTO `databasechangelog` VALUES ('SICMA-16','malatasf','db/migration/000.xml','2024-11-14 03:48:46',1,'EXECUTED','8:374c3ee74ed18e52a98b10b534c42690','createTable tableName=tbrole','Create Role Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-27','hendra','db/migration/000.xml','2024-11-14 03:48:46',2,'EXECUTED','8:184c18c0f94929cbd9339f49a1c90a0a','createTable tableName=tbuser','Create User Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-32','malatasf','db/migration/000.xml','2024-11-14 03:48:46',3,'EXECUTED','8:e6f8a50242c18b5d31c4d1288c3c9456','insert tableName=tbuser','Add Default User admin/password',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-19','hendra','db/migration/000.xml','2024-11-14 03:48:46',4,'EXECUTED','8:a8d059e7815d2662c85d0f72f8a869ea','createTable tableName=tbuserrole','Create UserRole Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-21','hendra','db/migration/000.xml','2024-11-14 03:48:46',5,'EXECUTED','8:64ab25582ac18f8e31f5806cf28ebe09','createTable tableName=tbmenu','Create Menu Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-51','hendra','db/migration/000.xml','2024-11-14 03:48:46',6,'EXECUTED','8:a00369fa6c8dc00e21696bdee0c22110','createTable tableName=tbtop','Create Top Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-55','hendra','db/migration/000.xml','2024-11-14 03:48:46',7,'EXECUTED','8:23cc351303c6e36fbb754285d101a132','createTable tableName=tbcustomertype','Create Top Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-59','hendra','db/migration/000.xml','2024-11-14 03:48:46',8,'EXECUTED','8:c684dbd76483f7a2a02d7de0cd31b91a','createTable tableName=tbmasterunit','Create Unit Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-44','hendra','db/migration/000.xml','2024-11-14 03:48:46',9,'EXECUTED','8:220e67b0de8d73c1b5e5c054282956b8','createTable tableName=tbcustomer','Create Customer Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-46','hendra','db/migration/000.xml','2024-11-14 03:48:46',10,'EXECUTED','8:138880c6114ca6c7313e92f2688b560d','createTable tableName=tbpic; dropColumn tableName=tbcustomer','Create Pic Database\nDrop columns from tbcustomer',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-84','hendra','db/migration/000.xml','2024-11-14 03:48:46',11,'EXECUTED','8:af88cee740a953d12274fc12d8d4dd2e','createTable tableName=tbpaymenttype','Create Payment Type Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-62','malatasf','db/migration/000.xml','2024-11-14 03:48:46',12,'EXECUTED','8:e18d5fd4973d958f02911ca2fbd9e8c3','createTable tableName=tbproduct','Create Product Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-31','malatasf','db/migration/000.xml','2024-11-14 03:48:46',13,'EXECUTED','8:a4b93c3029bc28208752c20c9d81321e','createTable tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop','System Properties Table',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-81','hendra','db/migration/000.xml','2024-11-14 03:48:46',14,'EXECUTED','8:879181b4556402272b601e3fc8f70d17','createTable tableName=tbinvoice; createTable tableName=tbinvoiceitem','Create Invoice Database\nCreate InvoiceItem Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-20','malatasf','db/migration/000.xml','2024-11-14 03:48:47',15,'EXECUTED','8:0efdb7b0fd6a9424478855401dbbd60e','createTable tableName=tbpermission; addPrimaryKey constraintName=pk-tbpermission, tableName=tbpermission; createTable tableName=tbrolepermission; loadData tableName=tbpermission','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-83','hendra','db/migration/000.xml','2024-11-14 03:48:47',16,'EXECUTED','8:1935cacd63b1f5e7242cdf4aa11ccad6','createTable tableName=tbinvoicepayment','Create InvoicePayment Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-103','hendra','db/migration/000.xml','2024-11-14 03:48:47',17,'EXECUTED','8:591e32577b414d898e45e186f0e3d486','createTable tableName=tbwarehouse','Create Warehouse Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-107','hendra','db/migration/000.xml','2024-11-14 03:48:47',18,'EXECUTED','8:28e61955a51565e618bf73898287ea00','renameTable newTableName=tbwarehousefacility, oldTableName=tbwarehouse','Rename Warehouse Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-22','malatasf','db/migration/000.xml','2024-11-14 03:48:47',19,'EXECUTED','8:4bf150b65be2be5e77ccf7850835673f','delete tableName=tbmenu; addColumn tableName=tbmenu; loadData tableName=tbmenu','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-109','hendra','db/migration/000.xml','2024-11-14 03:48:47',20,'EXECUTED','8:d1dd5a5dc869a37d32fc9af0e1a4031a','createTable tableName=tbwarehouselocation','Create Warehouse Location Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-97','hendra','db/migration/000.xml','2024-11-14 03:48:47',21,'EXECUTED','8:63d0bac1e8ab9a83903f11a4ca7a6ffd','insert tableName=tbsysprop','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-115','malatasf','db/migration/000.xml','2024-11-14 03:48:47',22,'EXECUTED','8:4fa694c4bbe3d7e40b9cceb5db120093','loadUpdateData tableName=tbpermission; insert tableName=tbrole; sql; sql; sql','Update Admin Permissions',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-133','hendra','db/migration/000.xml','2024-11-14 03:48:47',23,'EXECUTED','8:96d8181273037ccf6f2fa42206e02079','addColumn tableName=tbcustomer; addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-131','hendra','db/migration/000.xml','2024-11-14 03:48:47',24,'EXECUTED','8:4b4de6a7be04f359ed8e93c86a7ef18e','createTable tableName=tbsalepricebook; createTable tableName=tbsalepricebookprice; addColumn tableName=tbcustomer; sql; sql; sql','Create PriceBook Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-132','hendra','db/migration/000.xml','2025-02-17 22:30:36',468,'RERAN','8:749b8003cb7a4f3fb44af619eebab07c','sql','',NULL,'3.10.3',NULL,NULL,'9831436130'),('SICMA-164','hendra','db/migration/000.xml','2024-11-14 03:48:48',26,'EXECUTED','8:8efd8d2f89d530377f1a49b06a2aac7c','insert tableName=tbsysprop; createTable tableName=tbfile','Insert New Property\nCreate File Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-165','hendra','db/migration/000.xml','2024-11-14 03:48:48',27,'EXECUTED','8:6ae15fae295e8b05a2bd7991315a1bb6','createTable tableName=tbcustomerfile','Create Customer File Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-167','hendra','db/migration/000.xml','2024-11-14 03:48:48',28,'EXECUTED','8:c53c9e889ad54f14da6700e6d17618b7','createTable tableName=tbproductfile','Create Product File Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-183','hendra','db/migration/000.xml','2024-11-14 03:48:48',29,'EXECUTED','8:ed62f80a82e2a9dde78d3199a7b54fa6','addColumn tableName=tbcustomer','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-178','hendra','db/migration/000.xml','2024-11-14 03:48:48',30,'EXECUTED','8:c910f27efcd5563d8e09ffbdec7649b2','insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-181','hendra','db/migration/000.xml','2024-11-14 03:48:48',31,'EXECUTED','8:5fd75bccc66627a153d143073666004b','sql; sql; sql','Update Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-170','hendra','db/migration/000.xml','2024-11-14 03:48:48',32,'EXECUTED','8:13cbc21289bfdca08ff13dd11e668765','addColumn tableName=tbproduct','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-136','hendra','db/migration/000.xml','2024-11-14 03:48:48',33,'EXECUTED','8:2f35506438a501026e88b17ab2057971','createTable tableName=tbinventory','Create Inventory Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-229','hendra','db/migration/000.xml','2024-11-14 03:48:48',34,'EXECUTED','8:f746d7c6b0076e804df66af5344da79f','dropTable tableName=tbinventory; createTable tableName=tbinventoryblock; createTable tableName=tbinventoryallocation; createTable tableName=tbproductquantity; createTable tableName=tbinventorymovement','Drop Table tbinventory\nCreate Table tbinventoryblock\nCreate Table tbinventoryallocation\nCreate Table tbproductquantity\nCreate Table tbinventorymovement',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-194','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:48',35,'EXECUTED','8:89f16ccfc65e8f743e2b7110711f7d0c','modifyDataType columnName=creditlimit, tableName=tbcustomer','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-195','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:48',36,'EXECUTED','8:79d84f9a6ac85586083bd6b474ffeec0','createTable tableName=tbmasterorganisation','Create Organisation Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-215','hendra','db/migration/000.xml','2024-11-14 03:48:48',37,'EXECUTED','8:f86bf5875fe42b5cb78a52975c6e80e4','dropForeignKeyConstraint baseTableName=tbinventorymovement, constraintName=fk-fromallocation; dropForeignKeyConstraint baseTableName=tbinventorymovement, constraintName=fk-toallocation; dropColumn tableName=tbinventorymovement; addColumn tableName...','Modify Table tbinventorymovement',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-218','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:48',38,'EXECUTED','8:85a1c988761efd59d620028f559824d9','addColumn tableName=tbcustomer','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-214','hendra','db/migration/000.xml','2024-11-14 03:48:48',39,'EXECUTED','8:924908d8465bc7f0579a9537b891052f','createTable tableName=tbdispatch; createTable tableName=tbdispatchallocation','Create Table for Dispatch\nCreate Table for DispatchAllocation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-203','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:48',40,'EXECUTED','8:580dc788f6f02413d8da3bca7e4ed226','insert tableName=tbsysprop; insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-217','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:48',41,'EXECUTED','8:8ea7c94b34390c2f8f3df99af44de76d','addColumn tableName=tbmasterorganisation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-227','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',42,'EXECUTED','8:6f5bb2cc31cfb09f9946017cb2d110e7','addColumn tableName=tbwarehousefacility','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-204','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',43,'EXECUTED','8:daf396fc1cc38e392d77219c3047e212','insert tableName=tbtop; insert tableName=tbtop; insert tableName=tbtop; insert tableName=tbtop; addColumn tableName=tbcustomer','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-236','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',44,'EXECUTED','8:2dac1d99139acea733f787ab5993b688','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-243','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',45,'EXECUTED','8:b4c8347d03f3baa229b6a373d2dba703','addColumn tableName=tbinvoice; sql; modifyDataType columnName=efaktur_sequence, tableName=tbmasterorganisation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-234','hendra','db/migration/000.xml','2024-11-14 03:48:49',46,'EXECUTED','8:8743ee14d55688f0a7db2c4d76e3c5a8','addColumn tableName=tbdispatchallocation','Modify Table tbdispatchallocation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-258','hendra','db/migration/000.xml','2024-11-14 03:48:49',47,'EXECUTED','8:69b68816c4ce25232f67e568f43b94c8','addColumn tableName=tbdispatchallocation','Modify Table tbdispatchallocation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-256 for SICMA-254','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',48,'EXECUTED','8:4ef15454926d01edbbbede748987c0d1','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-247','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',49,'EXECUTED','8:34c1308e37aad6f766cb618755d46f48','createTable tableName=tbtruck','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-254','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',50,'EXECUTED','8:f4131f9f8f2ba1d923f55401c3805cfc','modifyDataType columnName=phone, tableName=tbmasterorganisation; modifyDataType columnName=fax, tableName=tbmasterorganisation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-252','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',51,'EXECUTED','8:a3913ab50acea75ad88ba091f515d534','createTable tableName=tbdriver','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-222','hendra','db/migration/000.xml','2024-11-14 03:48:49',52,'EXECUTED','8:c11c7c9338c80667b4ef8ed4749e13a9','createTable tableName=tbcustomerlocation; addColumn tableName=tbinvoice; addColumn tableName=tbdispatch','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-259','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',53,'EXECUTED','8:bc8722896f74728b953b33acfbf205fd','createTable tableName=tbsalesperson','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-264','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:49',54,'EXECUTED','8:4eed90fa3c1260199013efa5656d7786','addColumn tableName=tbproduct','Modify Table tbproduct for import XLS/CSV',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-265','hendra','db/migration/000.xml','2024-11-14 03:48:50',55,'EXECUTED','8:16eaa9c07ae10e7a6c0174fa713a8bf1','addColumn tableName=tbcustomer','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-298','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:50',56,'EXECUTED','8:b6900564e807e9fbe7ae7528d63692ba','createTable tableName=tbprodcategory','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-263','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:50',57,'EXECUTED','8:e1f2c88c73e3a9941224cee86bd4bd10','addColumn tableName=tbcustomer; addColumn tableName=tbsalesperson','Modify Table tbcustomer for import XLS/CSV, and add shortname to salesperson',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-269','hendra','db/migration/000.xml','2024-11-14 03:48:50',58,'EXECUTED','8:2feee0c7a9a583a55c05537c93ddaa93','addColumn tableName=tbinventoryblock','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-291','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:50',59,'EXECUTED','8:7d9be41bb48baea94ae4a8c1d923b231','createTable tableName=tbdeliveryarea','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-271','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:50',60,'EXECUTED','8:206f48c07cdfce4bc2ddf6cade04400e','modifyDataType columnName=product_category, tableName=tbproduct; addForeignKeyConstraint baseTableName=tbproduct, constraintName=fk-product-productcategory, referencedTableName=tbprodcategory','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-303','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:50',61,'EXECUTED','8:d0eae0f4f5e6a6d2cca43ba987e68fbb','insert tableName=tbsysprop','Tax percent in System Properties Table',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-272','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:50',62,'EXECUTED','8:097b75aaf66170bd2c7e23be80a1bb66','addColumn tableName=tbcustomer','enable/disable taxation on customer',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-273','hendra','db/migration/000.xml','2024-11-14 03:48:50',63,'EXECUTED','8:cc036f72375212a025d18fc5a1e892b2','addColumn tableName=tbdispatchallocation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-270','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:50',64,'EXECUTED','8:a030ba49a6e5a31349c7b8114eea276d','addColumn tableName=tbcustomer','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-308','hendra','db/migration/000.xml','2024-11-14 03:48:50',65,'EXECUTED','8:e2f4009113fa22a9ae56708ec591174f','insert tableName=tbsysprop','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-309','hendra','db/migration/000.xml','2024-11-14 03:48:50',66,'EXECUTED','8:eb7622a45237ddf8393247fe9501d7e3','insert tableName=tbsysprop','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-306','hendra','db/migration/000.xml','2024-11-14 03:48:50',67,'EXECUTED','8:c1ef22e568bb10124254ed8c5494d6b5','insert tableName=tbsysprop','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-283','hendra','db/migration/000.xml','2024-11-14 03:48:50',68,'EXECUTED','8:898a5222b71b7dab41eeeb5d06ef96ef','insert tableName=tbsysprop','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-276','hendra','db/migration/000.xml','2024-11-14 03:48:50',69,'EXECUTED','8:d2d97b8c22eb46b97cfc5d7df018ce1e','insert tableName=tbsysprop','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-322','[KSS] Arseno Harahap]','db/migration/000.xml','2024-11-14 03:48:50',70,'EXECUTED','8:a6797b322f97ce8d6f6371b490dc1743','addColumn tableName=tbmasterorganisation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-342','malatasf','db/migration/000.xml','2024-11-14 03:48:51',71,'EXECUTED','8:25a74b280fd7f713fbb851d2c2e207c7','delete tableName=tbmenu; loadData tableName=tbmenu','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-327','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',72,'EXECUTED','8:53755fa4f1c4d693f16d31c9e9b3b754','addColumn tableName=tbinvoicepayment','Add remark in payment',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-333','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',73,'EXECUTED','8:fd4f55666b71f5c1253328d41df469fe','addColumn tableName=tbmasterorganisation','Reporting ID fields in Organisation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-230','hendra','db/migration/000.xml','2024-11-14 03:48:51',74,'EXECUTED','8:01de176ad552036e876a5d1c8584768d','insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermi...','Insert new property\nCreate Return Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-323','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',75,'EXECUTED','8:ab4b664be2a7876d033e54b61a03de8d','addColumn tableName=tbmasterorganisation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-208','malatasf','db/migration/000.xml','2024-11-14 03:48:51',76,'EXECUTED','8:7f1989d1394be5c75fdc66c91c2bbffb','createTable tableName=tbdashboard; createTable tableName=tbdashboardtile; insert tableName=tbpermission; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbr...','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-209','malatasf','db/migration/000.xml','2024-11-14 03:48:51',77,'EXECUTED','8:20b111a3db20f44cd1a53e2546be8d31','insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-210','malatasf','db/migration/000.xml','2024-11-14 03:48:51',78,'EXECUTED','8:13697a963b81d24386ab1c05a0560c60','addColumn tableName=tbuser; renameColumn newColumnName=menu_group, oldColumnName=group, tableName=tbmenu','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-211','malatasf','db/migration/000.xml','2024-11-14 03:48:51',79,'EXECUTED','8:564b61ddd50481013f9c1679e94664d3','createTable tableName=tbdashboardtileconfiguration','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-279','hendra','db/migration/000.xml','2024-11-14 03:48:51',80,'EXECUTED','8:a787d576d6c423b4e6e96d5f7842c84e','createTable tableName=tbrefund; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbro...','Create Refund Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-328','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',81,'EXECUTED','8:9c0017d9b0e8cacad50a67a181925918','addColumn tableName=tbmasterorganisation; addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-350','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',82,'EXECUTED','8:8593304e57cc675fe26e32f464359d27','addColumn tableName=tbwarehousefacility','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-318','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',83,'EXECUTED','8:5c8d11ae15a7b94eb20df379d925e875','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-347','hendra','db/migration/000.xml','2024-11-14 03:48:51',84,'EXECUTED','8:50e7e60da8f7554cbc3e19b3a1dc2478','insert tableName=tbmenu','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-472-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',85,'EXECUTED','8:be765614973365a97c1c18d1da041f50','createTable tableName=tbshipmenttype','New Entity : ShipmentType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-472-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',86,'EXECUTED','8:432e3b590428767446730cccaa252ded','insert tableName=tbshipmenttype; insert tableName=tbshipmenttype','Default values for ShipmentType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-472-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',87,'EXECUTED','8:e57548a33bbecfa16251a313385a9d2f','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for ShipmentType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-472-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',88,'EXECUTED','8:2ca0168b7e92bc6f47b92ed6aa839d6a','insert tableName=tbmenu','Insert navigation to ShipmentType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-396','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',89,'EXECUTED','8:7744ea7370b44ec49ee21407334e7324','renameColumn newColumnName=driver_license_expiry_date, oldColumnName=birth_date, tableName=tbdriver; addColumn tableName=tbdriver','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-397','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',90,'EXECUTED','8:fd773233cb5f80fc857b973bc20b60c9','addColumn tableName=tbtruck','Add KIR and STNK expiry columns in Truck',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-474','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',91,'EXECUTED','8:e75e5fbe2a0f5c633ee3ae0441542b18','delete tableName=tbsysprop; delete tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-390','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',92,'EXECUTED','8:a5006b12c876d9e5b540b8ed2c6fdbb5','addColumn tableName=tbinvoicepayment; dropColumn tableName=tbinvoice','Move payment_report_number to invoicepayment',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-338','malatasf','db/migration/000.xml','2024-11-14 03:48:51',93,'EXECUTED','8:f843d426747d6a9c61927c0c7f695cf5','insert tableName=tbpermission; insert tableName=tbrolepermission','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-348','hendra','db/migration/000.xml','2024-11-14 03:48:51',94,'EXECUTED','8:bdee216d8b868cce4942f1e2c8e52330','insert tableName=tbmenu','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-413','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',95,'EXECUTED','8:85a2502c09e58c5c52da0a09d9f121a5','addColumn tableName=tbsalepricebookprice','pricepoint selectable during invoicing',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-379','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',96,'EXECUTED','8:51cbec36c428e6454c974be71d32a174','insert tableName=tbpermission; insert tableName=tbrolepermission','Permission for Exporting Pricebook',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-371','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:51',97,'EXECUTED','8:fb6d3c1b2a85929879056cfdb5e77b03','addColumn tableName=tbproduct','Audit fields for tbproduct',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-528','[KSS] Arseno Harahap]','db/migration/000.xml','2024-11-14 03:48:52',98,'EXECUTED','8:f1cf394116750219d87233cff536cc37','addColumn tableName=tbcustomer','Default Payment Type Id for customer',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-458','malatasf','db/migration/000.xml','2024-11-14 03:48:52',99,'EXECUTED','8:e8361d5ae0c845a4da0014b6971ddc80','renameColumn newColumnName=duedate, oldColumnName=duedate, tableName=tbinvoice; renameColumn newColumnName=invoicedate, oldColumnName=invoicedate, tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-341','hendra','db/migration/000.xml','2024-11-14 03:48:52',100,'EXECUTED','8:a37c2c47470ba252c8d62d0a58593e60','insert tableName=tbmenu','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-459','malatasf','db/migration/000.xml','2024-11-14 03:48:52',101,'EXECUTED','8:a16fdc09327825e533dfd72b2fed5e9a','renameColumn newColumnName=issue_date, oldColumnName=issue_date, tableName=tbcustomerfile; renameColumn newColumnName=expiration_date, oldColumnName=expiration_date, tableName=tbcustomerfile','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-475','malatasf','db/migration/000.xml','2024-11-14 03:48:52',102,'EXECUTED','8:382d838e90de990d290a22af510e93aa','renameColumn newColumnName=driver_license_expiry_date, oldColumnName=driver_license_expiry_date, tableName=tbdriver','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-478','malatasf','db/migration/000.xml','2024-11-14 03:48:52',103,'EXECUTED','8:28915d6211237b6565fadc47ecd352af','renameColumn newColumnName=number_plate_expiry, oldColumnName=number_plate_expiry, tableName=tbtruck; renameColumn newColumnName=periodic_inspection_expiry, oldColumnName=periodic_inspection_expiry, tableName=tbtruck','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-334','malatasf','db/migration/000.xml','2024-11-14 03:48:52',104,'EXECUTED','8:b1a120020a93cf79578a3c05d5eeae07','addColumn tableName=tbsalepricebook','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-380','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:52',105,'EXECUTED','8:6b66e3cb916a95efa7f88cf5768c5b83','insert tableName=tbpermission; insert tableName=tbrolepermission','Permission for Importing Pricebook',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-372','hendra','db/migration/000.xml','2024-11-14 03:48:52',106,'EXECUTED','8:1b0ecc216415bfde1e0e8baeade76975','addColumn tableName=tbcustomer','Audit fields for tbcustomer',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-373','hendra','db/migration/000.xml','2024-11-14 03:48:52',107,'EXECUTED','8:b8c84bbc82858673c53a1b65f441cba2','addColumn tableName=tbinvoice','Audit fields for tbinvoice',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-374','hendra','db/migration/000.xml','2024-11-14 03:48:52',108,'EXECUTED','8:0ec51a54684a434203aa3e94e9fe3f06','addColumn tableName=tbinvoicepayment','Audit fields for tbinvoicepayment',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-401','hendra','db/migration/000.xml','2024-11-14 03:48:52',109,'EXECUTED','8:476a5bf93ab094b7f3eedf413bf3e49b','addColumn tableName=tbinvoice','add notes for invoice',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-527','hendra','db/migration/000.xml','2024-11-14 03:48:52',110,'EXECUTED','8:a667fe9b38b96b2cc41601b354f81d99','insert tableName=tbsysprop; insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-525','malatasf','db/migration/000.xml','2024-11-14 03:48:52',111,'EXECUTED','8:c1d23588dc0bd1f212856782d48d0c3c','insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbmenu; createTable tableName=tbinvoiceinvoicepayment; sql; dropForeignKeyConstraint baseTableName=tbinvoicepayment, constraintName=fk-invoiceforinvoicepayment; dro...','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-441-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:52',112,'EXECUTED','8:643afc770749655fc3ecfc2ba5135feb','createTable tableName=tbproductavailability','New Master Data Entity : ProductAvailability',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-441-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:52',113,'EXECUTED','8:d0bd0f64bc1dae44b57f349230afadef','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for ProductAvailability',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-441-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:52',114,'EXECUTED','8:9a85dff1e56ae2479b6e275f0042b046','insert tableName=tbproductavailability; insert tableName=tbproductavailability','Default values for ProductAvailability',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-441-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:52',115,'EXECUTED','8:5519a2cdf9a5b838a7f1e9a91f01ea28','insert tableName=tbmenu','Insert navigation to ProductAvailability',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-595','hendra','db/migration/000.xml','2024-11-14 03:48:52',116,'EXECUTED','8:aacf6e3e3926412a854e4a81bb0fa152','insert tableName=tbmenu','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-440-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',117,'EXECUTED','8:8f2397878feb4fd3d66743d763390115','createTable tableName=tbmeattype','New Master Data Entity : MeatType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-440-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',118,'EXECUTED','8:951fc4f87ab991d7ddb0818553fac36d','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for MeatType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-440-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',119,'EXECUTED','8:b02bbdd2280b63492365630d28b97e84','insert tableName=tbmeattype; insert tableName=tbmeattype; insert tableName=tbmeattype','Default values for MeatType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-440-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',120,'EXECUTED','8:ea8cc86d67b080fb44f3f67e83431af9','insert tableName=tbmenu','Insert navigation to MeatType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-542','hendra','db/migration/000.xml','2024-11-14 03:48:53',121,'EXECUTED','8:c6f8bc1ad078c63759f92da91fcdfc31','addColumn tableName=tbwarehouselocation','add quarantine for location',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-399','Ghifari Arsa','db/migration/000.xml','2024-11-14 03:48:53',122,'EXECUTED','8:1261a605e90cbdc60a8abc4e6a27454d','insert tableName=tbsysprop','Penambahan default.dateformat',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-439','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',123,'EXECUTED','8:ec5ef8ea3215cee741e52db544a15cf7','addColumn tableName=tbsalesperson','penambahan WorkArea dan PhoneNumber pada Salesperson',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-589','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',124,'EXECUTED','8:96690730683e9addc17a966ccfb57ade','addColumn tableName=tbproductavailability; update tableName=tbproductavailability; update tableName=tbproductavailability','Active and Description field on Product Availability',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-539','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',125,'EXECUTED','8:6fea91d6e708ab5a06359ac9335d650b','addColumn tableName=tbcustomer','Shipment Type Id for customer',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-393','Ghifari Arsa','db/migration/000.xml','2024-11-14 03:48:53',126,'EXECUTED','8:ad705d4a8a4deabf5c2869847670fab1','delete tableName=tbsysprop; delete tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-442-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',127,'EXECUTED','8:4a2ee109d898fc59ab602753fb4bdc58','createTable tableName=tbpackagingtype','New Master Data Entity : PackagingType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-442-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',128,'EXECUTED','8:f515a288c8c0a6f52d98d0a1a2f3916d','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for PackagingType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-442-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',129,'EXECUTED','8:8e863aab40fc7e0061650617ccb61523','insert tableName=tbpackagingtype; insert tableName=tbpackagingtype; insert tableName=tbpackagingtype','Default values for PackagingType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-442-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',130,'EXECUTED','8:18869c5630ecee56809ad38c21ea57c7','insert tableName=tbmenu','Insert navigation to PackagingType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-445-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',131,'EXECUTED','8:30a1e4d51535b2120295c276c1f61242','createTable tableName=tbcasingtype','New Master Data Entity : CasingType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-445-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',132,'EXECUTED','8:0dc095f26710f31f649de609aa4d67ac','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for CasingType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-445-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',133,'EXECUTED','8:5375985df4113d72cc9ef2e77dc4f1da','insert tableName=tbcasingtype; insert tableName=tbcasingtype; insert tableName=tbcasingtype; insert tableName=tbcasingtype','Default values for CasingType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-445-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',134,'EXECUTED','8:fe962f5de35046085a3b35f551971072','insert tableName=tbmenu','Insert navigation to CasingType',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-443-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',135,'EXECUTED','8:e2b9bb4fd280a7c1ce1cc965476c1729','createTable tableName=tbproductgroup','New Master Data Entity : ProductGroup',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-443-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',136,'EXECUTED','8:62a50a6cc07027e49747d245cebac241','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for ProductGroup',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-443-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',137,'EXECUTED','8:f1ed8f1769bc35faca6c3f727e2b290e','insert tableName=tbproductgroup; insert tableName=tbproductgroup','Default values for ProductGroup',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-443-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',138,'EXECUTED','8:418ea9cc89480e7b7bc34bdc965f7c75','insert tableName=tbmenu','Insert navigation to ProductGroup',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-599','hendra','db/migration/000.xml','2024-11-14 03:48:53',139,'EXECUTED','8:099962308f3c3e34f205009623190f3b','addColumn tableName=tbinventoryallocation; insert tableName=tbmenu','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-558-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',140,'EXECUTED','8:fe2db01095d01d405ed63d8995633609','createTable tableName=tbproductweight','New Master Data Entity : ProductWeight',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-558-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',141,'EXECUTED','8:a6942258171c59132bdec5d213f51f66','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for ProductWeight',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-558-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',142,'EXECUTED','8:9fbdef25538343819c8c754a8540666c','insert tableName=tbproductweight; insert tableName=tbproductweight; insert tableName=tbproductweight; insert tableName=tbproductweight; insert tableName=tbproductweight; insert tableName=tbproductweight','Default values for ProductWeight',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-558-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',143,'EXECUTED','8:a579c3f6aa840c83b03d1bdcffd63ae3','insert tableName=tbmenu','Insert navigation to ProductWeight',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-588','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',144,'EXECUTED','8:036494c1cea7d16166e54acff9f9577c','addColumn tableName=tbmeattype','Active field on tbmeattype',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-661-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',145,'EXECUTED','8:f104b196ce84640bbacdc77786c08899','createTable tableName=tbdeliverycolorcode','New Master Data Entity : DeliveryColorCode',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-661-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',146,'EXECUTED','8:de19db7e6addf7b5c832ce1cbcd97582','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for DeliveryColorCode',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-661-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',147,'EXECUTED','8:cbef02cb60055029ffbb4953c8d45bea','insert tableName=tbdeliverycolorcode; insert tableName=tbdeliverycolorcode; insert tableName=tbdeliverycolorcode; insert tableName=tbdeliverycolorcode; insert tableName=tbdeliverycolorcode; insert tableName=tbdeliverycolorcode; insert tableName=tb...','Default values for DeliveryColorCode',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-661-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',148,'EXECUTED','8:5cfec44dfebfc00dc5a0be8afca7025e','insert tableName=tbmenu','Insert navigation to DeliveryColorCode',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-444-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',149,'EXECUTED','8:8f659c62aa8a3f2c67d97f0b7d91df04','createTable tableName=tbproductcasing','New Master Data Entity : ProductCasing',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-444-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',150,'EXECUTED','8:b0b5373e5bb5b0f53e6d4431ad162cef','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for ProductCasing',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-444-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',151,'EXECUTED','8:ee1c093f55c59f07d2f90a851a0f1ba4','insert tableName=tbproductcasing; insert tableName=tbproductcasing; insert tableName=tbproductcasing','Default values for ProductCasing',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-444-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',152,'EXECUTED','8:4380bba448139356f37184ee9efcef9d','insert tableName=tbmenu','Insert navigation to ProductCasing',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-383','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',153,'EXECUTED','8:0b9b9723d3554f08e1eeaab0aba19c07','addColumn tableName=tbcustomer','Delivery Parking Fee on Custoemr',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-560-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',154,'EXECUTED','8:342f1dc0f34ccb8b70047a8aa0a788cb','createTable tableName=tbkilogramweight','New Master Data Entity : KilogramWeight',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-560-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',155,'EXECUTED','8:a3c5344bdc89c63129ac3ad066bea74b','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for KilogramWeight',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-560-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',156,'EXECUTED','8:7c5a297cff7916e202f34ffa163cfab2','insert tableName=tbkilogramweight; insert tableName=tbkilogramweight; insert tableName=tbkilogramweight; insert tableName=tbkilogramweight; insert tableName=tbkilogramweight; insert tableName=tbkilogramweight','Default values for KilogramWeight',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-560-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',157,'EXECUTED','8:8d75a5002ef5487b8eefa06dac954d7a','insert tableName=tbmenu','Insert navigation to KilogramWeight',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-613-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',158,'EXECUTED','8:c7ee6eeda1fc569c2ab29b8b32bcf6e2','createTable tableName=tbdeliverymethod','New Master Data Entity : DeliveryMethod',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-598','hendra','db/migration/000.xml','2024-11-14 03:48:53',159,'EXECUTED','8:6974e2003b38685a5c5990891074a80b','dropNotNullConstraint columnName=location, tableName=tbinventoryallocation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-613-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',160,'EXECUTED','8:a5c1e856fc067d4295f467af84003dc7','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for DeliveryMethod',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-613-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',161,'EXECUTED','8:eddc20834b83e7614c4403c1987fc202','insert tableName=tbdeliverymethod; insert tableName=tbdeliverymethod; insert tableName=tbdeliverymethod; insert tableName=tbdeliverymethod; insert tableName=tbdeliverymethod; insert tableName=tbdeliverymethod','Default values for DeliveryMethod',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-613-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:53',162,'EXECUTED','8:3b39ea7c42822af77e728d499ff36eb8','insert tableName=tbmenu','Insert navigation to DeliveryMethod',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-524','malatasf','db/migration/000.xml','2024-11-14 03:48:53',163,'EXECUTED','8:3a3d93a4fc93b8418347b2d304930a8f','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-547','hendra','db/migration/000.xml','2024-11-14 03:48:54',164,'EXECUTED','8:244ae47bfa0d8d8d0ddb0ef9d479b0f6','insert tableName=tbmenu','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-577','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:54',165,'EXECUTED','8:9ab76742505db72330ba88721816eda8','addColumn tableName=tbproduct','Master Data relationships in Product',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-577-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:54',166,'EXECUTED','8:95f16269a2b1d2832b46fae73ea04037','insert tableName=tbmasterunit; insert tableName=tbmasterunit; insert tableName=tbprodcategory; insert tableName=tbprodcategory; insert tableName=tbprodcategory; insert tableName=tbprodcategory; insert tableName=tbprodcategory; insert tableName=tbp...','Other default values on product-related master data',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-408','malatasf','db/migration/000.xml','2024-11-14 03:48:54',167,'EXECUTED','8:e7ca608634c915df5a2018185b93b363','addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-618','malatasf','db/migration/000.xml','2024-11-14 03:48:55',168,'EXECUTED','8:603f58b8df96f923eec23810bb362a26','addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-453','malatasf','db/migration/000.xml','2024-11-14 03:48:55',169,'EXECUTED','8:3838a0aa3ef4c5a8f96a3bf19b1acfec','addColumn tableName=tbinvoiceitem; sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-437','malatasf','db/migration/000.xml','2024-11-14 03:48:55',170,'EXECUTED','8:c8a4e2d56977fb6ccfec3e2cc1eced21','dropForeignKeyConstraint baseTableName=tbinvoicepayment, constraintName=fk-typeforinvoicepayment; dropColumn columnName=paymenttype, tableName=tbinvoicepayment; addColumn tableName=tbinvoicepayment; sql; sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-526','malatasf','db/migration/000.xml','2024-11-14 03:48:55',171,'EXECUTED','8:86faee4d0bbf6cbb5042798bb69fd00f','addColumn tableName=tbpaymenttype','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-523','malatasf','db/migration/000.xml','2024-11-14 03:48:55',172,'EXECUTED','8:292421d50cf288ab94c81c9578d94ad5','createTable tableName=tbsettlemet; createTable tableName=tbinvoicepaymentsettlement; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission;...','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-630','hendra','db/migration/000.xml','2025-01-03 03:33:02',458,'RERAN','8:437f939d688ac6da47b349734c85fbb6','sql','',NULL,'3.10.3',NULL,NULL,'5875182219'),('SICMA-552','malatasf','db/migration/000.xml','2024-11-14 03:48:55',174,'EXECUTED','8:d75e4c78a29afb43e555be662293a73b','addColumn tableName=tbsettlemet; renameTable newTableName=tbsettlement, oldTableName=tbsettlemet; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepe...','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-436','malatasf','db/migration/000.xml','2024-11-14 03:48:55',175,'EXECUTED','8:fb1f90a8abec1f86d0991a616cd2ca78','addColumn tableName=tbsettlement; insert tableName=tbpermission; insert tableName=tbrolepermission','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-615','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:55',176,'EXECUTED','8:d511cdda5baef6c24ce4e1dbb4873678','update tableName=tbcustomer; renameColumn newColumnName=delivery_method_id, oldColumnName=delivery_method, tableName=tbcustomer; addForeignKeyConstraint baseTableName=tbcustomer, constraintName=fk-customer-deliverymethod, referencedTableName=tbdel...','Master Data relationships in Customer',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-615-old-masterdata-uniforming','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:55',177,'EXECUTED','8:44fc0ee4c8d189e9fae1cbc5c38d42f0','addColumn tableName=tbdeliveryarea','DeliveryArea datastructure should follow the exact same pattern as other Customer-related masterdata',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-615-defaultvalues','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:55',178,'EXECUTED','8:b6611ff029f62c3fc3c526021e534943','addDefaultValue columnName=status, tableName=tbpaymenttype; addDefaultValue columnName=status, tableName=tbcustomertype; insert tableName=tbpaymenttype; insert tableName=tbpaymenttype; insert tableName=tbpaymenttype; insert tableName=tbpaymenttype...','Default values for Customer Masterdatas',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-623','malatasf','db/migration/000.xml','2024-11-14 03:48:55',179,'EXECUTED','8:693dc7025ad4284a6b5098e73667dfc9','addColumn tableName=tbmasterorganisation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-549','malatasf','db/migration/000.xml','2024-11-14 03:48:55',180,'EXECUTED','8:58bdb47223faad9c28b8768a8ebfd21a','addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-409','malatasf','db/migration/000.xml','2024-11-14 03:48:55',181,'EXECUTED','8:4c1f0ffa41d45fdd5ff1d07a8650f8f7','renameColumn newColumnName=destination, oldColumnName=location, tableName=tbinvoice; addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-365','hendra','db/migration/000.xml','2024-11-14 03:48:55',182,'EXECUTED','8:781649ae4efd223c9cce53ff51c52625','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-364','hendra','db/migration/000.xml','2024-11-14 03:48:55',183,'EXECUTED','8:38defc24504445825bf9521e628a3215','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-325','hendra','db/migration/000.xml','2024-11-14 03:48:55',184,'EXECUTED','8:e0f5a527ac988fa37644920f36a84ce4','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-638','hendra','db/migration/000.xml','2024-11-14 03:48:55',185,'EXECUTED','8:bca4d62c102a8b0616ef45f4bfb909c6','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-398','hendra','db/migration/000.xml','2024-11-14 03:48:55',186,'EXECUTED','8:bdf57295386f9f3b2e5565d3712c3e6d','addColumn tableName=tbtruck','Add color code in Truck',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-368','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:55',187,'EXECUTED','8:9377786a14ac1e2aa5b6334f024d1327','dropColumn tableName=tbmasterorganisation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-640','malatasf','db/migration/000.xml','2024-11-14 03:48:56',188,'EXECUTED','8:5521472cd12afd8fc9866b103db92a2a','addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-402','hendra','db/migration/000.xml','2024-11-14 03:48:56',189,'EXECUTED','8:41351e8c5143c788b48cfdb2c0cf708e','addColumn tableName=tbdispatchallocation','Modify Table tbdispatchallocation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-641','malatasf','db/migration/000.xml','2024-11-14 03:48:56',190,'EXECUTED','8:6a5a0642ffa99a545d85c4e077a95020','addColumn tableName=tbinvoice; sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-648','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',191,'EXECUTED','8:001e080626a959d1a9f4aefbbef6abd4','dropForeignKeyConstraint baseTableName=tbcustomer, constraintName=fk-customer-default-delivery-color-code; dropColumn tableName=tbcustomer; addColumn tableName=tbcustomer','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-648-default-delivery-color','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',192,'EXECUTED','8:0bf8c4e573e6fb2d25b75221cfbac325','update tableName=tbcustomer','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-312-bug','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',193,'EXECUTED','8:e667c782f56d2a4e3786e3e45e439321','modifyDataType columnName=company_name, tableName=tbcustomer; modifyDataType columnName=category, tableName=tbsalepricebook; modifyDataType columnName=name, tableName=tbwarehousefacility; modifyDataType columnName=description, tableName=tbwarehous...','string length limit on name of automatically-generated location and facility and pricebook should follow customer name length limit',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-559','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',194,'EXECUTED','8:fc429ea8fd28459e76c5d55fdf3036b7','addColumn tableName=tbcustomer','add Hari Tukar Faktur to tbcustomer',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-644','malatasf','db/migration/000.xml','2024-11-14 03:48:56',195,'EXECUTED','8:c0fdc1942b1b769336d94380f705615b','addColumn tableName=tbinvoiceitem','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-419','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',196,'EXECUTED','8:392938a1b786a5abcf3c7117b7c3f552','sql','regrouping master data by their related business object',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-605','hendra','db/migration/000.xml','2024-11-14 03:48:56',197,'EXECUTED','8:962141eefb7376908bcec81149e904ba','addColumn tableName=tbinventoryblock','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-629','hendra','db/migration/000.xml','2024-11-14 03:48:56',198,'EXECUTED','8:51311dd7d2ca6a16575798540c0357ea','addColumn tableName=tbtruck','Add delivery area in Truck',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-661','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',199,'EXECUTED','8:af99fff199bcefc8e40ea0d8dff74883','sql','regrouping some parts in menu',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-431','hendra','db/migration/000.xml','2024-11-14 03:48:56',200,'EXECUTED','8:5aad6f0e4cb3cae418be5e377c42929c','addColumn tableName=tbmasterorganisation','Reporting ID fields in Organisation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-673-init','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',201,'EXECUTED','8:0427216fbc0c6a9e971caef0149aa442','createTable tableName=tbdeliverycity','New Master Data Entity : DeliveryCity',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-673-permissions','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',202,'EXECUTED','8:4fe219a392690bb7c4a9cc734decc80b','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Permissions for DeliveryCity',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-673-nav','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',203,'EXECUTED','8:a1723f47eaa2c9c8757a10ed4d9b01ae','insert tableName=tbmenu','Insert navigation to DeliveryCity',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-673-relationship','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:56',204,'EXECUTED','8:2c06f0d5a70c0e6c3408b5adcc43fe3c','addColumn tableName=tbcustomer; sql; dropColumn tableName=tbcustomer','set reference on Customer to DeliveryCity',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-678','hendra','db/migration/000.xml','2024-11-14 03:48:57',205,'EXECUTED','8:81651e88acebe0715538b54ff0168a35','addColumn tableName=tbinventoryallocation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-660','Ghifari Arsa','db/migration/000.xml','2024-11-14 03:48:57',206,'EXECUTED','8:bf409033d03dabc4764f95d3847e1b97','dropTable tableName=tbpermission; createTable tableName=tbpermission; loadData tableName=tbpermission','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-473','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:57',207,'EXECUTED','8:fe81b8363a8fae5acc5cd842ccdb5a3a','update tableName=tbmenu','reposition DeliveryCity to MasterData Customer',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-449','malatasf','db/migration/000.xml','2024-11-14 03:48:57',208,'EXECUTED','8:0ab589aa1ad4b65bc0da9cfe65ca4078','addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-600','hendra','db/migration/000.xml','2024-11-14 03:48:57',209,'EXECUTED','8:47df3be3c10c97471fb72b6f645f6635','insert tableName=tbmenu','New Menu - Receiving',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-700','[KSS] Arseno Harahap','db/migration/000.xml','2025-02-17 22:30:36',469,'RERAN','8:c28b4d1e0cf384e1ee6792fcfadf10aa','update tableName=tbwarehouselocation','handle old data where quarantine is null instead of true nor false',NULL,'3.10.3',NULL,NULL,'9831436130'),('SICMA-707','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:57',211,'EXECUTED','8:c48ea9cbf7c77de815970b45474d4cb7','insert tableName=tbsysprop','Report ID for Faktur Pajak',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-663','hendra','db/migration/000.xml','2024-11-14 03:48:57',212,'EXECUTED','8:6152a15f298465e1c057fffa75de1f3f','createTable tableName=tbdispatchrequest; dropColumn tableName=tbinventoryallocation','Create Table for DispatchRequest',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-736','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:57',213,'EXECUTED','8:7af55225c778173ee89085f8deadf2c2','update tableName=tbmenu','default link for inventory to lead into allocation instead of block',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-737','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:57',214,'EXECUTED','8:270002d80b8a7d9dcda760c3a6f9dbb2','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-735','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:58',215,'EXECUTED','8:ed62596e9c1a9ad229ba14eed7a1c507','addColumn tableName=tbinvoice','add faktur has been printed field',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-730','hendra','db/migration/000.xml','2024-11-14 03:48:58',216,'EXECUTED','8:16a288296c5d54ce50080d13c1397591','addColumn tableName=tbdispatchallocation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-767','hendra','db/migration/000.xml','2024-11-14 03:48:58',217,'EXECUTED','8:d8367cf567d16224520ad8bf1674f860','addColumn tableName=tbdispatchallocation','Modify Table tbdispatchallocation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-739','[KSS] Arseno Harahap','db/migration/000.xml','2024-11-14 03:48:58',218,'EXECUTED','8:a19b3c78bfbb35e1976a9612b6a7d3b0','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-789','hendra','db/migration/000.xml','2024-11-14 03:48:58',219,'EXECUTED','8:6c6b611f785be172fba6625296f607fc','addColumn tableName=tbdispatchallocation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-729','hendra','db/migration/000.xml','2024-11-14 03:48:58',220,'EXECUTED','8:0546f9e43dd8f25c5eab19503976bf4f','createTable tableName=tbdespatch; createTable tableName=tbdespatchallocation; insert tableName=tbmenu','Create Table for Despatch\nCreate Table for DespatchAllocation\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-729-tbmenu','hendra','db/migration/000.xml','2025-01-03 03:33:02',460,'RERAN','8:2e84563bcf397fa5d668c73f7864df35','sql','',NULL,'3.10.3',NULL,NULL,'5875182219'),('SICMA-816','hendra','db/migration/000.xml','2024-11-14 03:48:58',222,'EXECUTED','8:a1597269f2fc1cf7c4272c90a70c7bba','modifyDataType columnName=invoicecode, tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-822','hendra','db/migration/000.xml','2024-11-14 03:48:58',223,'EXECUTED','8:0cf8c1a75f87d3e89dc8572e33a55a8e','addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-833','hendra','db/migration/000.xml','2024-11-14 03:48:58',224,'EXECUTED','8:9be287fb5357ba61a0d337ca9bb20ce6','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-786','hendra','db/migration/000.xml','2024-11-14 03:48:58',225,'EXECUTED','8:a009ce2992ab8947d6b28fe883953c98','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=t...','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-786-fix','hendra','db/migration/000.xml','2024-11-14 03:48:58',226,'EXECUTED','8:cbbd704e6823a938ece051340ca08d83','delete tableName=tbmenu; dropColumn tableName=tbdespatchallocation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-786-menu','hendra','db/migration/000.xml','2024-11-14 03:48:58',227,'EXECUTED','8:20f71a45d031693b7ec8a7cb260e4e78','insert tableName=tbmenu; insert tableName=tbmenu; insert tableName=tbmenu; insert tableName=tbmenu','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-786-fix-1','hendra','db/migration/000.xml','2024-11-14 03:48:58',228,'EXECUTED','8:482a3f6eccf4e76e31de549f5b5931ba','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-925','hendra','db/migration/000.xml','2024-11-14 03:48:58',229,'EXECUTED','8:dfbb3c2006528240c0dbb5a429492aee','insert tableName=tbmenu; insert tableName=tbmenu; insert tableName=tbmenu; delete tableName=tbmenu','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-914','hendra','db/migration/000.xml','2024-11-14 03:48:58',230,'EXECUTED','8:b10ebbbb7a05f183db3f2707fb7760bc','insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-935','hendra','db/migration/000.xml','2024-11-14 03:48:58',231,'EXECUTED','8:87dcff8b229e596e77e0ac4e56537573','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-937','hendra','db/migration/000.xml','2024-11-14 03:48:58',232,'EXECUTED','8:a4451835f9fe10a34dad6b0dcb8efe04','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-936','hendra','db/migration/000.xml','2024-11-14 03:48:58',233,'EXECUTED','8:56ebefacdbd3174d9a8ffe57b0d8d9c2','addColumn tableName=tbsalepricebookprice; addColumn tableName=tbsalepricebook; sql','Add Alternative Name in PriceBook',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-930','hendra','db/migration/000.xml','2024-11-14 03:48:58',234,'EXECUTED','8:027d9ffdee45aefd8756d243d55f5cff','addColumn tableName=tbwarehousefacility; addColumn tableName=tbdespatch','Add Prefix and Sequence in Warehouse\nAdd code in Pindah Gudang',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-951','hendra','db/migration/000.xml','2024-11-14 03:48:58',235,'EXECUTED','8:671a899a7afc44cd863de4ddf4a3c96a','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-949','hendra','db/migration/000.xml','2024-11-14 03:48:58',236,'EXECUTED','8:b039549c4c6c4ab62e7286ca71bcfa42','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-946','hendra','db/migration/000.xml','2024-11-14 03:48:58',237,'EXECUTED','8:9223ae0a93d9e099d1ef4751eb1ce657','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-958','hendra','db/migration/000.xml','2024-11-14 03:48:58',238,'EXECUTED','8:635aae0a41e778b74ea648b8f348d303','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-955','hendra','db/migration/000.xml','2024-11-14 03:48:58',239,'EXECUTED','8:1dc47eaf7a74a12cd27fa7e3414c2721','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-947','hendra','db/migration/000.xml','2024-11-14 03:48:58',240,'EXECUTED','8:f76a0876e97a26d202cad77f4396d39a','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-959','hendra','db/migration/000.xml','2024-11-14 03:48:58',241,'EXECUTED','8:18dc3e1aed05a2e89668b3e3c9dab37b','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-788','hendra','db/migration/000.xml','2024-11-14 03:48:58',242,'EXECUTED','8:0cdef3ddde1982fbb64caef8fa18bc00','addColumn tableName=tbdispatchallocation','Add Retur to Dispatch Allocation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-977','hendra','db/migration/000.xml','2024-11-14 03:48:59',243,'EXECUTED','8:6b24ca16796a67a0767c0bdfd4770e10','sql; addColumn tableName=tbreturn; addNotNullConstraint columnName=invoice, tableName=tbreturn; createTable tableName=tbreturnitem; addColumn tableName=tbdispatchallocation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-343','hendra','db/migration/000.xml','2024-11-14 03:48:59',244,'EXECUTED','8:35c97d3f758ceddf78bf77c0654e2e72','sql; addColumn tableName=tbinvoicepaymentsettlement','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-810','hendra','db/migration/000.xml','2024-11-14 03:48:59',245,'EXECUTED','8:14a77e4324112594f71fa9db1eecd345','insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-979','hendra','db/migration/000.xml','2024-11-14 03:48:59',246,'EXECUTED','8:f4211a416dab52122315824fd6ad2f31','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-848','hendra','db/migration/000.xml','2024-11-14 03:48:59',247,'EXECUTED','8:61efa70ebb49fee3e7f69d883246473e','addColumn tableName=tbinventoryallocation','Audit fields for tbinventoryallocation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-997','hendra','db/migration/000.xml','2024-11-14 03:48:59',248,'EXECUTED','8:87136ac78df8349aabc6cd6c8b022657','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1004','hendra','db/migration/000.xml','2024-11-14 03:48:59',249,'EXECUTED','8:a8eea72c3b7fa4cd58ba6e8745dacbc3','addColumn tableName=tbinventorymovement','Audit fields for tbinventorymovement',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1014','hendra','db/migration/000.xml','2024-11-14 03:48:59',250,'EXECUTED','8:656c3bd8e9dce04fce1b30facfae9d08','modifyDataType columnName=number_plate, tableName=tbtruck','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1015','hendra','db/migration/000.xml','2024-11-14 03:48:59',251,'EXECUTED','8:a49368eb563a885bc0ec767a048ae598','modifyDataType columnName=efaktur_sequence, tableName=tbmasterorganisation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1018','hendra','db/migration/000.xml','2024-11-14 03:48:59',252,'EXECUTED','8:08fe4b50453de8b26c624378c5cb1913','addColumn tableName=tbinventorymovement','Description for tbinventorymovement',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-954','hendra','db/migration/000.xml','2024-11-14 03:48:59',253,'EXECUTED','8:1db0ed353d48547b7004d9f044e8a84e','insert tableName=tbpermission; insert tableName=tbrolepermission','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1019','hendra','db/migration/000.xml','2024-11-14 03:48:59',254,'EXECUTED','8:f725be7706001b919f2963008cc59e25','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1021','hendra','db/migration/000.xml','2024-11-14 03:49:00',255,'EXECUTED','8:bf44d3358fd9dd33451759a5a4c74a11','createTable tableName=tbrepackage','Create Repackage',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1028','hendra','db/migration/000.xml','2024-11-14 03:49:00',256,'EXECUTED','8:6347827345c03321d892a8fe0e0e686c','renameColumn newColumnName=resale_price_list, oldColumnName=companyaddress, tableName=tbcustomer','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1029','hendra','db/migration/000.xml','2024-11-14 03:49:00',257,'EXECUTED','8:62c00e70f9655c6ac38f4dd0562df316','addColumn tableName=tbinvoiceitem','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1030','hendra','db/migration/000.xml','2024-11-14 03:49:00',258,'EXECUTED','8:0f23051de895ef2b3e9aebe75501e427','insert tableName=tbpermission; insert tableName=tbrolepermission','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('sicma-1031','hendra','db/migration/000.xml','2024-11-14 03:49:00',259,'EXECUTED','8:de913581d590e87e6dd836fae0cc8fb7','addColumn tableName=tbinvoicepayment','Add New Column',NULL,'3.10.3',NULL,NULL,'1556125930'),('sicma-1034','hendra','db/migration/000.xml','2024-11-14 03:49:00',260,'EXECUTED','8:0714936b41e350c206511890b48a0366','createTable tableName=tbsettlementadjustment','Create Table tbsettlementadjustment',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1039','hendra','db/migration/000.xml','2024-11-14 03:49:00',261,'EXECUTED','8:8cefc07a911f4f2ebfa468ebf580cfdb','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1067','hendra','db/migration/000.xml','2024-11-14 03:49:00',262,'EXECUTED','8:6887cc98b0de188a88b88a0e2d632214','sql; insert tableName=tbsysprop','Update Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1068','hendra','db/migration/000.xml','2024-11-14 03:49:00',263,'EXECUTED','8:d5ec419d349d93060deea2bd9448c5a4','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1069','hendra','db/migration/000.xml','2024-11-14 03:49:00',264,'EXECUTED','8:28180573ec73ae4ea4cd75dab4b9fbb5','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1070','hendra','db/migration/000.xml','2024-11-14 03:49:00',265,'EXECUTED','8:fb335a9f0efc8fb4bd7fb707aaef6050','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1079','hendra','db/migration/000.xml','2024-11-14 03:49:00',266,'EXECUTED','8:8316751b2b79520447e6cc2d52ce355a','addColumn tableName=tbinvoicepayment','Add New Column',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1081','hendra','db/migration/000.xml','2024-11-14 03:49:00',267,'EXECUTED','8:373106cf188e9cdda9d62fcea78ba3e8','insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1071','hendra','db/migration/000.xml','2024-11-14 03:49:00',268,'EXECUTED','8:1baa59a9b72d9f2c22468a84a70a2dee','insert tableName=tbpermission; insert tableName=tbrolepermission','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1082','hendra','db/migration/000.xml','2024-11-14 03:49:00',269,'EXECUTED','8:2bf646b4ab78026630dd2c3179d9d8e7','update tableName=tbmenu','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1091','hendra','db/migration/000.xml','2024-11-14 03:49:00',270,'EXECUTED','8:658e39aff320de37aba610f351f3be65','addColumn tableName=tbsettlement','Add remark in settlement',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1065','hendra','db/migration/000.xml','2024-11-14 03:49:00',271,'EXECUTED','8:1cda4b61e775773337d33dd55ab45515','addColumn tableName=tbdespatch; addColumn tableName=tbdespatchallocation','Add Creator for Despatch\nAdd Receiver for Despatch Allocation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1108','hendra','db/migration/000.xml','2024-11-14 03:49:00',272,'EXECUTED','8:6daf0687ed6a2ea35a897ae6a0df466f','addColumn tableName=tbinvoiceitem','Add Alternative Name to Invoice Item',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1109','hendra','db/migration/000.xml','2024-11-14 03:49:00',273,'EXECUTED','8:d2305bb7843e5ce0cf85c41154922c88','addColumn tableName=tbinvoiceitem','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1117','hendra','db/migration/000.xml','2024-11-14 03:49:00',274,'EXECUTED','8:8a0449a4867196374c94e2a3a629f064','modifyDataType columnName=remark, tableName=tbinvoicepayment','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1118','hendra','db/migration/000.xml','2024-11-14 03:49:00',275,'EXECUTED','8:a78df251fc2bf105a70c7000f6fa638c','insert tableName=tbpermission; insert tableName=tbrolepermission','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1120','hendra','db/migration/000.xml','2024-11-14 03:49:00',276,'EXECUTED','8:d2f4d23dd5c4aa8282d414f738778fc4','addColumn tableName=tbinvoice','Add new column to tbinvoice',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1126','hendra','db/migration/000.xml','2024-11-14 03:49:00',277,'EXECUTED','8:430b3231eed81cb996292fcf2c0f717d','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1099','hendra','db/migration/000.xml','2024-11-14 03:49:00',278,'EXECUTED','8:95141d40a6b8ca058590d17c3f56a551','createTable tableName=tbholdingcompany; addColumn tableName=tbcustomer','Create Table Holding Company\nAdd column to Table Customer',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1146','hendra','db/migration/000.xml','2024-11-14 03:49:00',279,'EXECUTED','8:418e166389d079fd3949055224071261','addColumn tableName=tbinvoicepayment','Audit fields for tbinvoicepayment',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1144','hendra','db/migration/000.xml','2024-11-14 03:49:00',280,'EXECUTED','8:ab7340e19b3d7b6fb7b6ae294cc301da','addColumn tableName=tbsettlement','Status for tbsettlement',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1148','hendra','db/migration/000.xml','2024-11-14 03:49:00',281,'EXECUTED','8:ee5d114ab852ca447db18d70fb3074d3','modifyDataType columnName=period, tableName=tbinvoicepayment','Modify tbinvoicepayment',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1152','hendra','db/migration/000.xml','2024-11-14 03:49:00',282,'EXECUTED','8:e96191d7e2dc145b7ea922fe21a0db70','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1154','hendra','db/migration/000.xml','2024-11-14 03:49:00',283,'EXECUTED','8:4f93469080bea5179555cda51e334f3a','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1156','hendra','db/migration/000.xml','2024-11-14 03:49:00',284,'EXECUTED','8:bed148c34ece01d70e57803bdc0047d8','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1100','hendra','db/migration/000.xml','2024-11-14 03:49:00',285,'EXECUTED','8:e2665e4debb5382f8771c5f666616698','addColumn tableName=tbinvoicepayment; sql','Customer for InvoicePayment',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1131','hendra','db/migration/000.xml','2024-11-14 03:49:00',286,'EXECUTED','8:d8f27aa7995197b2275ef3d824f5e3c3','createTable tableName=tbmasteradjustment; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tabl...','Create Adjustment Description Database\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1157','hendra','db/migration/000.xml','2024-11-14 03:49:00',287,'EXECUTED','8:04e4ef6fba91a573b454d5f463e1dd41','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1158','hendra','db/migration/000.xml','2024-11-14 03:49:00',288,'EXECUTED','8:910d9c841abbfa3b190d3d262edbf812','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1159','hendra','db/migration/000.xml','2024-11-14 03:49:00',289,'EXECUTED','8:97b820cdb956718550a05684725ade9a','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1160','hendra','db/migration/000.xml','2024-11-14 03:49:00',290,'EXECUTED','8:40c9c715febb2c88d91a405115211930','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1164','hendra','db/migration/000.xml','2024-11-14 03:49:00',291,'EXECUTED','8:fab8ae7df2fd97cb5c10c16a073a597f','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1166','hendra','db/migration/000.xml','2024-11-14 03:49:01',292,'EXECUTED','8:18c340dea0ee01c4c2be37cce9bdb0cc','insert tableName=tbpermission; insert tableName=tbrolepermission','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1171','hendra','db/migration/000.xml','2024-11-14 03:49:01',293,'EXECUTED','8:c271715d22ab029877548a0680b127a4','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1167','hendra','db/migration/000.xml','2024-11-14 03:49:01',294,'EXECUTED','8:2a09d61d7c6246c24562e71ca77e1dbe','addColumn tableName=tbinventoryblock','Add Creator for Inventory Block',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1178','hendra','db/migration/000.xml','2024-11-14 03:49:01',295,'EXECUTED','8:b4c3342f73140f144363d9b7e1867858','renameColumn newColumnName=surat_jalan_report_small_id, oldColumnName=surat_jalan_report_id, tableName=tbmasterorganisation; addColumn tableName=tbmasterorganisation','Add reporting id for organisation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1169','hendra','db/migration/000.xml','2024-11-14 03:49:01',296,'EXECUTED','8:2d7b6b1d5fc3467c96153456874c0947','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1170','hendra','db/migration/000.xml','2024-11-14 03:49:01',297,'EXECUTED','8:b974d9dd0f3d6355f08245eaff1c306e','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1182','hendra','db/migration/000.xml','2024-11-14 03:49:01',298,'EXECUTED','8:1cc30053ef28f025653270b19931c08e','addColumn tableName=tbinvoicepayment','Create new column for tbinvoicepayment',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1183','hendra','db/migration/000.xml','2024-11-14 03:49:01',299,'EXECUTED','8:40aba69b06c13b4358eb607e9fc954e8','modifyDataType columnName=payment_sequence, tableName=tbmasterorganisation','modify column data type',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1192','hendra','db/migration/000.xml','2024-11-14 03:49:01',300,'EXECUTED','8:b31fd5b7ab5c14488a2a537ee5959449','createTable tableName=tbsettlementoverpayment','create table tbsettlementoverpayment',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1211','hendra','db/migration/000.xml','2024-11-14 03:49:01',301,'EXECUTED','8:c7297a51c325c2d3ce35db5b82bf620f','addColumn tableName=tbmasterorganisation','Add reporting id for organisation',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1212','hendra','db/migration/000.xml','2024-11-14 03:49:01',302,'EXECUTED','8:aebc0d2c95c6007a1c9fd4c25ca69f73','addColumn tableName=tbmasterorganisation','Add reporting id for Quotation Letter',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1214','hendra','db/migration/000.xml','2024-11-14 03:49:01',303,'EXECUTED','8:b14a68ae1a1ebc859fbcb38fa3b4f6cd','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1216','hendra','db/migration/000.xml','2024-11-14 03:49:01',304,'EXECUTED','8:8e990aeb20fb93f97f34f7d14f39f2c9','insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1220','hendra','db/migration/000.xml','2024-11-14 03:49:01',305,'EXECUTED','8:1e84a017b6c8a65dcc7c3e7d71f66a03','createTable tableName=tbreport; sql; insert tableName=tbpermission; insert tableName=tbrolepermission; update tableName=tbmenu; insert tableName=tbmenu','Report Table\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1224','hendra','db/migration/000.xml','2024-11-14 03:49:01',306,'EXECUTED','8:2687c6d2376fa4b4494e6b10b73b043b','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1226','hendra','db/migration/000.xml','2024-11-14 03:49:01',307,'EXECUTED','8:c4954b87bc81fddeee60efafd72783e5','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1228','hendra','db/migration/000.xml','2024-11-14 03:49:01',308,'EXECUTED','8:69ba2b3f9b0c62ba3b9aa7cf882d6268','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1229','hendra','db/migration/000.xml','2024-11-14 03:49:01',309,'EXECUTED','8:9b1833640172b057e8f68c4c4043ebbe','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1230','hendra','db/migration/000.xml','2024-11-14 03:49:01',310,'EXECUTED','8:ec0ae641687c17ceb64ea5431cc64126','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1231','hendra','db/migration/000.xml','2024-11-14 03:49:01',311,'EXECUTED','8:8da36f055f5def84ae0c5d99e385ad85','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1232','hendra','db/migration/000.xml','2024-11-14 03:49:01',312,'EXECUTED','8:44d865f73a810f7537d04950bf884b4b','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1233','hendra','db/migration/000.xml','2024-11-14 03:49:01',313,'EXECUTED','8:42b1900fbe671b514a0de7b8a3297c90','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1234','hendra','db/migration/000.xml','2024-11-14 03:49:01',314,'EXECUTED','8:a771848461f833355608155b9c8456ef','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1235','hendra','db/migration/000.xml','2024-11-14 03:49:01',315,'EXECUTED','8:2398d5519ed1f81d522fb19ac711b522','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1249','hendra','db/migration/000.xml','2024-11-14 03:49:01',316,'EXECUTED','8:864b7fd7ee69f56f625bcc1f06880111','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1251','hendra','db/migration/000.xml','2024-11-14 03:49:01',317,'EXECUTED','8:ecf9836822308d18be44c55772747e1a','addColumn tableName=tbsettlement','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1265','hendra','db/migration/000.xml','2024-11-14 03:49:01',318,'EXECUTED','8:9edc2a1eb78a64ca62ac0d2c3b5fe64c','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1280','hendra','db/migration/000.xml','2024-11-14 03:49:01',319,'EXECUTED','8:145016958a0714d6601b6dce72a9df63','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1285','Hendra','db/migration/000.xml','2024-11-14 03:49:01',320,'EXECUTED','8:a92a23a3ea169ffb5a2a645a91b3944c','addColumn tableName=tbsalesperson','Position for Salesperson',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1293','hendra','db/migration/000.xml','2024-11-14 03:49:01',321,'EXECUTED','8:f366d11572b8b7a20c3b7606a3c661c6','delete tableName=tbreport; delete tableName=tbreport','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1295','hendra','db/migration/000.xml','2024-11-14 03:49:01',322,'EXECUTED','8:385239264addb58f57687737ea0ca74d','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1296','hendra','db/migration/000.xml','2024-11-14 03:49:01',323,'EXECUTED','8:2c12d6b9d5245299b481a6b87a8bc92a','createTable tableName=tbproductqtyhistory','Create New Table',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1296-modify','hendra','db/migration/000.xml','2024-11-14 03:49:01',324,'EXECUTED','8:848b6a19b8d7025db27d7662ebee1d85','dropColumn columnName=after_m_source_f_p_stock_q, tableName=tbproductqtyhistory; dropColumn columnName=after_m_target_f_p_stock_q, tableName=tbproductqtyhistory; addColumn tableName=tbproductqtyhistory','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1326','hendra','db/migration/000.xml','2024-11-14 03:49:01',325,'EXECUTED','8:71e0d53eab8b4d3f6b883173cd05ea59','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1330','hendra','db/migration/000.xml','2024-11-14 03:49:01',326,'EXECUTED','8:c68d915cf53b7c25f50893d0ed1a46aa','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1338','hendra','db/migration/000.xml','2024-11-14 03:49:01',327,'EXECUTED','8:fa93557f92cf8b9962a6f95b03986042','createTable tableName=tbsettlementoverpaymentdetail','Create New Table',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1344','hendra','db/migration/000.xml','2024-11-14 03:49:01',328,'EXECUTED','8:9afbf172a949fadd4f21036495c84a12','dropForeignKeyConstraint baseTableName=tbsettlementoverpaymentdetail, constraintName=fk-settlementoverpayment-sod; dropForeignKeyConstraint baseTableName=tbsettlementoverpaymentdetail, constraintName=fk-settlement-sod; dropForeignKeyConstraint bas...','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1350','hendra','db/migration/000.xml','2024-11-14 03:49:01',329,'EXECUTED','8:482d9282880915e334748c9eb2494ff0','addColumn tableName=tbinvoiceitem','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1350-Update','hendra','db/migration/000.xml','2024-11-14 03:49:01',330,'EXECUTED','8:a7a770c20b06c019f2bb70898b097cf3','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1355','hendra','db/migration/000.xml','2024-11-14 03:49:01',331,'EXECUTED','8:56892fde44538a3d283309084b111b28','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1397','hendra','db/migration/000.xml','2024-11-14 03:49:01',332,'EXECUTED','8:3f9e6d9c3c3dd03838abd49d1e0dcdc4','insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1479','galih','db/migration/000.xml','2024-11-14 03:49:01',333,'EXECUTED','8:8648291d502b2aec0e1eedaa2964dfb0','insert tableName=tbreport','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1497','galih','db/migration/000.xml','2024-11-14 03:49:01',334,'EXECUTED','8:ff0fa8ed0f00711e8c8ca4ee007cec95','addColumn tableName=tbproduct','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1507','ahmad','db/migration/000.xml','2024-11-14 03:49:01',335,'EXECUTED','8:80fd385733476740ad18ef573aee33b5','insert tableName=tbreport','Add Value Report Table',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1395','hendra','db/migration/000.xml','2024-11-14 03:49:01',336,'EXECUTED','8:5f7d5ef3039d97504ed3a6733aa4d085','addColumn tableName=tbmenu; sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1372','hendra','db/migration/000.xml','2024-11-14 03:49:02',337,'EXECUTED','8:04f4e27858242780513368857ffc4786','createTable tableName=tbcurrency; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermis...','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1364','hendra','db/migration/000.xml','2024-11-14 03:49:02',338,'EXECUTED','8:d635899751cb87eada944a26ef34d028','createTable tableName=tbrawmaterialscategory; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=...','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1380','hendra','db/migration/000.xml','2024-11-14 03:49:02',339,'EXECUTED','8:f8d88a7a3eb8b15bc2bdc497d7d396a4','createTable tableName=tbstorehousefacility; createTable tableName=tbstorehousefacilitycategory; createTable tableName=tbstorehouselocation; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbp...','Create Storehouse Database\nTo define what raw materials category can be stored\nCreate Storehouse Location Database\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('sicma-1388','hendra','db/migration/000.xml','2024-11-14 03:49:02',340,'EXECUTED','8:93e3f999bf058aecb56d2bf3432f8d1e','createTable tableName=tbrawmaterials; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepe...','Create Module Raw Materials\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1407','hendra','db/migration/000.xml','2024-11-14 03:49:02',341,'EXECUTED','8:b978149415d9fd38d2732577664eff26','createTable tableName=tbrawmaterialsfile; insert tableName=tbmenu; modifyDataType columnName=content_type, tableName=tbfile','Create Raw Materials File Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1402','hendra','db/migration/000.xml','2024-11-14 03:49:02',342,'EXECUTED','8:2c106c61575f34d544d80c1da85741cc','addColumn tableName=tbstorehouselocation','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1389','hendra','db/migration/000.xml','2024-11-14 03:49:02',343,'EXECUTED','8:a8d7924d500e075f58fd386c5fe38f63','sql; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableN...','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1368','hendra','db/migration/000.xml','2024-11-14 03:49:02',344,'EXECUTED','8:270efab50eb8fc78469261d55c67f0c3','update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmen...','Table Supplier Category\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1376','hendra','db/migration/000.xml','2024-11-14 03:49:02',345,'EXECUTED','8:0c9bdde970ecd6cd52e8dcbd6b76b03f','renameColumn newColumnName=issue_date, oldColumnName=issue_date, tableName=tbrawmaterialsfile; renameColumn newColumnName=expiration_date, oldColumnName=expiration_date, tableName=tbrawmaterialsfile; renameTable newTableName=tbsuppliersfile, oldTa...','Rename Table tbrawmaterialsfile\nCreate Supplier Database\nCreate Pic Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1383','hendra','db/migration/000.xml','2024-11-14 03:49:02',346,'EXECUTED','8:d6c8fe27048d882ec95c10edb5169db6','update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmenu','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1427','hendra','db/migration/000.xml','2024-11-14 03:49:02',347,'EXECUTED','8:c82165f691fe1ff3831858ad3011fedc','dropForeignKeyConstraint baseTableName=tbsupplier, constraintName=fk-suppliercategory; dropColumn columnName=category, tableName=tbsupplier; delete tableName=tbmenu; dropTable tableName=tbsuppliercategory; createTable tableName=tbsuppliercategory','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1384','hendra','db/migration/000.xml','2024-11-14 03:49:02',348,'EXECUTED','8:cffc7a49d39727ca08c36c498990d3ae','update tableName=tbmenu; update tableName=tbmenu; update tableName=tbmenu; createTable tableName=tbsuppliercatalog; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableN...','Create Catalog Database\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1441','hendra','db/migration/000.xml','2024-11-14 03:49:02',349,'EXECUTED','8:1eba2bcc489e078c99fe93d6b192c652','insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop; insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1442','hendra','db/migration/000.xml','2024-11-14 03:49:02',350,'EXECUTED','8:ca411c0f83d98ee566e8d1f44dc240c8','addColumn tableName=tbrawmaterials','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1429','hendra','db/migration/000.xml','2024-11-14 03:49:03',351,'EXECUTED','8:0dd181af9e21c384d011fc310e277328','sql; createTable tableName=tbpurchaserequest; createTable tableName=tbpurchaserequestitem; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert ...','Create Purchase Request Database\nCreate Purchase Request Item Database\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1433','hendra','db/migration/000.xml','2024-11-14 03:49:03',352,'EXECUTED','8:b1876c37380471e875e7e09fa916d498','createTable tableName=tbpurchaseorder; createTable tableName=tbpurchaseorderitem; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName...','Create Purchase Order Database\nCreate Purchase Order Item Database\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1437','hendra','db/migration/000.xml','2024-11-14 03:49:03',353,'EXECUTED','8:10999a9c14e452fc07733d21f6e2ccac','renameColumn newColumnName=invoice_status, oldColumnName=status, tableName=tbpurchaseorder; renameColumn newColumnName=invoice_status, oldColumnName=status, tableName=tbpurchaseorderitem; addColumn tableName=tbpurchaseorder; addColumn tableName=tb...','Create Purchase Invoice Database\nCreate Purchase Invoice Item Database\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1489','ahmad','db/migration/000.xml','2024-11-14 03:49:03',354,'EXECUTED','8:f8ef2f8f9065195d1e86adb0f670f429','insert tableName=tbsysprop; insert tableName=tbsysprop','Insert New Property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1461','hendra','db/migration/000.xml','2024-11-14 03:49:03',355,'EXECUTED','8:98651005e2c3171e8e4af66880bee24d','createTable tableName=tbpurchaseinvoicepayment; createTable tableName=tbpurchaseinvoicepaymentitem; createTable tableName=tbpurchaseinvoicepaymentadjustment; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; in...','Create Purchase Invoice Payment Database\nCreate Purchase Invoice Payment Item Database\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1446','hendra','db/migration/000.xml','2024-11-14 03:49:03',356,'EXECUTED','8:cd9146cbd409f15b5ce84d6bc3433c66','addColumn tableName=tbpurchaserequestitem','Modify Purchase Request Item',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1447','hendra','db/migration/000.xml','2024-11-14 03:49:03',357,'EXECUTED','8:42e8fbfba486c6f8587bd287d7a2a262','dropColumn tableName=tbpurchaseorder; addColumn tableName=tbpurchaseorder; addColumn tableName=tbpurchaseorderitem','Drop columns from tbpurchaseorder\nAdd columns for tbpurchaseorder\nAdd columns for tbpurchaseorderitem',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1448','hendra','db/migration/000.xml','2024-11-14 03:49:03',358,'EXECUTED','8:f9e3db149877e443e95b3c1748fb1bac','createTable tableName=tbqualitycontrol; createTable tableName=tbqualitycontrolitem; createTable tableName=tbqualitycontroliteminspection; createTable tableName=tbqcinspectioncriteria; createTable tableName=tbqualitycontroliteminspectionresult; sql...','create table Quality Control\ncreate table Quality Control Item\ncreate table Quality Control Item Inspection\ncreate table Quality Control Item Inspection Criteria\ncreate table Quality Control Item Inspection result\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1517','ahmad','db/migration/000.xml','2024-11-14 03:49:03',359,'EXECUTED','8:30a112d055e9dde89a571d6c0c8d69eb','insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbrolepermission; insert tableName=tbrolepermission; insert tableName=tbrolepermi...','Insert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1485','ahmad','db/migration/000.xml','2024-11-14 03:49:03',360,'EXECUTED','8:e8ff82f53d4c58c4b1f810bba65ebfc6','createTable tableName=tbmaterialrequest; createTable tableName=tbmaterialrequestitem; delete tableName=tbsysprop','Create MaterialRequest Database\nCreate MaterialRequestItem Database',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1457','hendra','db/migration/000.xml','2024-11-14 03:49:04',361,'EXECUTED','8:7690f50e8e69e6206907080c21a126ef','createTable tableName=tbstockblock; createTable tableName=tbstockallocation; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbro...','create table Stock Block\ncreate table Stock Allocation\nInsert new property',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1463','hendra','db/migration/000.xml','2024-11-14 03:49:04',362,'EXECUTED','8:460283073ce416e0100cf85c6d98e166','insert tableName=tbsysprop; insert tableName=tbsysprop; createTable tableName=tbstagingfacility; createTable tableName=tbstaginglocation; insert tableName=tbstagingfacility; insert tableName=tbstaginglocation; createTable tableName=tbmaterialrelea...','Insert New Property\nCreate Staging Facility and Location Database\nCreate MaterialRelease Database\nCreate MaterialReleaseItem Database\nCreate Menu for Staging Area Database\nInsert new property\nUpdate Menu for Material Request Database\nCreate Menu f...',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1463-UPDATE','hendra','db/migration/000.xml','2024-11-14 03:49:04',363,'EXECUTED','8:4ffd6ac7c0c994e192b7c885fd80aa11','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1522','irfan','db/migration/000.xml','2024-11-14 03:49:04',364,'EXECUTED','8:a56583af7537d14ddbb2af1dc9442484','addColumn tableName=tbsupplier','Add columns for tbsupplier',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1520','irfan','db/migration/000.xml','2024-11-14 03:49:04',365,'EXECUTED','8:4661a16f83e66825a6d4de1efbf862a1','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1530','irfan','db/migration/000.xml','2024-11-14 03:49:04',366,'EXECUTED','8:ac4cb3ee30b393f443267623633926f6','createTable tableName=tbstockmovement','create table Stock Movement',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1524','hendra','db/migration/000.xml','2024-11-14 03:49:04',367,'EXECUTED','8:a42f23ea9f97d71c52a413081a6977b0','sql','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1464','hendra','db/migration/000.xml','2024-11-14 03:49:05',368,'EXECUTED','8:62c3884365ad6710426e6578ff2259d4','dropForeignKeyConstraint baseTableName=tbmaterialreleaseitem, constraintName=fk-qciiForMri; dropForeignKeyConstraint baseTableName=tbmaterialreleaseitem, constraintName=fk-fromLocationForMri; dropForeignKeyConstraint baseTableName=tbmaterialreleas...','Modify Table tbmaterialreleaseitem\nModify Table tbstockallocation\nModify Table tbstockmovement',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1534','irfan','db/migration/000.xml','2024-11-14 03:49:05',369,'EXECUTED','8:de9ce47f074fda2e4d2d30a65a28c502','createTable tableName=tbshippingterms; createTable tableName=tbshippingtermsremarks; insert tableName=tbmenu; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableName=tbpermission; insert tableN...','Create ShippingTerms Database\nCreate ShippingTermsRemarks Database\nInsert navigation to ShippingTerms\nPermissions for ShippingTerms',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1542','hendra','db/migration/000.xml','2024-11-14 03:49:05',370,'EXECUTED','8:613b8c50cafae1a588a158eaeb7b3393','modifyDataType columnName=result, tableName=tbqualitycontroliteminspectionresult','',NULL,'3.10.3',NULL,NULL,'1556125930'),('SICMA-1549','hendra','db/migration/000.xml','2024-11-15 06:11:38',373,'EXECUTED','8:3f81c7558506f7c48d586718dc59a512','sql','',NULL,'3.10.3',NULL,NULL,'1651098082'),('SICMA-1529','hendra','db/migration/000.xml','2024-12-04 03:06:23',398,'EXECUTED','8:d3d037fabbded707f11a28ddef0c3668','insert tableName=tbmenu','Menu Stock Quarantine',NULL,'3.10.3',NULL,NULL,'3281583764'),('SICMA-1558','hendra','db/migration/000.xml','2024-12-04 03:45:31',405,'EXECUTED','8:570eb3efe425bf98a6a3f3e757dc2c21','insert tableName=tbreport','Add Value Report Table',NULL,'3.10.3',NULL,NULL,'3283931530'),('SICMA-1535','yana andika','db/migration/000.xml','2024-12-16 02:04:25',423,'EXECUTED','8:520929ed1d1ceb90b30acc2777f2048e','createTable tableName=tbpurchaseordershippingtermsremarks','',NULL,'3.10.3',NULL,NULL,'4314664862'),('SICMA-1462','hendra','db/migration/000.xml','2025-01-03 03:29:06',456,'EXECUTED','8:e7edcd39528a3ccc4fb6f2f8e62f87bf','dropNotNullConstraint columnName=quality_control_item_inspection, tableName=tbstockblock; addColumn tableName=tbstockblock','',NULL,'3.10.3',NULL,NULL,'5874946291'),('SICMA-1572','hendra','db/migration/000.xml','2025-01-08 11:18:41',467,'EXECUTED','8:133fe24753a1ae654808fb72ad1d2cf7','addColumn tableName=tbinvoice','',NULL,'3.10.3',NULL,NULL,'6335120784'),('SICMA-1521','juniko','db/migration/000.xml','2025-02-17 22:30:36',470,'EXECUTED','8:f13174e3df9846f8b4a2c443215e9623','addColumn tableName=tbrawmaterials','',NULL,'3.10.3',NULL,NULL,'9831436130'),('SICMA-1561','hendra','db/migration/000.xml','2025-02-17 22:30:36',471,'EXECUTED','8:9042031d704fbc8ae38c735a487b40c6','createTable tableName=tbpurchaseinvoiceexchange; createTable tableName=tbpurchaseinvoiceexchangeitem; insert tableName=tbsysprop','',NULL,'3.10.3',NULL,NULL,'9831436130'),('SICMA-1578','hendra','db/migration/000.xml','2025-02-17 22:30:36',472,'EXECUTED','8:3189f5c712c86f7c7099559d63f28114','addColumn tableName=tbcustomer; addColumn tableName=tbproduct; addColumn tableName=tbmasterorganisation','',NULL,'3.10.3',NULL,NULL,'9831436130'),('SICMA-1575','hendra','db/migration/000.xml','2025-02-17 22:30:36',473,'EXECUTED','8:d879cd2b187bf2335a16177ce6aca598','addColumn tableName=tbinvoice; update tableName=tbinvoice; update tableName=tbinvoice; update tableName=tbcustomer; update tableName=tbcustomer; update tableName=tbproduct','update tax_rate, for GST-disabled invoice, before DPP exist.\nupdate tax_rate, for GST-disabled invoice, after DPP exist.\nupdate tbcustomer column buyer_idt_ku and trx_code\nupdate tbproduct column code_ref_tax',NULL,'3.10.3',NULL,NULL,'9831436130'),('SICMA-1564','hendra','db/migration/000.xml','2025-02-17 22:30:36',474,'EXECUTED','8:817a4db1591500946791b6bd0fa71a5f','addColumn tableName=tbsupplier; sql','',NULL,'3.10.3',NULL,NULL,'9831436130'),('SICMA-1577','hendra','db/migration/000.xml','2025-02-17 22:30:36',475,'EXECUTED','8:fe6112c295e90d0e9ba952bec40e5398','insert tableName=tbreport','Add Value Report Table',NULL,'3.10.3',NULL,NULL,'9831436130');
/*!40000 ALTER TABLE `databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangeloglock` (
  `ID` int NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangeloglock`
--

LOCK TABLES `databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `databasechangeloglock` DISABLE KEYS */;
INSERT INTO `databasechangeloglock` VALUES (1,_binary '\0',NULL,NULL);
/*!40000 ALTER TABLE `databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcasingtype`
--

DROP TABLE IF EXISTS `tbcasingtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbcasingtype` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcasingtype`
--

LOCK TABLES `tbcasingtype` WRITE;
/*!40000 ALTER TABLE `tbcasingtype` DISABLE KEYS */;
INSERT INTO `tbcasingtype` VALUES (1,'COLLAGEN 21','21mm Collagen Casing',_binary ''),(2,'COLLAGEN 28','28mm Collagen Casing',_binary ''),(3,'NALO','NALO fibre casing',_binary ''),(4,'skinless','tanpa casing',_binary '');
/*!40000 ALTER TABLE `tbcasingtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcurrency`
--

DROP TABLE IF EXISTS `tbcurrency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbcurrency` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `country` varchar(128) DEFAULT NULL,
  `currency` varchar(128) DEFAULT NULL,
  `code` varchar(128) DEFAULT NULL,
  `symbol` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcurrency`
--

LOCK TABLES `tbcurrency` WRITE;
/*!40000 ALTER TABLE `tbcurrency` DISABLE KEYS */;
INSERT INTO `tbcurrency` VALUES (1,'INDONESIA','RP','IDR','RUPIAH'),(2,'AUSTRALIA','AUD','AUD','AUD'),(3,'AMERICA','USD','USD','USD'),(4,'EURO','EURO','EURO','EURO');
/*!40000 ALTER TABLE `tbcurrency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcustomer`
--

DROP TABLE IF EXISTS `tbcustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbcustomer` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(32) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `company_name` varchar(250) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `resale_price_list` varchar(250) DEFAULT NULL,
  `companyaddress2` varchar(250) DEFAULT NULL,
  `zipcode` varchar(16) DEFAULT NULL,
  `deliveryaddress` varchar(250) DEFAULT NULL,
  `delivery_recipient_name` varchar(128) DEFAULT NULL,
  `delivery_method_id` bigint DEFAULT NULL,
  `delivery_parking_fee` decimal(18,4) DEFAULT '0.0000',
  `shipment_type_id` bigint DEFAULT '2',
  `phone` varchar(250) DEFAULT NULL,
  `fax` varchar(128) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `TYPE` bigint DEFAULT NULL,
  `npwp` varchar(250) DEFAULT NULL,
  `npwpcompanyname` varchar(250) DEFAULT NULL,
  `npwpcompanyaddress` varchar(250) DEFAULT NULL,
  `buyer_Idt_Ku` varchar(250) DEFAULT NULL,
  `trx_code` varchar(250) DEFAULT NULL,
  `add_info` varchar(250) DEFAULT NULL,
  `TOP` bigint DEFAULT NULL,
  `creditlimit` decimal(19,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `salepricebook` bigint DEFAULT NULL,
  `ttf` int NOT NULL,
  `htf` varchar(30) DEFAULT NULL,
  `duedate_days_offset` bigint DEFAULT '3',
  `organisation_id` bigint DEFAULT NULL,
  `salesperson` bigint DEFAULT NULL,
  `enable_tax` bit(1) DEFAULT b'0',
  `delivery_area_id` bigint DEFAULT NULL,
  `default_payment_type_id` bigint DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `default_delivery_color_code_id` bigint DEFAULT NULL,
  `delivery_city_id` bigint DEFAULT NULL,
  `holding_company_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-top` (`TOP`),
  KEY `fk-customertype` (`TYPE`),
  KEY `fk-salepricebookforcustomer` (`salepricebook`),
  KEY `fk-customer-organisation` (`organisation_id`),
  KEY `fk-custsalesperson` (`salesperson`),
  KEY `fk-customer-deliveryarea` (`delivery_area_id`),
  KEY `fk-customer-defaultpaymenttype` (`default_payment_type_id`),
  KEY `fk-customer-shipmenttype` (`shipment_type_id`),
  KEY `fk-customer-deliverymethod` (`delivery_method_id`),
  KEY `fk-customer-deliverycity` (`delivery_city_id`),
  KEY `fk-holding` (`holding_company_id`),
  CONSTRAINT `fk-customer-defaultpaymenttype` FOREIGN KEY (`default_payment_type_id`) REFERENCES `tbpaymenttype` (`id`),
  CONSTRAINT `fk-customer-deliveryarea` FOREIGN KEY (`delivery_area_id`) REFERENCES `tbdeliveryarea` (`id`),
  CONSTRAINT `fk-customer-deliverycity` FOREIGN KEY (`delivery_city_id`) REFERENCES `tbdeliverycity` (`id`),
  CONSTRAINT `fk-customer-deliverymethod` FOREIGN KEY (`delivery_method_id`) REFERENCES `tbdeliverymethod` (`id`),
  CONSTRAINT `fk-customer-organisation` FOREIGN KEY (`organisation_id`) REFERENCES `tbmasterorganisation` (`id`),
  CONSTRAINT `fk-customer-shipmenttype` FOREIGN KEY (`shipment_type_id`) REFERENCES `tbshipmenttype` (`id`),
  CONSTRAINT `fk-customertype` FOREIGN KEY (`TYPE`) REFERENCES `tbcustomertype` (`id`),
  CONSTRAINT `fk-custsalesperson` FOREIGN KEY (`salesperson`) REFERENCES `tbsalesperson` (`id`),
  CONSTRAINT `fk-holding` FOREIGN KEY (`holding_company_id`) REFERENCES `tbholdingcompany` (`id`),
  CONSTRAINT `fk-salepricebookforcustomer` FOREIGN KEY (`salepricebook`) REFERENCES `tbsalepricebook` (`id`),
  CONSTRAINT `fk-top` FOREIGN KEY (`TOP`) REFERENCES `tbtop` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcustomer`
--

LOCK TABLES `tbcustomer` WRITE;
/*!40000 ALTER TABLE `tbcustomer` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbcustomer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcustomerfile`
--

DROP TABLE IF EXISTS `tbcustomerfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbcustomerfile` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `customer` bigint NOT NULL,
  `file` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-filerelated` (`file`),
  KEY `fk-customerrelated` (`customer`),
  CONSTRAINT `fk-customerrelated` FOREIGN KEY (`customer`) REFERENCES `tbcustomer` (`id`),
  CONSTRAINT `fk-filerelated` FOREIGN KEY (`file`) REFERENCES `tbfile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcustomerfile`
--

LOCK TABLES `tbcustomerfile` WRITE;
/*!40000 ALTER TABLE `tbcustomerfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbcustomerfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcustomerlocation`
--

DROP TABLE IF EXISTS `tbcustomerlocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbcustomerlocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `customer` bigint NOT NULL,
  `location` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-custloccust` (`customer`),
  KEY `fk-custlocloc` (`location`),
  CONSTRAINT `fk-custloccust` FOREIGN KEY (`customer`) REFERENCES `tbcustomer` (`id`),
  CONSTRAINT `fk-custlocloc` FOREIGN KEY (`location`) REFERENCES `tbwarehouselocation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcustomerlocation`
--

LOCK TABLES `tbcustomerlocation` WRITE;
/*!40000 ALTER TABLE `tbcustomerlocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbcustomerlocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcustomertype`
--

DROP TABLE IF EXISTS `tbcustomertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbcustomertype` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `status` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcustomertype`
--

LOCK TABLES `tbcustomertype` WRITE;
/*!40000 ALTER TABLE `tbcustomertype` DISABLE KEYS */;
INSERT INTO `tbcustomertype` VALUES (1,'HOTEL','Pelanggan dengan bentuk usaha Penginapan',_binary ''),(2,'RESTAURANT','Pelanggan dengan bentuk usaha Restoran',_binary ''),(3,'CAFE','Pelanggan dengan bentuk usaha Cafe',_binary ''),(4,'BAKERY','Pelanggan dengan bentuk usaha Toko Roti',_binary ''),(5,'SUPERMARKET','Pelanggan dengan bentuk usaha Pasar Swalayan',_binary ''),(6,'RESELLER RESMI','Reseller resmi mitra perusahaan',_binary ''),(7,'PERORANGAN','Pelanggan Perorangan',_binary ''),(8,'KARYAWAN','Karyawan perusahaan (in-house)',_binary '');
/*!40000 ALTER TABLE `tbcustomertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdashboard`
--

DROP TABLE IF EXISTS `tbdashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdashboard` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdashboard`
--

LOCK TABLES `tbdashboard` WRITE;
/*!40000 ALTER TABLE `tbdashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdashboardtile`
--

DROP TABLE IF EXISTS `tbdashboardtile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdashboardtile` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `dashboard_id` bigint NOT NULL,
  `heading` varchar(50) NOT NULL,
  `location` varchar(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `order_key` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdashboardtile`
--

LOCK TABLES `tbdashboardtile` WRITE;
/*!40000 ALTER TABLE `tbdashboardtile` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdashboardtile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdashboardtileconfiguration`
--

DROP TABLE IF EXISTS `tbdashboardtileconfiguration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdashboardtileconfiguration` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `dashboardtile_id` bigint NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdashboardtileconfiguration`
--

LOCK TABLES `tbdashboardtileconfiguration` WRITE;
/*!40000 ALTER TABLE `tbdashboardtileconfiguration` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdashboardtileconfiguration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdeliveryarea`
--

DROP TABLE IF EXISTS `tbdeliveryarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdeliveryarea` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdeliveryarea`
--

LOCK TABLES `tbdeliveryarea` WRITE;
/*!40000 ALTER TABLE `tbdeliveryarea` DISABLE KEYS */;
INSERT INTO `tbdeliveryarea` VALUES (1,'JAKARTA',NULL,_binary ''),(2,'SURABAYA',NULL,_binary ''),(3,'BANDUNG',NULL,_binary ''),(4,'CIKARANG KARAWANG',NULL,_binary ''),(5,'BEKASI',NULL,_binary ''),(6,'LUAR KOTA',NULL,_binary ''),(7,'BOGOR PUNCAK',NULL,_binary '');
/*!40000 ALTER TABLE `tbdeliveryarea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdeliverycity`
--

DROP TABLE IF EXISTS `tbdeliverycity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdeliverycity` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(240) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdeliverycity`
--

LOCK TABLES `tbdeliverycity` WRITE;
/*!40000 ALTER TABLE `tbdeliverycity` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdeliverycity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdeliverycolorcode`
--

DROP TABLE IF EXISTS `tbdeliverycolorcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdeliverycolorcode` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(240) DEFAULT NULL,
  `color` int DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdeliverycolorcode`
--

LOCK TABLES `tbdeliverycolorcode` WRITE;
/*!40000 ALTER TABLE `tbdeliverycolorcode` DISABLE KEYS */;
INSERT INTO `tbdeliverycolorcode` VALUES (1,'JAKARTA PUSAT 1','Kode warna pengiriman untuk zona JAKARTA PUSAT 1',16777215,_binary ''),(2,'JAKARTA PUSAT 2','Kode warna pengiriman untuk zona JAKARTA PUSAT 2',16777215,_binary ''),(3,'JAKARTA UTARA 1','Kode warna pengiriman untuk zona JAKARTA UTARA 1',16777215,_binary ''),(4,'JAKARTA UTARA 2','Kode warna pengiriman untuk zona JAKARTA UTARA 2',16777215,_binary ''),(5,'PIK','Kode warna pengiriman untuk zona PIK',16777215,_binary ''),(6,'BANDUNG 1','Kode warna pengiriman untuk zona BANDUNG 1',16777215,_binary ''),(7,'BANDUNG 2','Kode warna pengiriman untuk zona BANDUNG 2',16777215,_binary '');
/*!40000 ALTER TABLE `tbdeliverycolorcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdeliverymethod`
--

DROP TABLE IF EXISTS `tbdeliverymethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdeliverymethod` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(240) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdeliverymethod`
--

LOCK TABLES `tbdeliverymethod` WRITE;
/*!40000 ALTER TABLE `tbdeliverymethod` DISABLE KEYS */;
INSERT INTO `tbdeliverymethod` VALUES (1,'MOBIL BOX','Mobil Box swadaya',_binary ''),(2,'KERETA','KALOG Logistik Kereta Api Indonesia',_binary ''),(3,'MEX','Ekspedisi MEX, PT MEX Barlian Dirgantara',_binary ''),(4,'PANCA KOBRA','Ekspedisi Panca Kobra',_binary ''),(5,'PAXEL','Ekspedisi Paxel',_binary ''),(6,'GIRI INDAH','Ekspedisi Giri Indah',_binary '');
/*!40000 ALTER TABLE `tbdeliverymethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdespatch`
--

DROP TABLE IF EXISTS `tbdespatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdespatch` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(16) DEFAULT NULL,
  `product` bigint NOT NULL,
  `quantity` decimal(19,2) NOT NULL,
  `status` int NOT NULL,
  `target_location` bigint NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `created_by` varchar(64) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk-despatchlocation` (`target_location`),
  KEY `fk-despatchproduct` (`product`),
  CONSTRAINT `fk-despatchlocation` FOREIGN KEY (`target_location`) REFERENCES `tbwarehouselocation` (`id`),
  CONSTRAINT `fk-despatchproduct` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdespatch`
--

LOCK TABLES `tbdespatch` WRITE;
/*!40000 ALTER TABLE `tbdespatch` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdespatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdespatchallocation`
--

DROP TABLE IF EXISTS `tbdespatchallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdespatchallocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `despatch` bigint NOT NULL,
  `allocation` bigint NOT NULL,
  `source_location` bigint NOT NULL,
  `movement` bigint DEFAULT NULL,
  `quantity` decimal(19,2) DEFAULT NULL,
  `weight` decimal(19,4) DEFAULT NULL,
  `status` int NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `packing_date` datetime DEFAULT NULL,
  `ready_date` datetime DEFAULT NULL,
  `loading_date` datetime DEFAULT NULL,
  `delivering_date` datetime DEFAULT NULL,
  `received_date` datetime DEFAULT NULL,
  `delivered_date` datetime DEFAULT NULL,
  `returned_date` datetime DEFAULT NULL,
  `truck` bigint DEFAULT NULL,
  `driver` bigint DEFAULT NULL,
  `driver_assistant` bigint DEFAULT NULL,
  `delivery_priority` bigint DEFAULT NULL,
  `delivery_color_code` bigint DEFAULT NULL,
  `received_by` varchar(64) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk-despatchdriver` (`driver`),
  KEY `fk-despatchdriverassistant` (`driver_assistant`),
  KEY `fk-despatchedallocation` (`allocation`),
  KEY `fk-despatchtruck` (`truck`),
  KEY `fk-despatch-delivery-color-code` (`delivery_color_code`),
  KEY `fk-despatchmovement` (`movement`),
  KEY `fk-sourcelocation` (`source_location`),
  KEY `fk-allocationdespatch` (`despatch`),
  CONSTRAINT `fk-allocationdespatch` FOREIGN KEY (`despatch`) REFERENCES `tbdespatch` (`id`),
  CONSTRAINT `fk-despatch-delivery-color-code` FOREIGN KEY (`delivery_color_code`) REFERENCES `tbdeliverycolorcode` (`id`),
  CONSTRAINT `fk-despatchdriver` FOREIGN KEY (`driver`) REFERENCES `tbdriver` (`id`),
  CONSTRAINT `fk-despatchdriverassistant` FOREIGN KEY (`driver_assistant`) REFERENCES `tbdriver` (`id`),
  CONSTRAINT `fk-despatchedallocation` FOREIGN KEY (`allocation`) REFERENCES `tbinventoryallocation` (`id`),
  CONSTRAINT `fk-despatchmovement` FOREIGN KEY (`movement`) REFERENCES `tbinventorymovement` (`id`),
  CONSTRAINT `fk-despatchtruck` FOREIGN KEY (`truck`) REFERENCES `tbtruck` (`id`),
  CONSTRAINT `fk-sourcelocation` FOREIGN KEY (`source_location`) REFERENCES `tbwarehouselocation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdespatchallocation`
--

LOCK TABLES `tbdespatchallocation` WRITE;
/*!40000 ALTER TABLE `tbdespatchallocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdespatchallocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdispatch`
--

DROP TABLE IF EXISTS `tbdispatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdispatch` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `invoice` bigint DEFAULT NULL,
  `product` bigint NOT NULL,
  `quantity` decimal(19,2) NOT NULL,
  `status` int NOT NULL,
  `location` bigint NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-dispatchproduct` (`product`),
  KEY `fk-newlocation` (`location`),
  KEY `fk-dispatchinvoice` (`invoice`),
  CONSTRAINT `fk-dispatchinvoice` FOREIGN KEY (`invoice`) REFERENCES `tbinvoice` (`id`),
  CONSTRAINT `fk-dispatchproduct` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`),
  CONSTRAINT `fk-newlocation` FOREIGN KEY (`location`) REFERENCES `tbwarehouselocation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdispatch`
--

LOCK TABLES `tbdispatch` WRITE;
/*!40000 ALTER TABLE `tbdispatch` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdispatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdispatchallocation`
--

DROP TABLE IF EXISTS `tbdispatchallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdispatchallocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `dispatch` bigint NOT NULL,
  `allocation` bigint NOT NULL,
  `movement` bigint DEFAULT NULL,
  `quantity` decimal(19,2) DEFAULT NULL,
  `weight` decimal(19,4) DEFAULT NULL,
  `status` int NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `packing_date` datetime DEFAULT NULL,
  `ready_date` datetime DEFAULT NULL,
  `loading_date` datetime DEFAULT NULL,
  `delivering_date` datetime DEFAULT NULL,
  `delivered_date` datetime DEFAULT NULL,
  `truck` bigint DEFAULT NULL,
  `driver` bigint DEFAULT NULL,
  `driver_assistant` bigint DEFAULT NULL,
  `delivery_priority` bigint DEFAULT NULL,
  `delivery_color_code` bigint DEFAULT NULL,
  `returitem` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-dispatchedallocation` (`allocation`),
  KEY `fk-allocationdispatch` (`dispatch`),
  KEY `fk-damovement` (`movement`),
  KEY `fk-datruck` (`truck`),
  KEY `fk-dadriver` (`driver`),
  KEY `fk-dadriverassistant` (`driver_assistant`),
  KEY `fk-da-delivery-color-code` (`delivery_color_code`),
  KEY `fk-returitem` (`returitem`),
  CONSTRAINT `fk-allocationdispatch` FOREIGN KEY (`dispatch`) REFERENCES `tbdispatch` (`id`),
  CONSTRAINT `fk-da-delivery-color-code` FOREIGN KEY (`delivery_color_code`) REFERENCES `tbdeliverycolorcode` (`id`),
  CONSTRAINT `fk-dadriver` FOREIGN KEY (`driver`) REFERENCES `tbdriver` (`id`),
  CONSTRAINT `fk-dadriverassistant` FOREIGN KEY (`driver_assistant`) REFERENCES `tbdriver` (`id`),
  CONSTRAINT `fk-damovement` FOREIGN KEY (`movement`) REFERENCES `tbinventorymovement` (`id`),
  CONSTRAINT `fk-datruck` FOREIGN KEY (`truck`) REFERENCES `tbtruck` (`id`),
  CONSTRAINT `fk-dispatchedallocation` FOREIGN KEY (`allocation`) REFERENCES `tbinventoryallocation` (`id`),
  CONSTRAINT `fk-returitem` FOREIGN KEY (`returitem`) REFERENCES `tbreturnitem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdispatchallocation`
--

LOCK TABLES `tbdispatchallocation` WRITE;
/*!40000 ALTER TABLE `tbdispatchallocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdispatchallocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdispatchrequest`
--

DROP TABLE IF EXISTS `tbdispatchrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdispatchrequest` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `invoice` bigint NOT NULL,
  `product` bigint NOT NULL,
  `returned_dispatchment` bigint NOT NULL,
  `replacement_dispatch` bigint DEFAULT NULL,
  `quantity` decimal(19,2) DEFAULT NULL,
  `status` int NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `action_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-relatedinvoice` (`invoice`),
  KEY `fk-requestedproduct` (`product`),
  KEY `fk-replacementdispatch` (`replacement_dispatch`),
  KEY `fk-returneddispatchment` (`returned_dispatchment`),
  CONSTRAINT `fk-relatedinvoice` FOREIGN KEY (`invoice`) REFERENCES `tbinvoice` (`id`),
  CONSTRAINT `fk-replacementdispatch` FOREIGN KEY (`replacement_dispatch`) REFERENCES `tbdispatch` (`id`),
  CONSTRAINT `fk-requestedproduct` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`),
  CONSTRAINT `fk-returneddispatchment` FOREIGN KEY (`returned_dispatchment`) REFERENCES `tbdispatchallocation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdispatchrequest`
--

LOCK TABLES `tbdispatchrequest` WRITE;
/*!40000 ALTER TABLE `tbdispatchrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdispatchrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdriver`
--

DROP TABLE IF EXISTS `tbdriver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbdriver` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `phone_number` varchar(128) DEFAULT NULL,
  `driver_license_expiry_date` date DEFAULT NULL,
  `driver_license_number` varchar(64) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdriver`
--

LOCK TABLES `tbdriver` WRITE;
/*!40000 ALTER TABLE `tbdriver` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdriver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbfile`
--

DROP TABLE IF EXISTS `tbfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbfile` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `filename` varchar(250) NOT NULL,
  `content_type` varchar(100) DEFAULT NULL,
  `size` bigint DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filename` (`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbfile`
--

LOCK TABLES `tbfile` WRITE;
/*!40000 ALTER TABLE `tbfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbholdingcompany`
--

DROP TABLE IF EXISTS `tbholdingcompany`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbholdingcompany` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `customer` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-customerforholding` (`customer`),
  CONSTRAINT `fk-customerforholding` FOREIGN KEY (`customer`) REFERENCES `tbcustomer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbholdingcompany`
--

LOCK TABLES `tbholdingcompany` WRITE;
/*!40000 ALTER TABLE `tbholdingcompany` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbholdingcompany` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinventoryallocation`
--

DROP TABLE IF EXISTS `tbinventoryallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbinventoryallocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `block` bigint NOT NULL,
  `product` bigint NOT NULL,
  `location` bigint DEFAULT NULL,
  `quantity` decimal(19,2) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `quarantine_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-inventoryallocationproduct` (`product`),
  KEY `fk-inventoryallocationlocation` (`location`),
  KEY `fk-inventoryallocationblock` (`block`),
  CONSTRAINT `fk-inventoryallocationblock` FOREIGN KEY (`block`) REFERENCES `tbinventoryblock` (`id`),
  CONSTRAINT `fk-inventoryallocationlocation` FOREIGN KEY (`location`) REFERENCES `tbwarehouselocation` (`id`),
  CONSTRAINT `fk-inventoryallocationproduct` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinventoryallocation`
--

LOCK TABLES `tbinventoryallocation` WRITE;
/*!40000 ALTER TABLE `tbinventoryallocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinventoryallocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinventoryblock`
--

DROP TABLE IF EXISTS `tbinventoryblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbinventoryblock` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `product` bigint NOT NULL,
  `quantity` decimal(19,2) DEFAULT NULL,
  `production_code` varchar(32) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `created_by` varchar(64) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk-inventoryblockproduct` (`product`),
  CONSTRAINT `fk-inventoryblockproduct` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinventoryblock`
--

LOCK TABLES `tbinventoryblock` WRITE;
/*!40000 ALTER TABLE `tbinventoryblock` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinventoryblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinventorymovement`
--

DROP TABLE IF EXISTS `tbinventorymovement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbinventorymovement` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `from_location` bigint DEFAULT NULL,
  `to_location` bigint DEFAULT NULL,
  `product` bigint NOT NULL,
  `block` bigint NOT NULL,
  `type` int NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `allocation` bigint NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-tolocation` (`to_location`),
  KEY `fk-movementproduct` (`product`),
  KEY `fk-fromlocation` (`from_location`),
  KEY `fk-movementblock` (`block`),
  KEY `fk-moveallocation` (`allocation`),
  CONSTRAINT `fk-fromlocation` FOREIGN KEY (`from_location`) REFERENCES `tbwarehouselocation` (`id`),
  CONSTRAINT `fk-moveallocation` FOREIGN KEY (`allocation`) REFERENCES `tbinventoryallocation` (`id`),
  CONSTRAINT `fk-movementblock` FOREIGN KEY (`block`) REFERENCES `tbinventoryblock` (`id`),
  CONSTRAINT `fk-movementproduct` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`),
  CONSTRAINT `fk-tolocation` FOREIGN KEY (`to_location`) REFERENCES `tbwarehouselocation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinventorymovement`
--

LOCK TABLES `tbinventorymovement` WRITE;
/*!40000 ALTER TABLE `tbinventorymovement` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinventorymovement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinvoice`
--

DROP TABLE IF EXISTS `tbinvoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbinvoice` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `customer` bigint DEFAULT NULL,
  `destination` bigint DEFAULT NULL,
  `invoicedate` date DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `invoicecode` varchar(32) DEFAULT NULL,
  `monthly_sequence` bigint DEFAULT '1',
  `description` varchar(255) DEFAULT NULL,
  `reference` varchar(250) DEFAULT NULL,
  `subtotal` decimal(19,4) DEFAULT NULL,
  `discount` decimal(19,4) DEFAULT NULL,
  `gst` decimal(19,4) DEFAULT NULL,
  `delivery_charge` decimal(19,4) DEFAULT NULL,
  `adhoc_discount` decimal(19,4) DEFAULT '0.0000',
  `total` decimal(19,4) DEFAULT NULL,
  `adjustment` decimal(19,4) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `faktur_has_been_printed` bit(1) DEFAULT b'0',
  `organisation_id` bigint DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `shipment_type_id` bigint DEFAULT '2',
  `delivery_method_id` bigint DEFAULT '1',
  `source` bigint DEFAULT '1',
  `dpp` decimal(19,4) DEFAULT '0.0000',
  `tax_rate` decimal(3,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `fk-customerforinvoice` (`customer`),
  KEY `fk-invoice-organisation` (`organisation_id`),
  KEY `fk-invoicelocation` (`destination`),
  CONSTRAINT `fk-customerforinvoice` FOREIGN KEY (`customer`) REFERENCES `tbcustomer` (`id`),
  CONSTRAINT `fk-invoice-organisation` FOREIGN KEY (`organisation_id`) REFERENCES `tbmasterorganisation` (`id`),
  CONSTRAINT `fk-invoicelocation` FOREIGN KEY (`destination`) REFERENCES `tbwarehouselocation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinvoice`
--

LOCK TABLES `tbinvoice` WRITE;
/*!40000 ALTER TABLE `tbinvoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinvoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinvoiceinvoicepayment`
--

DROP TABLE IF EXISTS `tbinvoiceinvoicepayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbinvoiceinvoicepayment` (
  `invoice_id` bigint DEFAULT NULL,
  `invoicepayment_id` bigint DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinvoiceinvoicepayment`
--

LOCK TABLES `tbinvoiceinvoicepayment` WRITE;
/*!40000 ALTER TABLE `tbinvoiceinvoicepayment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinvoiceinvoicepayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinvoiceitem`
--

DROP TABLE IF EXISTS `tbinvoiceitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbinvoiceitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `invoice` bigint DEFAULT NULL,
  `product` bigint DEFAULT NULL,
  `alternative_name` varchar(128) DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `weight` decimal(19,4) DEFAULT NULL,
  `percentage_discount` decimal(19,4) DEFAULT '0.0000',
  `unitprice` decimal(19,4) DEFAULT NULL,
  `taxrate` decimal(19,4) DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `ctn` varchar(16) DEFAULT NULL,
  `kg` varchar(16) DEFAULT NULL,
  `formula` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-invoiceforinvoiceitem` (`invoice`),
  KEY `fk-productforinvoiceitem` (`product`),
  CONSTRAINT `fk-invoiceforinvoiceitem` FOREIGN KEY (`invoice`) REFERENCES `tbinvoice` (`id`),
  CONSTRAINT `fk-productforinvoiceitem` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinvoiceitem`
--

LOCK TABLES `tbinvoiceitem` WRITE;
/*!40000 ALTER TABLE `tbinvoiceitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinvoiceitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinvoicepayment`
--

DROP TABLE IF EXISTS `tbinvoicepayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbinvoicepayment` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `paymentdate` date DEFAULT NULL,
  `receipt_received_date` date DEFAULT NULL,
  `tfdate` date DEFAULT NULL,
  `payment_report_number` varchar(32) DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `period` varchar(100) DEFAULT NULL,
  `remark` varchar(600) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `status` varchar(32) DEFAULT 'OUTSTANDING',
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `customer` bigint DEFAULT NULL,
  `printed` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `fk-customerforinvoicepayment` (`customer`),
  CONSTRAINT `fk-customerforinvoicepayment` FOREIGN KEY (`customer`) REFERENCES `tbcustomer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinvoicepayment`
--

LOCK TABLES `tbinvoicepayment` WRITE;
/*!40000 ALTER TABLE `tbinvoicepayment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinvoicepayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinvoicepaymentsettlement`
--

DROP TABLE IF EXISTS `tbinvoicepaymentsettlement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbinvoicepaymentsettlement` (
  `invoice_payment_id` bigint DEFAULT NULL,
  `settlement_id` bigint DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `invoice` bigint NOT NULL,
  KEY `fk-invoiceforsettlement` (`invoice`),
  CONSTRAINT `fk-invoiceforsettlement` FOREIGN KEY (`invoice`) REFERENCES `tbinvoice` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinvoicepaymentsettlement`
--

LOCK TABLES `tbinvoicepaymentsettlement` WRITE;
/*!40000 ALTER TABLE `tbinvoicepaymentsettlement` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinvoicepaymentsettlement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbkilogramweight`
--

DROP TABLE IF EXISTS `tbkilogramweight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbkilogramweight` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `value` decimal(19,4) DEFAULT NULL,
  `description` varchar(240) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbkilogramweight`
--

LOCK TABLES `tbkilogramweight` WRITE;
/*!40000 ALTER TABLE `tbkilogramweight` DISABLE KEYS */;
INSERT INTO `tbkilogramweight` VALUES (1,0.2000,'0.2 Kilogram',_binary ''),(2,0.2500,'0.25 Kilogram',_binary ''),(3,0.3000,'0.3 Kilogram',_binary ''),(4,0.3200,'0.32 Kilogram',_binary ''),(5,0.5000,'0.5 Kilogram',_binary ''),(6,1.0000,'1 Kilogram',_binary '');
/*!40000 ALTER TABLE `tbkilogramweight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmasteradjustment`
--

DROP TABLE IF EXISTS `tbmasteradjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbmasteradjustment` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmasteradjustment`
--

LOCK TABLES `tbmasteradjustment` WRITE;
/*!40000 ALTER TABLE `tbmasteradjustment` DISABLE KEYS */;
INSERT INTO `tbmasteradjustment` VALUES (1,'ADMIN FEE','ADMIN FEE',_binary '');
/*!40000 ALTER TABLE `tbmasteradjustment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmasterorganisation`
--

DROP TABLE IF EXISTS `tbmasterorganisation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbmasterorganisation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `address_line_1` varchar(256) DEFAULT NULL,
  `address_line_2` varchar(256) DEFAULT NULL,
  `phone` varchar(1024) DEFAULT NULL,
  `fax` varchar(1024) DEFAULT NULL,
  `logo_url` varchar(128) DEFAULT NULL,
  `efaktur_sequence` varchar(16) DEFAULT NULL,
  `payment_sequence` varchar(16) DEFAULT NULL,
  `bank_name` varchar(32) DEFAULT NULL,
  `bank_acct_number` varchar(64) DEFAULT NULL,
  `bank_acct_holder` varchar(128) DEFAULT NULL,
  `invoice_prefix` varchar(64) DEFAULT NULL,
  `invoice_reporting_id` varchar(64) DEFAULT NULL,
  `receipt_reporting_id` varchar(64) DEFAULT NULL,
  `surat_jalan_report_large_id` varchar(64) DEFAULT NULL,
  `surat_jalan_report_small_id` varchar(64) DEFAULT NULL,
  `financial_pic_name` varchar(128) DEFAULT NULL,
  `financial_pic_position` varchar(128) DEFAULT NULL,
  `other_pic_name` varchar(128) DEFAULT NULL,
  `other_pic_position` varchar(128) DEFAULT NULL,
  `invoice_reporting_small_id` varchar(64) DEFAULT NULL,
  `report_price_increase_letter_id` varchar(64) DEFAULT NULL,
  `report_quotation_id` varchar(64) DEFAULT NULL,
  `seller_Idt_Ku` varchar(250) DEFAULT NULL,
  `npwp` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmasterorganisation`
--

LOCK TABLES `tbmasterorganisation` WRITE;
/*!40000 ALTER TABLE `tbmasterorganisation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbmasterorganisation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmasterunit`
--

DROP TABLE IF EXISTS `tbmasterunit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbmasterunit` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmasterunit`
--

LOCK TABLES `tbmasterunit` WRITE;
/*!40000 ALTER TABLE `tbmasterunit` DISABLE KEYS */;
INSERT INTO `tbmasterunit` VALUES (1,'PACK','Produk dijual per pack',_binary ''),(2,'WHOLE','Produk dijual utuh',_binary ''),(3,'PCS','',_binary ''),(4,'KG','',_binary ''),(5,'ROLL','',_binary ''),(6,'BOX','',_binary ''),(7,'CARTON','',_binary ''),(8,'LITER','',_binary ''),(9,'STICK','STICK',_binary ''),(10,'HANK','HANK',_binary '');
/*!40000 ALTER TABLE `tbmasterunit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmaterialrelease`
--

DROP TABLE IF EXISTS `tbmaterialrelease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbmaterialrelease` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `material_request` bigint NOT NULL,
  `mr_document_sequence` varchar(16) NOT NULL,
  `mr_batch_sequence` varchar(16) NOT NULL,
  `release_date` date NOT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-releaseMaterialRequest` (`material_request`),
  CONSTRAINT `fk-releaseMaterialRequest` FOREIGN KEY (`material_request`) REFERENCES `tbmaterialrequest` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmaterialrelease`
--

LOCK TABLES `tbmaterialrelease` WRITE;
/*!40000 ALTER TABLE `tbmaterialrelease` DISABLE KEYS */;
INSERT INTO `tbmaterialrelease` VALUES (1,1,'MR-Doc-0000001','MR-Batch-0000001','2024-11-15','admin','2024-11-15 06:36:12',NULL,'2024-11-15 06:36:12');
/*!40000 ALTER TABLE `tbmaterialrelease` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmaterialreleaseitem`
--

DROP TABLE IF EXISTS `tbmaterialreleaseitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbmaterialreleaseitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `material_release` bigint NOT NULL,
  `material_request_item` bigint NOT NULL,
  `quantity` decimal(19,4) NOT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-materialRelease` (`material_release`),
  KEY `fk-materialRequestItem` (`material_request_item`),
  CONSTRAINT `fk-materialRelease` FOREIGN KEY (`material_release`) REFERENCES `tbmaterialrelease` (`id`),
  CONSTRAINT `fk-materialRequestItem` FOREIGN KEY (`material_request_item`) REFERENCES `tbmaterialrequestitem` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmaterialreleaseitem`
--

LOCK TABLES `tbmaterialreleaseitem` WRITE;
/*!40000 ALTER TABLE `tbmaterialreleaseitem` DISABLE KEYS */;
INSERT INTO `tbmaterialreleaseitem` VALUES (1,1,1,1.0000,'admin','2024-11-15 06:36:12',NULL,NULL);
/*!40000 ALTER TABLE `tbmaterialreleaseitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmaterialrequest`
--

DROP TABLE IF EXISTS `tbmaterialrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbmaterialrequest` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `request_date` date NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  `requester` varchar(50) NOT NULL,
  `mr_sequence` varchar(16) NOT NULL,
  `status` int NOT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmaterialrequest`
--

LOCK TABLES `tbmaterialrequest` WRITE;
/*!40000 ALTER TABLE `tbmaterialrequest` DISABLE KEYS */;
INSERT INTO `tbmaterialrequest` VALUES (1,'2024-11-15','','Aris Rismawan','S-0000001',2,'admin','2024-11-15 06:35:33','admin','2024-11-15 06:36:12');
/*!40000 ALTER TABLE `tbmaterialrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmaterialrequestitem`
--

DROP TABLE IF EXISTS `tbmaterialrequestitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbmaterialrequestitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `material_request` bigint NOT NULL,
  `raw_materials` bigint NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `unit_weight` decimal(19,4) NOT NULL,
  `unit_count` decimal(19,4) NOT NULL,
  `status` int NOT NULL DEFAULT '0',
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-rawMaterials` (`raw_materials`),
  KEY `fk-materialRequest` (`material_request`),
  CONSTRAINT `fk-materialRequest` FOREIGN KEY (`material_request`) REFERENCES `tbmaterialrequest` (`id`),
  CONSTRAINT `fk-rawMaterials` FOREIGN KEY (`raw_materials`) REFERENCES `tbrawmaterials` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmaterialrequestitem`
--

LOCK TABLES `tbmaterialrequestitem` WRITE;
/*!40000 ALTER TABLE `tbmaterialrequestitem` DISABLE KEYS */;
INSERT INTO `tbmaterialrequestitem` VALUES (1,1,2,'Ayam Oy Ayam',5.0000,1.0000,2,'admin','2024-11-15 06:35:33','admin','2024-11-15 06:36:12');
/*!40000 ALTER TABLE `tbmaterialrequestitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmeattype`
--

DROP TABLE IF EXISTS `tbmeattype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbmeattype` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(240) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmeattype`
--

LOCK TABLES `tbmeattype` WRITE;
/*!40000 ALTER TABLE `tbmeattype` DISABLE KEYS */;
INSERT INTO `tbmeattype` VALUES (1,'BEEF','Daging Sapi',_binary ''),(2,'CHICKEN','Daging Ayam',_binary ''),(3,'PORK','Daging Babi',_binary '');
/*!40000 ALTER TABLE `tbmeattype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmenu`
--

DROP TABLE IF EXISTS `tbmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbmenu` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `main_group` varchar(128) DEFAULT NULL,
  `menu_group` varchar(128) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `pattern` varchar(250) NOT NULL,
  `icon` varchar(250) DEFAULT NULL,
  `order_by` bigint DEFAULT NULL,
  `business_object` varchar(512) DEFAULT NULL,
  `action` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmenu`
--

LOCK TABLES `tbmenu` WRITE;
/*!40000 ALTER TABLE `tbmenu` DISABLE KEYS */;
INSERT INTO `tbmenu` VALUES (1,NULL,'Security','User','/security/user','fa fa-user',3100,'User','READ'),(2,NULL,'Security','Role','/security/role','fa fa-users',3200,'Role','READ'),(3,NULL,'Settings','SysProps','/sys-properties','fa fa-cog',2900,'System Properties','READ'),(4,'MasterData','MasterDataDelivery','Driver','/fulfillment/driver/','fas fa-id-badge',2500,'Truck','READ'),(5,'MasterData','MasterDataDelivery','Truck','/fulfillment/truck/','fas fa-truck',2400,'Truck','READ'),(6,'MasterData','MasterDataCustomer','DeliveryArea','/master/deliveryarea','fa fa-map-marker',1800,'DeliveryArea','READ'),(7,NULL,'Settings','Organisation','/master/organisation','far fa-building',2700,'Organisation','READ'),(8,'MasterData','MasterDataProduct','ProductCategories','/productcategory','fa fa-cubes',300,'ProductCategory','READ'),(9,'FinishedProducts','Product','Products','/products','fa fa-cubes',3300,'Product','READ'),(10,NULL,'CustomerManagement','Customers','/customers','fa fa-address-book',1300,'Customer','READ'),(11,'MasterData','MasterDataCustomer','PaymentTerms','/master/top','fa fa-credit-card',1400,'Terms of Payment','READ'),(12,'MasterData','MasterDataCustomer','Salespersons','/salespersons','fa fa-id-badge',1400,'Salesperson','READ'),(13,'MasterData','MasterDataCustomer','CustomerType','/master/customertype','fa fa-address-card',1500,'Customer Type','READ'),(14,'MasterData','MasterDataCustomer','PaymentType','/master/paymenttype','fa fa-credit-card',1600,'Payment Type','READ'),(15,'MasterData','MasterDataProduct','Measurement','/master/unitofmeasurement','fa fa-balance-scale',900,'Unit of Measurement','READ'),(16,'FinishedProducts','Sales','Invoice','/invoices','fa fa-money',1700,'Invoice','READ'),(17,NULL,'CustomerManagement','PriceBook','/pricebook','fa fa-money',200,'PriceBook','READ'),(18,'FinishedProducts','Warehouse','Warehouses','/warehouse/facility','fa fa-money',3500,'Facility','READ'),(19,NULL,'Settings','Files','/files','fa fa-money',3000,'File','READ'),(20,'FinishedProducts','Reports','Reports','/reports','fa fa-money',5700,'Report','READ'),(21,'FinishedProducts','Warehouse','Inventories','/inventory/allocation','fa fa-money',3600,'Inventory','READ'),(22,'FinishedProducts','Dispatch','All','/dispatchment','fa fa-money',1800,'Dispatchment','READ'),(23,'FinishedProducts','Sales','Return','/returns','fa fa-money',4700,'Return','READ'),(24,NULL,'Settings','Dashboard','/dashboard','fa fa-home',2800,'Dashboard','READ'),(25,NULL,NULL,'Home','/','fa fa-home',0,'Home','READ'),(26,'FinishedProducts','Dispatch','OrderDelivery','/dispatchment/grouped/readyloading','fa fa-money',3550,'Dispatchment','READ'),(27,'MasterData','MasterDataCustomer','ShipmentType','/master/shipmenttype','fa fa-money',1900,'ShipmentType','READ'),(28,'FinishedProducts','Dispatch','Shipment','/dispatchment/grouped/delivering','fa fa-money',3600,'Dispatchment','READ'),(29,'FinishedProducts','Dispatch','Dispatchment','/dispatchment/grouped/newpacking','fa fa-money',3450,'Dispatchment','READ'),(30,'FinishedProducts','Sales','TukarFaktur','/invoice/payment','fa fa-money',4600,'Payment','READ'),(31,'MasterData','MasterDataProduct','ProductAvailability','/master/productavailability/','fa fa-check',700,'ProductAvailability','READ'),(32,'FinishedProducts','Dispatch','Delivered','/dispatchment/grouped/delivered','fa fa-money',3650,'Dispatchment','READ'),(33,'MasterData','MasterDataProduct','MeatType','/master/meattype/','fa fa-object-group',600,'MeatType','READ'),(34,'MasterData','MasterDataProduct','PackagingType','/master/packagingtype/','fa fa-square-o',1000,'PackagingType','READ'),(35,'MasterData','MasterDataProduct','CasingType','/master/casingtype/','fa fa-circle-o-notch',400,'CasingType','READ'),(36,'MasterData','MasterDataProduct','ProductGroup','/master/productgroup/','fa fa-object-group',800,'ProductGroup','READ'),(38,'MasterData','MasterDataProduct','ProductWeight','/master/productweight/','fa fa-object-group',1100,'ProductWeight','READ'),(39,'MasterData','MasterDataDelivery','DeliveryColorCode','/master/deliverycolorcode/','fa fa-object-group',2300,'DeliveryColorCode','READ'),(40,'MasterData','MasterDataProduct','ProductCasing','/master/productcasing/','fa fa-object-group',500,'ProductCasing','READ'),(41,'MasterData','MasterDataProduct','KilogramWeight','/master/kilogramweight/','fa fa-object-group',1200,'KilogramWeight','READ'),(42,'MasterData','MasterDataCustomer','DeliveryMethod','/master/deliverymethod/','fa fa-object-group',2000,'DeliveryMethod','READ'),(43,'FinishedProducts','Dispatch','Scaling','/dispatchment/grouped/scaling','fa fa-money',3500,'Dispatchment','READ'),(44,'FinishedProducts','Sales','Settlement','/invoice/settlement/groupedByCustomers','fa fa-money',4800,'Settlement','READ'),(45,'MasterData','MasterDataCustomer','DeliveryCity','/master/deliverycity/','fa fa-object-group',2100,'DeliveryCity','READ'),(46,'FinishedProducts','Warehouse','Receiving','/receiving/grouped','fa fa-object-group',3700,'Inventory','READ'),(47,'FinishedProducts','Despatch','Despatchment','/pindahgudang/grouped/newpacking','fa fa-money',1600,'Despatchment','READ'),(49,'FinishedProducts','Despatch','All','/pindahgudang','fa fa-money',3800,'Despatchment','READ'),(50,'FinishedProducts','Despatch','OrderDelivery','/pindahgudang/grouped/readyloading','fa fa-money',4000,'Despatchment','READ'),(51,'FinishedProducts','Despatch','Shipment','/pindahgudang/grouped/delivering','fa fa-money',4100,'Despatchment','READ'),(52,'FinishedProducts','Despatch','Received','/pindahgudang/grouped/received','fa fa-money',4200,'Despatchment','READ'),(53,'FinishedProducts','Despatch','Delivered','/pindahgudang/grouped/delivered','fa fa-money',4300,'Despatchment','READ'),(54,'FinishedProducts','Despatch','Returned','/pindahgudang/grouped/returned','fa fa-money',4400,'Despatchment','READ'),(55,'FinishedProducts','Dispatch','Returned','/dispatchment/grouped/returned','fa fa-money',5600,'Dispatchment','READ'),(56,'FinishedProducts','Warehouse','Quarantine','/inventory/quarantine/grouped','fa fa-money',3400,'Inventory','READ'),(57,'MasterData','MasterDataCustomer','MasterAdjustment','/master/adjustment','fa fa-money',1700,'MasterAdjustment','READ'),(58,'FinishedProducts','Reports','ReportProperties','/reports/property','fa fa-money',5800,'Report','READ'),(59,'FinishedProducts','Sales','Global','/invoice/combination/global','fa fa-money',4900,'GlobalSales','READ'),(60,'MasterData','MasterDataCustomer','Currency','/master/currency','fa fa-money',2200,'Currency','READ'),(61,'MasterData','MasterDataRawMaterials','RawMaterialsCategories','/rawMaterials/rawMaterialsCategories','fa fa-money',2600,'RawMaterialsCategories','READ'),(62,'RawMaterials','Storehouse','Storehouses','/storehouse/facility','fa fa-money',6400,'Storehouse','READ'),(63,'RawMaterials','RawMaterial','RawMaterials','/rawMaterials','fa fa-money',6300,'RawMaterials','READ'),(66,NULL,'Supplier','Suppliers','/suppliers','fa fa-money',6100,'Supplier','READ'),(67,NULL,'Supplier','Catalog','/suppliers/catalog','fa fa-money',6200,'SupplierCatalog','READ'),(68,'RawMaterials','Purchase','PurchaseRequest','/purchase/request','fa fa-money',5900,'PurchaseRequest','READ'),(69,'RawMaterials','Purchase','PurchaseOrder','/purchase/order','fa fa-money',6000,'PurchaseOrder','READ'),(70,'RawMaterials','Purchase','PurchaseInvoice','/purchase/invoice','fa fa-money',6500,'PurchaseInvoice','READ'),(71,'RawMaterials','Purchase','PurchaseInvoicePayment','/purchase/invoice/payment/groupedBySuppliers','fa fa-money',6600,'PurchaseInvoicePayment','READ'),(72,'RawMaterials','Storing','QC','/qc','fa fa-money',6700,'QC','READ'),(73,NULL,'Production','MaterialRequest','/material/request','fa fa-money',6900,'MaterialRequest','READ'),(74,'RawMaterials','Storing','Stocks','/stocks/allocations','fa fa-money',6800,'Stock','READ'),(75,NULL,'Production','StagingArea','/staging/facility','fa fa-money',7100,'StagingArea','READ'),(76,NULL,'Production','MaterialRelease','/material/release','fa fa-money',7000,'MaterialRelease','READ'),(77,'MasterData','MasterDataDelivery','ShippingTerms','/shipping/terms','fa fa-ship',2510,'ShippingTerms','READ'),(78,'RawMaterials','Storing','Quarantine','/stocks/quarantine','fa fa-money',6810,'Stock','READ');
/*!40000 ALTER TABLE `tbmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpackagingtype`
--

DROP TABLE IF EXISTS `tbpackagingtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpackagingtype` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpackagingtype`
--

LOCK TABLES `tbpackagingtype` WRITE;
/*!40000 ALTER TABLE `tbpackagingtype` DISABLE KEYS */;
INSERT INTO `tbpackagingtype` VALUES (1,'260 X 12','Medium A',_binary ''),(2,'260 X 15','Medium B',_binary ''),(3,'300 X 15','Large',_binary '');
/*!40000 ALTER TABLE `tbpackagingtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpaymenttype`
--

DROP TABLE IF EXISTS `tbpaymenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpaymenttype` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `status` bit(1) DEFAULT b'1',
  `is_giro` bit(1) DEFAULT b'0',
  `is_cash` bit(1) DEFAULT b'0',
  `is_credit` bit(1) DEFAULT b'0',
  `is_transfer` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpaymenttype`
--

LOCK TABLES `tbpaymenttype` WRITE;
/*!40000 ALTER TABLE `tbpaymenttype` DISABLE KEYS */;
INSERT INTO `tbpaymenttype` VALUES (1,'CASH','Pembayaran tunai',_binary '',_binary '\0',_binary '',_binary '\0',_binary '\0'),(2,'TRANSFER PANIN SICMA','Pembayaran transfer melalui bank',_binary '',_binary '\0',_binary '\0',_binary '\0',_binary ''),(3,'GIRO PANIN SICMA','GIRO PANIN SICMA',_binary '',_binary '',_binary '\0',_binary '\0',_binary '\0'),(4,'CREDIT','Pembayaran credit melalui mitra kredit',_binary '',_binary '\0',_binary '\0',_binary '',_binary '\0'),(5,'TRANSFER BCA','TRANSFER BCA',_binary '',_binary '\0',_binary '\0',_binary '\0',_binary '');
/*!40000 ALTER TABLE `tbpaymenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpermission`
--

DROP TABLE IF EXISTS `tbpermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpermission` (
  `action` varchar(32) NOT NULL,
  `business_object` varchar(512) NOT NULL,
  `description` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpermission`
--

LOCK TABLES `tbpermission` WRITE;
/*!40000 ALTER TABLE `tbpermission` DISABLE KEYS */;
INSERT INTO `tbpermission` VALUES ('CREATE','CasingType','description.casingtype.create'),('DELETE','CasingType','description.casingtype.delete'),('READ','CasingType','description.casingtype.read'),('UPDATE','CasingType','description.casingtype.update'),('CREATE','Customer','description.customer.create'),('DELETE','Customer','description.customer.delete'),('IMPORT','Customer','description.customer.import'),('READ','Customer','description.customer.read'),('RESTORE','Customer','description.customer.restore'),('UPDATE','Customer','description.customer.update'),('CREATE','Customer Type','description.customertype.create'),('READ','Customer Type','description.customertype.read'),('UPDATE','Customer Type','description.customertype.update'),('CREATE','Dashboard','description.dashboard.create'),('READ','Dashboard','description.dashboard.read'),('UPDATE','Dashboard','description.dashboard.update'),('CREATE','DeliveryArea','description.deliveryarea.create'),('READ','DeliveryArea','description.deliveryarea.read'),('UPDATE','DeliveryArea','description.deliveryarea.update'),('CREATE','DeliveryCity','description.deliverycity.create'),('DELETE','DeliveryCity','description.deliverycity.delete'),('READ','DeliveryCity','description.deliverycity.read'),('UPDATE','DeliveryCity','description.deliverycity.update'),('CREATE','DeliveryColorCode','description.deliverycolorcode.create'),('DELETE','DeliveryColorCode','description.deliverycolorcode.delete'),('READ','DeliveryColorCode','description.deliverycolorcode.read'),('UPDATE','DeliveryColorCode','description.deliverycolorcode.update'),('CREATE','DeliveryMethod','description.deliverymethod.create'),('DELETE','DeliveryMethod','description.deliverymethod.delete'),('READ','DeliveryMethod','description.deliverymethod.read'),('UPDATE','DeliveryMethod','description.deliverymethod.update'),('CREATE','Dispatchment','description.dispatchment.create'),('DELETE','Dispatchment','description.dispatchment.delete'),('READ','Dispatchment','description.dispatchment.read'),('UPDATE','Dispatchment','description.dispatchment.update'),('CREATE','Driver','description.driver.create'),('READ','Driver','description.driver.read'),('UPDATE','Driver','description.driver.update'),('CREATE','Facility','description.facility.create'),('READ','Facility','description.facility.read'),('UPDATE','Facility','description.facility.update'),('CREATE','File','description.file.create'),('DELETE','File','description.file.delete'),('READ','File','description.file.read'),('UPDATE','File','description.file.update'),('READ','Home','description.home.read'),('CREATE','Inventory','description.inventory.create'),('DELETE','Inventory','description.inventory.delete'),('READ','Inventory','description.inventory.read'),('UPDATE','Inventory','description.inventory.update'),('APPROVE','Invoice','description.invoice.approve'),('CREATE','Invoice','description.invoice.create'),('OVERRIDE','Invoice','description.invoice.override'),('READ','Invoice','description.invoice.read'),('UPDATE','Invoice','description.invoice.update'),('CREATE','KilogramWeight','description.kilogramweight.create'),('DELETE','KilogramWeight','description.kilogramweight.delete'),('READ','KilogramWeight','description.kilogramweight.read'),('UPDATE','KilogramWeight','description.kilogramweight.update'),('CREATE','Location','description.location.create'),('READ','Location','description.location.read'),('UPDATE','Location','description.location.update'),('CREATE','MeatType','description.meattype.create'),('DELETE','MeatType','description.meattype.delete'),('READ','MeatType','description.meattype.read'),('UPDATE','MeatType','description.meattype.update'),('CREATE','Navigation','description.navigation.create'),('READ','Navigation','description.navigation.read'),('CREATE','Organisation','description.organisation.create'),('READ','Organisation','description.organisation.read'),('UPDATE','Organisation','description.organisation.update'),('CREATE','PackagingType','description.PackagingType.create'),('DELETE','PackagingType','description.PackagingType.delete'),('READ','PackagingType','description.PackagingType.read'),('UPDATE','PackagingType','description.PackagingType.update'),('CREATE','Payment','description.Payment.create'),('DELETE','Payment','description.Payment.delete'),('READ','Payment','description.Payment.read'),('CREATE','Payment Type','description.paymenttype.create'),('READ','Payment Type','description.paymenttype.read'),('UPDATE','Payment Type','description.paymenttype.update'),('CREATE','PriceBook','description.pricebook.create'),('DELETE','PriceBook','description.pricebook.delete'),('EXPORT','PriceBook','description.pricebook.export'),('IMPORT','PriceBook','description.pricebook.import'),('READ','PriceBook','description.pricebook.read'),('UPDATE','PriceBook','description.pricebook.update'),('CREATE','Product','description.product.create'),('IMPORT','Product','description.product.import'),('READ','Product','description.product.read'),('UPDATE','Product','description.product.update'),('CREATE','ProductAvailability','description.productavailability.create'),('DELETE','ProductAvailability','description.productavailability.delete'),('READ','ProductAvailability','description.productavailability.read'),('UPDATE','ProductAvailability','description.productavailability.update'),('CREATE','ProductCasing','description.productcasing.create'),('DELETE','ProductCasing','description.productcasing.delete'),('READ','ProductCasing','description.productcasing.read'),('UPDATE','ProductCasing','description.productcasing.update'),('CREATE','ProductCategory','description.productcategory.create'),('READ','ProductCategory','description.productcategory.read'),('UPDATE','ProductCategory','description.productcategory.update'),('CREATE','ProductGroup','description.productgroup.create'),('DELETE','ProductGroup','description.productgroup.delete'),('READ','ProductGroup','description.productgroup.read'),('UPDATE','ProductGroup','description.productgroup.update'),('CREATE','ProductWeight','description.productweight.create'),('DELETE','ProductWeight','description.productweight.delete'),('READ','ProductWeight','description.productweight.read'),('UPDATE','ProductWeight','description.productweight.update'),('CREATE','Refund','description.refund.create'),('DELETE','Refund','description.refund.delete'),('READ','Refund','description.refund.read'),('UPDATE','Refund','description.refund.update'),('READ','Report','description.report.read'),('CREATE','Return','description.return.create'),('DELETE','Return','description.return.delete'),('READ','Return','description.return.read'),('UPDATE','Return','description.return.update'),('CREATE','Role','description.role.create'),('DELETE','Role','description.role.delete'),('READ','Role','description.role.read'),('UPDATE','Role','description.role.update'),('CREATE','Salesperson','description.salesperson.create'),('READ','Salesperson','description.salesperson.read'),('UPDATE','Salesperson','description.salesperson.update'),('ADJUSTMENT','Settlement','description.settlement.adjustment'),('CASH','Settlement','description.settlement.cash'),('CREATE','Settlement','description.settlement.create'),('CREDIT','Settlement','description.settlement.credit'),('DELETE','Settlement','description.settlement.delete'),('GIRO','Settlement','description.settlement.giro'),('READ','Settlement','description.settlement.read'),('TRANSFER','Settlement','description.settlement.transfer'),('CREATE','ShipmentType','description.shipmenttype.create'),('DELETE','ShipmentType','description.shipmenttype.delete'),('READ','ShipmentType','description.shipmenttype.read'),('UPDATE','ShipmentType','description.shipmenttype.update'),('READ','System Properties','description.sysprop.read'),('UPDATE','System Properties','description.sysprop.update'),('CREATE','Terms of Payment','description.tof.create'),('READ','Terms of Payment','description.tof.read'),('UPDATE','Terms of Payment','description.tof.update'),('CREATE','Truck','description.truck.create'),('READ','Truck','description.truck.read'),('UPDATE','Truck','description.truck.update'),('CREATE','Unit of Measurement','description.uof.create'),('READ','Unit of Measurement','description.uof.read'),('UPDATE','Unit of Measurement','description.uof.update'),('CREATE','User','description.user.create'),('READ','User','description.user.read'),('UPDATE','User','description.user.update'),('CREATE','Despatchment','description.despatchment.create'),('DELETE','Despatchment','description.despatchment.delete'),('READ','Despatchment','description.despatchment.read'),('UPDATE','Despatchment','description.despatchment.update'),('CREATE','Repackage','description.repackage.create'),('READ','Repackage','description.repackage.read'),('UPDATE','Repackage','description.repackage.update'),('UPDATE','InvoiceItemWeight','description.InvoiceItemWeight.update'),('UPDATE','InvoiceItemQty','description.InvoiceItemQty.update'),('CREATE','ReceiptReceivedDate','description.ReceiptReceivedDate.create'),('UPDATE','ReceiptReceivedDate','description.ReceiptReceivedDate.update'),('UPDATE','AssignTruck','description.AssignTruck.update'),('UPDATE','InvoiceItemUnitPrice','description.InvoiceItemUnitPrice.update'),('CREATE','MasterAdjustment','description.masterAdjustment.create'),('DELETE','MasterAdjustment','description.masterAdjustment.delete'),('READ','MasterAdjustment','description.masterAdjustment.read'),('UPDATE','MasterAdjustment','description.masterAdjustment.update'),('UPDATE','Payment','description.Payment.update'),('UPDATE','Report','description.report.update'),('READ','GlobalSales','description.GlobalSales.read'),('UPDATE','GlobalSales','description.GlobalSales.update'),('DELETE','GlobalSales','description.GlobalSales.delete'),('CREATE','Currency','description.Currency.create'),('READ','Currency','description.Currency.read'),('UPDATE','Currency','description.Currency.update'),('DELETE','Currency','description.Currency.delete'),('CREATE','RawMaterialsCategories','description.RawMaterialsCategories.create'),('READ','RawMaterialsCategories','description.RawMaterialsCategories.read'),('UPDATE','RawMaterialsCategories','description.RawMaterialsCategories.update'),('DELETE','RawMaterialsCategories','description.RawMaterialsCategories.delete'),('CREATE','Storehouse','description.Storehouse.create'),('READ','Storehouse','description.Storehouse.read'),('UPDATE','Storehouse','description.Storehouse.update'),('DELETE','Storehouse','description.Storehouse.delete'),('CREATE','RawMaterials','description.RawMaterials.create'),('READ','RawMaterials','description.RawMaterials.read'),('UPDATE','RawMaterials','description.RawMaterials.update'),('DELETE','RawMaterials','description.RawMaterials.delete'),('CREATE','Supplier','description.Supplier.create'),('READ','Supplier','description.Supplier.read'),('UPDATE','Supplier','description.Supplier.update'),('DELETE','Supplier','description.Supplier.delete'),('CREATE','SupplierCategories','description.SupplierCategories.create'),('READ','SupplierCategories','description.SupplierCategories.read'),('UPDATE','SupplierCategories','description.SupplierCategories.update'),('DELETE','SupplierCategories','description.SupplierCategories.delete'),('CREATE','SupplierCatalog','description.SupplierCatalog.create'),('READ','SupplierCatalog','description.SupplierCatalog.read'),('UPDATE','SupplierCatalog','description.SupplierCatalog.update'),('DELETE','SupplierCatalog','description.SupplierCatalog.delete'),('CREATE','PurchaseRequest','description.PurchaseRequest.create'),('READ','PurchaseRequest','description.PurchaseRequest.read'),('UPDATE','PurchaseRequest','description.PurchaseRequest.update'),('DELETE','PurchaseRequest','description.PurchaseRequest.delete'),('CREATE','PurchaseOrder','description.PurchaseOrder.create'),('READ','PurchaseOrder','description.PurchaseOrder.read'),('UPDATE','PurchaseOrder','description.PurchaseOrder.update'),('DELETE','PurchaseOrder','description.PurchaseOrder.delete'),('CREATE','PurchaseInvoice','description.PurchaseInvoice.create'),('READ','PurchaseInvoice','description.PurchaseInvoice.read'),('UPDATE','PurchaseInvoice','description.PurchaseInvoice.update'),('DELETE','PurchaseInvoice','description.PurchaseInvoice.delete'),('CREATE','PurchaseInvoicePayment','description.PurchaseInvoicePayment.create'),('READ','PurchaseInvoicePayment','description.PurchaseInvoicePayment.read'),('UPDATE','PurchaseInvoicePayment','description.PurchaseInvoicePayment.update'),('DELETE','PurchaseInvoicePayment','description.PurchaseInvoicePayment.delete'),('CREATE','QC','description.QC.create'),('READ','QC','description.QC.read'),('UPDATE','QC','description.QC.update'),('DELETE','QC','description.QC.delete'),('CREATE','MaterialRequest','description.MaterialRequest.create'),('READ','MaterialRequest','description.MaterialRequest.read'),('UPDATE','MaterialRequest','description.MaterialRequest.update'),('DELETE','MaterialRequest','description.MaterialRequest.delete'),('CREATE','Stock','description.Stock.create'),('READ','Stock','description.Stock.read'),('UPDATE','Stock','description.Stock.update'),('DELETE','Stock','description.Stock.delete'),('CREATE','StagingArea','description.StagingArea.create'),('READ','StagingArea','description.StagingArea.read'),('UPDATE','StagingArea','description.StagingArea.update'),('DELETE','StagingArea','description.StagingArea.delete'),('CREATE','MaterialRelease','description.MaterialRelease.create'),('READ','MaterialRelease','description.MaterialRelease.read'),('UPDATE','MaterialRelease','description.MaterialRelease.update'),('DELETE','MaterialRelease','description.MaterialRelease.delete'),('CREATE','ShippingTerms','description.ShippingTerms.create'),('READ','ShippingTerms','description.ShippingTerms.read'),('UPDATE','ShippingTerms','description.ShippingTerms.update'),('DELETE','ShippingTerms','description.ShippingTerms.delete');
/*!40000 ALTER TABLE `tbpermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpic`
--

DROP TABLE IF EXISTS `tbpic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpic` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `CUSTOMER` bigint NOT NULL,
  `picname` varchar(250) DEFAULT NULL,
  `picposition` varchar(250) DEFAULT NULL,
  `picdirectphone` varchar(250) DEFAULT NULL,
  `picmobilephone` varchar(250) DEFAULT NULL,
  `picemail` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-customer` (`CUSTOMER`),
  CONSTRAINT `fk-customer` FOREIGN KEY (`CUSTOMER`) REFERENCES `tbcustomer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpic`
--

LOCK TABLES `tbpic` WRITE;
/*!40000 ALTER TABLE `tbpic` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbpic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbprodcategory`
--

DROP TABLE IF EXISTS `tbprodcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbprodcategory` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbprodcategory`
--

LOCK TABLES `tbprodcategory` WRITE;
/*!40000 ALTER TABLE `tbprodcategory` DISABLE KEYS */;
INSERT INTO `tbprodcategory` VALUES (1,'Sosis','Sosis'),(2,'Baso','Baso'),(3,'Cold Cut','Daging mentah potongan'),(4,'Salami Pepperoni','Daging cetakan berbumbu'),(5,'Daging','Daging'),(6,'Ham','Daging cetakan besar'),(7,'Daging Asap','Daging Asap');
/*!40000 ALTER TABLE `tbprodcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproduct`
--

DROP TABLE IF EXISTS `tbproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbproduct` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(16) DEFAULT NULL,
  `code_ref_tax` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `short_name` varchar(128) DEFAULT NULL,
  `product_category` bigint DEFAULT NULL,
  `dimension_diameter` decimal(19,4) DEFAULT NULL,
  `dimension_length` decimal(19,4) DEFAULT NULL,
  `content_size` decimal(19,4) DEFAULT NULL,
  `unit_id` bigint DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `status` varchar(128) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  `sale_price` decimal(19,4) DEFAULT NULL,
  `is_raw_material` bit(1) DEFAULT b'0',
  `is_finished_good` bit(1) DEFAULT b'0',
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `meat_type_id` bigint DEFAULT NULL,
  `product_availability_id` bigint DEFAULT NULL,
  `packaging_type_id` bigint DEFAULT NULL,
  `product_group_id` bigint DEFAULT NULL,
  `product_casing_id` bigint DEFAULT NULL,
  `casing_type_id` bigint DEFAULT NULL,
  `product_weight_id` bigint DEFAULT NULL,
  `kilogram_weight_id` bigint DEFAULT NULL,
  `specification1` varchar(255) DEFAULT NULL,
  `specification2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-unit` (`unit_id`),
  KEY `fk-product-productcategory` (`product_category`),
  KEY `fk-product-meattype` (`meat_type_id`),
  KEY `fk-product-availability` (`product_availability_id`),
  KEY `fk-product-packagingtype` (`packaging_type_id`),
  KEY `fk-product-productgroup` (`product_group_id`),
  KEY `fk-product-productcasing` (`product_casing_id`),
  KEY `fk-product-casingtype` (`casing_type_id`),
  KEY `fk-product-productweight` (`product_weight_id`),
  KEY `fk-product-kilogramweight` (`kilogram_weight_id`),
  CONSTRAINT `fk-product-availability` FOREIGN KEY (`product_availability_id`) REFERENCES `tbproductavailability` (`id`),
  CONSTRAINT `fk-product-casingtype` FOREIGN KEY (`casing_type_id`) REFERENCES `tbcasingtype` (`id`),
  CONSTRAINT `fk-product-kilogramweight` FOREIGN KEY (`kilogram_weight_id`) REFERENCES `tbkilogramweight` (`id`),
  CONSTRAINT `fk-product-meattype` FOREIGN KEY (`meat_type_id`) REFERENCES `tbmeattype` (`id`),
  CONSTRAINT `fk-product-packagingtype` FOREIGN KEY (`packaging_type_id`) REFERENCES `tbpackagingtype` (`id`),
  CONSTRAINT `fk-product-productcasing` FOREIGN KEY (`product_casing_id`) REFERENCES `tbproductcasing` (`id`),
  CONSTRAINT `fk-product-productcategory` FOREIGN KEY (`product_category`) REFERENCES `tbprodcategory` (`id`),
  CONSTRAINT `fk-product-productgroup` FOREIGN KEY (`product_group_id`) REFERENCES `tbproductgroup` (`id`),
  CONSTRAINT `fk-product-productweight` FOREIGN KEY (`product_weight_id`) REFERENCES `tbproductweight` (`id`),
  CONSTRAINT `fk-unit` FOREIGN KEY (`unit_id`) REFERENCES `tbmasterunit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproduct`
--

LOCK TABLES `tbproduct` WRITE;
/*!40000 ALTER TABLE `tbproduct` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbproduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproductavailability`
--

DROP TABLE IF EXISTS `tbproductavailability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbproductavailability` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproductavailability`
--

LOCK TABLES `tbproductavailability` WRITE;
/*!40000 ALTER TABLE `tbproductavailability` DISABLE KEYS */;
INSERT INTO `tbproductavailability` VALUES (1,'Ready Stok','Dapat dipesan kapan saja',_binary ''),(2,'Made By Order','Sesuai detail pesanan customer',_binary '');
/*!40000 ALTER TABLE `tbproductavailability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproductcasing`
--

DROP TABLE IF EXISTS `tbproductcasing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbproductcasing` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(240) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproductcasing`
--

LOCK TABLES `tbproductcasing` WRITE;
/*!40000 ALTER TABLE `tbproductcasing` DISABLE KEYS */;
INSERT INTO `tbproductcasing` VALUES (1,'CC','Collagen Casing',_binary ''),(2,'SC','Skin Casing',_binary ''),(3,'Skinless','Tanpa Casing',_binary '');
/*!40000 ALTER TABLE `tbproductcasing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproductfile`
--

DROP TABLE IF EXISTS `tbproductfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbproductfile` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `product` bigint NOT NULL,
  `file` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-productfiletoproduct` (`product`),
  KEY `fk-productfiletofile` (`file`),
  CONSTRAINT `fk-productfiletofile` FOREIGN KEY (`file`) REFERENCES `tbfile` (`id`),
  CONSTRAINT `fk-productfiletoproduct` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproductfile`
--

LOCK TABLES `tbproductfile` WRITE;
/*!40000 ALTER TABLE `tbproductfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbproductfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproductgroup`
--

DROP TABLE IF EXISTS `tbproductgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbproductgroup` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproductgroup`
--

LOCK TABLES `tbproductgroup` WRITE;
/*!40000 ALTER TABLE `tbproductgroup` DISABLE KEYS */;
INSERT INTO `tbproductgroup` VALUES (1,'BREAKFAST','Breakfast Sausage',_binary ''),(2,'FRANKFURTER','Mix Sausage',_binary '');
/*!40000 ALTER TABLE `tbproductgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproductqtyhistory`
--

DROP TABLE IF EXISTS `tbproductqtyhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbproductqtyhistory` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `from_product` bigint NOT NULL,
  `productqty` bigint DEFAULT NULL,
  `movement` bigint NOT NULL,
  `after_movement_stock` decimal(19,2) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-pqhfromproduct` (`from_product`),
  KEY `fk-pqhmovement` (`movement`),
  KEY `fk-pqhproductqty` (`productqty`),
  CONSTRAINT `fk-pqhfromproduct` FOREIGN KEY (`from_product`) REFERENCES `tbproduct` (`id`),
  CONSTRAINT `fk-pqhmovement` FOREIGN KEY (`movement`) REFERENCES `tbinventorymovement` (`id`),
  CONSTRAINT `fk-pqhproductqty` FOREIGN KEY (`productqty`) REFERENCES `tbproductquantity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproductqtyhistory`
--

LOCK TABLES `tbproductqtyhistory` WRITE;
/*!40000 ALTER TABLE `tbproductqtyhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbproductqtyhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproductquantity`
--

DROP TABLE IF EXISTS `tbproductquantity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbproductquantity` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `facility` bigint NOT NULL,
  `product` bigint NOT NULL,
  `stock_quantity` decimal(19,2) DEFAULT NULL,
  `available_quantity` decimal(19,2) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-quantityproduct` (`product`),
  KEY `fk-productquantityfacility` (`facility`),
  CONSTRAINT `fk-productquantityfacility` FOREIGN KEY (`facility`) REFERENCES `tbwarehousefacility` (`id`),
  CONSTRAINT `fk-quantityproduct` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproductquantity`
--

LOCK TABLES `tbproductquantity` WRITE;
/*!40000 ALTER TABLE `tbproductquantity` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbproductquantity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproductweight`
--

DROP TABLE IF EXISTS `tbproductweight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbproductweight` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(240) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproductweight`
--

LOCK TABLES `tbproductweight` WRITE;
/*!40000 ALTER TABLE `tbproductweight` DISABLE KEYS */;
INSERT INTO `tbproductweight` VALUES (1,'200 gr','200 gram',_binary ''),(2,'250 gr','250 gram',_binary ''),(3,'300 gr','300 gram',_binary ''),(4,'320 gr','320 gram',_binary ''),(5,'500 gr','500 gram',_binary ''),(6,'1 KG','1 Kilogram',_binary '');
/*!40000 ALTER TABLE `tbproductweight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseinvoice`
--

DROP TABLE IF EXISTS `tbpurchaseinvoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseinvoice` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_order` bigint NOT NULL,
  `purchase_invoice_sequence` varchar(32) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `purchase_invoice_date` date DEFAULT NULL,
  `purchase_invoice_due_date` date DEFAULT NULL,
  `subtotal` decimal(19,4) DEFAULT NULL,
  `tax` decimal(19,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `total` decimal(19,4) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-poForPi` (`purchase_order`),
  CONSTRAINT `fk-poForPi` FOREIGN KEY (`purchase_order`) REFERENCES `tbpurchaseorder` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseinvoice`
--

LOCK TABLES `tbpurchaseinvoice` WRITE;
/*!40000 ALTER TABLE `tbpurchaseinvoice` DISABLE KEYS */;
INSERT INTO `tbpurchaseinvoice` VALUES (1,1,'S-0000001',2,'2024-11-15','2024-12-15',200000.0000,10.0000,0.0000,220000.0000,'','','admin','2024-11-15 06:21:14','admin','2024-11-18 07:32:10'),(2,2,'S-0000002',3,'2024-11-18','2024-12-18',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-18 07:33:55','admin','2024-11-18 07:34:22'),(3,5,'S-0000003',3,'2024-11-18','2024-12-18',50000.0000,10.0000,0.0000,55000.0000,'','','admin','2024-11-18 07:56:42','admin','2024-11-18 07:57:42'),(4,6,'S-0000004',1,'2024-11-21','2024-12-21',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-21 03:00:08',NULL,'2024-11-21 03:00:08'),(5,7,'S-0000005',1,'2024-11-21','2024-12-21',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-21 03:00:23',NULL,'2024-11-21 03:00:23'),(6,8,'S-0000006',1,'2024-11-21','2024-12-21',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-21 03:00:40',NULL,'2024-11-21 03:00:40'),(7,9,'S-0000007',1,'2024-11-21','2024-12-21',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-21 03:00:55',NULL,'2024-11-21 03:00:55'),(8,10,'S-0000008',1,'2024-11-21','2024-12-21',50000.0000,10.0000,0.0000,55000.0000,'','','admin','2024-11-21 03:01:12',NULL,'2024-11-21 03:01:12'),(9,11,'S-0000009',5,'2024-12-03','2025-01-02',545900.0000,0.0000,0.0000,545900.0000,'','','admin','2024-12-03 07:29:32','admin','2025-02-17 22:32:21'),(10,22,'S-0000010',1,'2024-12-27','2025-01-26',100000.0000,11.0000,0.0000,111000.0000,'','','admin','2024-12-27 06:57:19',NULL,'2024-12-27 06:57:19'),(11,24,'S-0000011',1,'2024-12-30','2025-01-29',40400.0000,11.0000,0.0000,44844.0000,'','','admin','2024-12-30 04:25:24',NULL,'2024-12-30 04:25:24'),(12,25,'S-0000012',1,'2024-12-30','2025-01-29',100000.0000,11.0000,0.0000,111000.0000,'','','admin','2024-12-30 04:29:26',NULL,'2024-12-30 04:29:26');
/*!40000 ALTER TABLE `tbpurchaseinvoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseinvoiceexchange`
--

DROP TABLE IF EXISTS `tbpurchaseinvoiceexchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseinvoiceexchange` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_invoice_exchange_sequence` varchar(32) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseinvoiceexchange`
--

LOCK TABLES `tbpurchaseinvoiceexchange` WRITE;
/*!40000 ALTER TABLE `tbpurchaseinvoiceexchange` DISABLE KEYS */;
INSERT INTO `tbpurchaseinvoiceexchange` VALUES (1,'TF-25000001','admin','2025-02-17 22:32:21',NULL,NULL);
/*!40000 ALTER TABLE `tbpurchaseinvoiceexchange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseinvoiceexchangeitem`
--

DROP TABLE IF EXISTS `tbpurchaseinvoiceexchangeitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseinvoiceexchangeitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_invoice_exchange` bigint NOT NULL,
  `purchase_invoice` bigint NOT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-pieForPieItem` (`purchase_invoice_exchange`),
  KEY `fk-piForPieItem` (`purchase_invoice`),
  CONSTRAINT `fk-pieForPieItem` FOREIGN KEY (`purchase_invoice_exchange`) REFERENCES `tbpurchaseinvoiceexchange` (`id`),
  CONSTRAINT `fk-piForPieItem` FOREIGN KEY (`purchase_invoice`) REFERENCES `tbpurchaseinvoice` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseinvoiceexchangeitem`
--

LOCK TABLES `tbpurchaseinvoiceexchangeitem` WRITE;
/*!40000 ALTER TABLE `tbpurchaseinvoiceexchangeitem` DISABLE KEYS */;
INSERT INTO `tbpurchaseinvoiceexchangeitem` VALUES (1,1,9,'','admin','2025-02-17 22:32:21',NULL,NULL);
/*!40000 ALTER TABLE `tbpurchaseinvoiceexchangeitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseinvoiceitem`
--

DROP TABLE IF EXISTS `tbpurchaseinvoiceitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseinvoiceitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_invoice` bigint NOT NULL,
  `purchase_order_item` bigint NOT NULL,
  `status` int DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-piForPii` (`purchase_invoice`),
  KEY `fk-poiForPii` (`purchase_order_item`),
  CONSTRAINT `fk-piForPii` FOREIGN KEY (`purchase_invoice`) REFERENCES `tbpurchaseinvoice` (`id`),
  CONSTRAINT `fk-poiForPii` FOREIGN KEY (`purchase_order_item`) REFERENCES `tbpurchaseorderitem` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseinvoiceitem`
--

LOCK TABLES `tbpurchaseinvoiceitem` WRITE;
/*!40000 ALTER TABLE `tbpurchaseinvoiceitem` DISABLE KEYS */;
INSERT INTO `tbpurchaseinvoiceitem` VALUES (1,1,1,3,'','2024-11-15','admin','2024-11-15 06:21:14','admin','2024-11-18 07:32:10'),(2,1,2,3,'','2024-11-15','admin','2024-11-15 06:21:14','admin','2024-11-18 07:32:10'),(3,2,3,3,'','2024-11-18','admin','2024-11-18 07:33:55','admin','2024-11-18 07:34:22'),(4,3,6,3,'','2024-11-18','admin','2024-11-18 07:56:42','admin','2024-11-18 07:57:42'),(5,4,7,1,'','2024-11-21','admin','2024-11-21 03:00:08',NULL,'2024-11-21 03:00:08'),(6,5,8,1,'','2024-11-21','admin','2024-11-21 03:00:23',NULL,'2024-11-21 03:00:23'),(7,6,9,1,'','2024-11-21','admin','2024-11-21 03:00:40',NULL,'2024-11-21 03:00:40'),(8,7,10,1,'','2024-11-21','admin','2024-11-21 03:00:55',NULL,'2024-11-21 03:00:55'),(9,8,11,1,'','2024-11-21','admin','2024-11-21 03:01:12',NULL,'2024-11-21 03:01:12'),(10,9,12,5,'','2024-12-03','admin','2024-12-03 07:29:32','admin','2025-02-17 22:32:21'),(11,9,13,5,'','2024-12-03','admin','2024-12-03 07:29:32','admin','2025-02-17 22:32:21'),(12,9,14,5,'','2024-12-03','admin','2024-12-03 07:29:32','admin','2025-02-17 22:32:21'),(13,9,15,5,'','2024-12-03','admin','2024-12-03 07:29:32','admin','2025-02-17 22:32:21'),(14,10,34,1,'','2024-12-28','admin','2024-12-27 06:57:19',NULL,'2024-12-27 06:57:19'),(15,11,36,1,'','2024-12-31','admin','2024-12-30 04:25:24',NULL,'2024-12-30 04:25:24'),(16,12,37,1,'','2024-12-30','admin','2024-12-30 04:29:26',NULL,'2024-12-30 04:29:26');
/*!40000 ALTER TABLE `tbpurchaseinvoiceitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseinvoicepayment`
--

DROP TABLE IF EXISTS `tbpurchaseinvoicepayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseinvoicepayment` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `supplier` bigint NOT NULL,
  `payment_type` bigint NOT NULL,
  `status` int DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `giro_date` date DEFAULT NULL,
  `giro_number` varchar(32) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `transfer_fee_description` varchar(250) DEFAULT NULL,
  `transfer_fee` decimal(19,4) DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-pip-supplier` (`supplier`),
  KEY `fk-pip-paymentType` (`payment_type`),
  CONSTRAINT `fk-pip-paymentType` FOREIGN KEY (`payment_type`) REFERENCES `tbpaymenttype` (`id`),
  CONSTRAINT `fk-pip-supplier` FOREIGN KEY (`supplier`) REFERENCES `tbsupplier` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseinvoicepayment`
--

LOCK TABLES `tbpurchaseinvoicepayment` WRITE;
/*!40000 ALTER TABLE `tbpurchaseinvoicepayment` DISABLE KEYS */;
INSERT INTO `tbpurchaseinvoicepayment` VALUES (1,1,3,1,'2024-11-18','2024-11-18','3234983322','','',0.0000,200000.0000,'admin','2024-11-18 07:32:10',NULL,'2024-11-18 07:32:10'),(2,2,3,1,'2024-11-18','2024-11-18','33221112212','','',0.0000,110000.0000,'admin','2024-11-18 07:34:22',NULL,'2024-11-18 07:34:22'),(3,2,3,1,'2024-11-18','2024-11-18','33221112212','','',2000.0000,53000.0000,'admin','2024-11-18 07:57:42',NULL,'2024-11-18 07:57:42');
/*!40000 ALTER TABLE `tbpurchaseinvoicepayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseinvoicepaymentadjustment`
--

DROP TABLE IF EXISTS `tbpurchaseinvoicepaymentadjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseinvoicepaymentadjustment` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_invoice_payment` bigint NOT NULL,
  `adjustment_type` bigint NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-pipAdjustment-pip` (`purchase_invoice_payment`),
  KEY `fk-pipAdjustmentType-pip` (`adjustment_type`),
  CONSTRAINT `fk-pipAdjustment-pip` FOREIGN KEY (`purchase_invoice_payment`) REFERENCES `tbpurchaseinvoicepayment` (`id`),
  CONSTRAINT `fk-pipAdjustmentType-pip` FOREIGN KEY (`adjustment_type`) REFERENCES `tbmasteradjustment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseinvoicepaymentadjustment`
--

LOCK TABLES `tbpurchaseinvoicepaymentadjustment` WRITE;
/*!40000 ALTER TABLE `tbpurchaseinvoicepaymentadjustment` DISABLE KEYS */;
INSERT INTO `tbpurchaseinvoicepaymentadjustment` VALUES (1,3,1,'ADMIN FEEE',2000.0000);
/*!40000 ALTER TABLE `tbpurchaseinvoicepaymentadjustment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseinvoicepaymentitem`
--

DROP TABLE IF EXISTS `tbpurchaseinvoicepaymentitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseinvoicepaymentitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_invoice_payment` bigint NOT NULL,
  `purchase_invoice` bigint NOT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-pipi-pi` (`purchase_invoice`),
  KEY `fk-pipi-pip` (`purchase_invoice_payment`),
  CONSTRAINT `fk-pipi-pi` FOREIGN KEY (`purchase_invoice`) REFERENCES `tbpurchaseinvoice` (`id`),
  CONSTRAINT `fk-pipi-pip` FOREIGN KEY (`purchase_invoice_payment`) REFERENCES `tbpurchaseinvoicepayment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseinvoicepaymentitem`
--

LOCK TABLES `tbpurchaseinvoicepaymentitem` WRITE;
/*!40000 ALTER TABLE `tbpurchaseinvoicepaymentitem` DISABLE KEYS */;
INSERT INTO `tbpurchaseinvoicepaymentitem` VALUES (1,1,1,200000.0000,'admin','2024-11-18 07:32:10',NULL,'2024-11-18 07:32:10'),(2,2,2,110000.0000,'admin','2024-11-18 07:34:22',NULL,'2024-11-18 07:34:22'),(3,3,3,55000.0000,'admin','2024-11-18 07:57:42',NULL,'2024-11-18 07:57:42');
/*!40000 ALTER TABLE `tbpurchaseinvoicepaymentitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseorder`
--

DROP TABLE IF EXISTS `tbpurchaseorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseorder` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_request` bigint NOT NULL,
  `supplier` bigint NOT NULL,
  `po_sequence` varchar(32) DEFAULT NULL,
  `invoice_status` int DEFAULT NULL,
  `delivery_status` int DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `subtotal` decimal(19,4) DEFAULT NULL,
  `tax` decimal(19,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `total` decimal(19,4) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-poForPr` (`purchase_request`),
  KEY `fk-supplierForPr` (`supplier`),
  CONSTRAINT `fk-poForPr` FOREIGN KEY (`purchase_request`) REFERENCES `tbpurchaserequest` (`id`),
  CONSTRAINT `fk-supplierForPr` FOREIGN KEY (`supplier`) REFERENCES `tbsupplier` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseorder`
--

LOCK TABLES `tbpurchaseorder` WRITE;
/*!40000 ALTER TABLE `tbpurchaseorder` DISABLE KEYS */;
INSERT INTO `tbpurchaseorder` VALUES (1,1,1,'S-0000001',4,4,'2024-11-15','2024-11-15',200000.0000,10.0000,0.0000,220000.0000,'','','admin','2024-11-15 06:20:35','admin','2024-11-18 07:32:10'),(2,2,2,'S-0000002',5,4,'2024-11-15','2024-11-15',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-15 07:12:00','admin','2024-11-18 07:34:22'),(3,3,2,'S-0000003',2,4,'2024-11-18','2024-11-18',100000.0000,10.0000,0.0000,110000.0000,'','',NULL,NULL,'admin','2024-11-21 09:05:22'),(4,4,2,'S-0000004',2,2,'2024-11-18','2024-11-18',50000.0000,10.0000,0.0000,55000.0000,'','',NULL,NULL,'admin','2024-12-27 02:07:24'),(5,5,2,'S-0000005',5,2,'2024-11-18','2024-11-18',50000.0000,10.0000,0.0000,55000.0000,'','','admin','2024-11-18 07:56:19','admin','2024-11-18 07:57:42'),(6,6,1,'S-0000006',3,2,'2024-11-21','2024-11-21',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-21 02:57:19','admin','2024-11-21 03:00:08'),(7,7,1,'S-0000007',3,2,'2024-11-21','2024-11-21',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-21 02:57:41','admin','2024-11-21 03:00:23'),(8,10,1,'S-0000008',3,2,'2024-11-21','2024-11-21',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-21 02:58:52','admin','2024-11-21 03:00:40'),(9,11,1,'S-0000009',3,2,'2024-11-21','2024-11-21',100000.0000,10.0000,0.0000,110000.0000,'','','admin','2024-11-21 02:59:11','admin','2024-11-21 03:00:55'),(10,12,1,'S-0000010',3,2,'2024-11-21','2024-11-21',50000.0000,10.0000,0.0000,55000.0000,'','','admin','2024-11-21 02:59:36','admin','2024-11-21 03:01:12'),(11,13,29,'S-0000011',7,2,'2024-11-26','2024-11-26',545900.0000,0.0000,0.0000,545900.0000,'','','admin','2024-11-26 04:23:20','admin','2025-02-17 22:32:21'),(12,14,29,'S-0000012',2,2,'2024-01-01','2024-11-30',583400.0000,0.0000,0.0000,583400.0000,'','',NULL,NULL,'admin','2024-12-27 02:08:08'),(13,15,2,'S-0000013',2,2,'2024-11-18','2024-11-18',50000.0000,10.0000,0.0000,55000.0000,'','',NULL,NULL,'admin','2024-11-28 06:06:54'),(14,16,1,'S-0000014',2,2,'2024-12-05','2024-12-05',250000.0000,10.0000,0.0000,275000.0000,'','','admin','2024-12-05 08:19:28',NULL,NULL),(15,17,3,'S-0000015',2,2,'2024-12-12','2024-12-12',120200.0000,10.0000,0.0000,132220.0000,'','','admin','2024-12-12 07:02:53',NULL,NULL),(16,18,3,'S-0000016',2,2,'2024-12-16','2024-12-16',140400.0000,10.0000,0.0000,154440.0000,'','','admin','2024-12-16 02:22:31',NULL,NULL),(17,19,1,'S-0000017',2,2,'2024-12-17','2024-12-28',300000.0000,10.0000,0.0000,330000.0000,'','','admin','2024-12-17 03:57:50',NULL,NULL),(18,20,2,'S-0000018',2,2,'2024-12-21','2024-12-21',100000.0000,10.0000,0.0000,110000.0000,'','',NULL,NULL,'admin','2024-12-19 03:40:53'),(19,21,3,'S-0000019',2,2,'2024-12-19','2024-12-27',20200.0000,10.0000,0.0000,22220.0000,'','',NULL,NULL,'admin','2024-12-23 04:06:21'),(20,22,3,'S-0000020',2,2,'2024-12-25','2024-12-31',430400.0000,10.0000,0.0000,473440.0000,'','',NULL,NULL,'admin','2024-12-27 02:07:15'),(21,23,3,'S-0000021',2,2,'2024-12-27','2024-12-30',40400.0000,11.0000,0.0000,44844.0000,'','',NULL,NULL,'admin','2024-12-27 06:51:13'),(22,24,3,'S-0000022',3,2,'2024-12-27','2024-12-27',100000.0000,11.0000,0.0000,111000.0000,'','','admin','2024-12-27 06:57:02','admin','2024-12-27 06:57:19'),(23,25,3,'S-0000023',2,2,'2024-12-28','2024-12-31',40400.0000,11.0000,0.0000,44844.0000,'','',NULL,NULL,'admin','2024-12-27 07:28:39'),(24,26,3,'S-0000024',3,2,'2024-12-30','2024-12-31',40400.0000,11.0000,0.0000,44844.0000,'','',NULL,NULL,'admin','2024-12-30 04:25:24'),(25,27,2,'S-0000025',3,2,'2024-12-30','2024-12-31',100000.0000,11.0000,0.0000,111000.0000,'','',NULL,NULL,'admin','2024-12-30 04:29:26');
/*!40000 ALTER TABLE `tbpurchaseorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseorderitem`
--

DROP TABLE IF EXISTS `tbpurchaseorderitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseorderitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_order` bigint NOT NULL,
  `purchase_request_item` bigint NOT NULL,
  `currency` bigint NOT NULL,
  `invoice_status` int DEFAULT NULL,
  `delivery_status` int DEFAULT NULL,
  `exchange_rate` decimal(19,4) DEFAULT NULL,
  `weight` decimal(19,4) DEFAULT NULL,
  `unit_price` decimal(19,4) DEFAULT NULL,
  `total_price` decimal(19,4) DEFAULT NULL,
  `total_amount` decimal(19,4) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-currencyForPoi` (`currency`),
  KEY `fk-poiForPo` (`purchase_order`),
  KEY `fk-poiForPri` (`purchase_request_item`),
  CONSTRAINT `fk-currencyForPoi` FOREIGN KEY (`currency`) REFERENCES `tbcurrency` (`id`),
  CONSTRAINT `fk-poiForPo` FOREIGN KEY (`purchase_order`) REFERENCES `tbpurchaseorder` (`id`),
  CONSTRAINT `fk-poiForPri` FOREIGN KEY (`purchase_request_item`) REFERENCES `tbpurchaserequestitem` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseorderitem`
--

LOCK TABLES `tbpurchaseorderitem` WRITE;
/*!40000 ALTER TABLE `tbpurchaseorderitem` DISABLE KEYS */;
INSERT INTO `tbpurchaseorderitem` VALUES (1,1,1,1,5,4,1.0000,2.0000,50000.0000,100000.0000,100000.0000,'','2024-11-15','admin','2024-11-15 06:20:35','admin','2024-11-18 07:32:10'),(2,1,2,1,5,4,1.0000,2.0000,50000.0000,100000.0000,100000.0000,'','2024-11-15','admin','2024-11-15 06:20:35','admin','2024-11-18 07:32:10'),(3,2,3,1,5,4,1.0000,20.0000,50000.0000,100000.0000,100000.0000,'','2024-11-15','admin','2024-11-15 07:12:00','admin','2024-11-18 07:34:22'),(4,3,4,1,0,4,1.0000,20.0000,50000.0000,100000.0000,100000.0000,'','2024-11-18',NULL,NULL,'admin','2024-11-21 09:05:22'),(5,4,5,1,0,0,1.0000,10.0000,50000.0000,50000.0000,50000.0000,'','2024-11-18',NULL,NULL,'admin','2024-12-27 02:07:24'),(6,5,6,1,5,2,1.0000,10.0000,50000.0000,50000.0000,50000.0000,'','2024-11-18','admin','2024-11-18 07:56:19','admin','2024-11-18 07:57:42'),(7,6,7,1,3,2,1.0000,2.0000,50000.0000,100000.0000,100000.0000,'','2024-11-21','admin','2024-11-21 02:57:19','admin','2024-11-21 03:00:08'),(8,7,8,1,3,2,1.0000,2.0000,50000.0000,100000.0000,100000.0000,'','2024-11-21','admin','2024-11-21 02:57:41','admin','2024-11-21 03:00:23'),(9,8,10,1,3,2,1.0000,2.0000,50000.0000,100000.0000,100000.0000,'','2024-11-21','admin','2024-11-21 02:58:52','admin','2024-11-21 03:00:40'),(10,9,11,1,3,2,1.0000,2.0000,50000.0000,100000.0000,100000.0000,'','2024-11-21','admin','2024-11-21 02:59:11','admin','2024-11-21 03:00:55'),(11,10,12,1,3,2,1.0000,1.0000,50000.0000,50000.0000,50000.0000,'','2024-11-21','admin','2024-11-21 02:59:36','admin','2024-11-21 03:01:12'),(12,11,13,1,7,3,1.0000,2.0000,50000.0000,100000.0000,100000.0000,'','2024-11-27','admin','2024-11-26 04:23:20','admin','2025-02-17 22:32:21'),(13,11,14,1,7,3,1.0000,3.0000,25500.0000,76500.0000,76500.0000,'','2024-11-27','admin','2024-11-26 04:23:20','admin','2025-02-17 22:32:21'),(14,11,15,1,7,2,1.0000,2.0000,22200.0000,44400.0000,44400.0000,'','2024-11-27','admin','2024-11-26 04:23:20','admin','2025-02-17 22:32:21'),(15,11,16,1,7,2,1.0000,5.0000,65000.0000,325000.0000,325000.0000,'','2024-11-27','admin','2024-11-26 04:23:20','admin','2025-02-17 22:32:21'),(16,12,17,1,0,0,1.0000,2.0000,32000.0000,64000.0000,64000.0000,'','2024-11-29',NULL,NULL,'admin','2024-12-27 02:08:08'),(17,12,18,1,0,0,1.0000,2.0000,22200.0000,44400.0000,44400.0000,'','2024-11-29',NULL,NULL,'admin','2024-12-27 02:08:08'),(18,12,19,1,0,0,1.0000,5.0000,65000.0000,325000.0000,325000.0000,'','2024-11-29',NULL,NULL,'admin','2024-12-27 02:08:08'),(19,12,20,1,0,0,1.0000,3.0000,50000.0000,150000.0000,150000.0000,'','2024-11-29',NULL,NULL,'admin','2024-12-27 02:08:08'),(20,13,21,1,0,0,1.0000,10.0000,50000.0000,50000.0000,50000.0000,'','2024-11-29',NULL,NULL,'admin','2024-11-28 06:06:54'),(21,14,22,1,2,2,1.0000,5.0000,50000.0000,250000.0000,250000.0000,'','2024-12-05','admin','2024-12-05 08:19:28',NULL,NULL),(22,15,23,1,2,2,1.0000,1.0000,20200.0000,20200.0000,20200.0000,'','2024-12-12','admin','2024-12-12 07:02:53',NULL,NULL),(23,15,24,1,2,2,1.0000,1.0000,50000.0000,50000.0000,50000.0000,'','2024-12-19','admin','2024-12-12 07:02:53',NULL,NULL),(24,15,25,1,2,2,1.0000,1.0000,50000.0000,50000.0000,50000.0000,'','2024-12-12','admin','2024-12-12 07:02:53',NULL,NULL),(25,16,26,1,2,2,1.0000,2.0000,20200.0000,40400.0000,40400.0000,'','2024-12-16','admin','2024-12-16 02:22:31',NULL,NULL),(26,16,27,1,2,2,1.0000,2.0000,50000.0000,100000.0000,100000.0000,'','2024-12-16','admin','2024-12-16 02:22:31',NULL,NULL),(27,17,28,1,2,2,1.0000,3.0000,50000.0000,150000.0000,150000.0000,'','2024-12-26','admin','2024-12-17 03:57:50',NULL,NULL),(28,17,29,1,2,2,1.0000,3.0000,50000.0000,150000.0000,150000.0000,'','2024-12-26','admin','2024-12-17 03:57:50',NULL,NULL),(29,18,30,1,0,0,1.0000,20.0000,50000.0000,100000.0000,100000.0000,'','2024-12-27',NULL,NULL,'admin','2024-12-19 03:40:53'),(30,19,31,1,0,0,1.0000,1.0000,20200.0000,20200.0000,20200.0000,'','2024-12-18',NULL,NULL,'admin','2024-12-23 04:06:21'),(31,20,32,1,0,0,1.0000,2.0000,20200.0000,40400.0000,40400.0000,'','2024-12-26',NULL,NULL,'admin','2024-12-27 02:07:15'),(32,20,33,1,0,0,1.0000,6.0000,65000.0000,390000.0000,390000.0000,'','2024-12-26',NULL,NULL,'admin','2024-12-27 02:07:15'),(33,21,34,1,0,0,1.0000,2.0000,20200.0000,40400.0000,40400.0000,'','2024-12-28',NULL,NULL,'admin','2024-12-27 06:51:13'),(34,22,35,1,3,2,1.0000,2.0000,50000.0000,100000.0000,100000.0000,'','2024-12-27','admin','2024-12-27 06:57:02','admin','2024-12-27 06:57:19'),(35,23,36,1,0,0,1.0000,2.0000,20200.0000,40400.0000,40400.0000,'','2024-12-31',NULL,NULL,'admin','2024-12-27 07:28:39'),(36,24,37,1,3,2,1.0000,2.0000,20200.0000,40400.0000,40400.0000,'','2024-12-31',NULL,NULL,'admin','2024-12-30 04:25:24'),(37,25,38,1,3,2,1.0000,20.0000,50000.0000,100000.0000,100000.0000,'','2024-12-31',NULL,NULL,'admin','2024-12-30 04:29:26');
/*!40000 ALTER TABLE `tbpurchaseorderitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaseordershippingtermsremarks`
--

DROP TABLE IF EXISTS `tbpurchaseordershippingtermsremarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaseordershippingtermsremarks` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_order` bigint NOT NULL,
  `shipping_terms_remarks` bigint NOT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-poForPoStr` (`purchase_order`),
  KEY `fk-strForPoStr` (`shipping_terms_remarks`),
  CONSTRAINT `fk-poForPoStr` FOREIGN KEY (`purchase_order`) REFERENCES `tbpurchaseorder` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-strForPoStr` FOREIGN KEY (`shipping_terms_remarks`) REFERENCES `tbshippingtermsremarks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaseordershippingtermsremarks`
--

LOCK TABLES `tbpurchaseordershippingtermsremarks` WRITE;
/*!40000 ALTER TABLE `tbpurchaseordershippingtermsremarks` DISABLE KEYS */;
INSERT INTO `tbpurchaseordershippingtermsremarks` VALUES (1,16,4,'admin','2024-12-16 02:22:31'),(2,16,7,'admin','2024-12-16 02:22:31'),(3,17,4,'admin','2024-12-17 03:57:50'),(4,17,5,'admin','2024-12-17 03:57:50'),(5,17,6,'admin','2024-12-17 03:57:50'),(6,17,7,'admin','2024-12-17 03:57:50'),(15,18,1,'admin','2024-12-19 03:40:53'),(16,18,6,'admin','2024-12-19 03:40:53'),(17,18,7,'admin','2024-12-19 03:40:53'),(21,19,1,'admin','2024-12-23 04:06:21'),(22,19,4,'admin','2024-12-23 04:06:21'),(28,20,1,'admin','2024-12-27 02:07:15'),(29,20,2,'admin','2024-12-27 02:07:15'),(30,20,3,'admin','2024-12-27 02:07:15'),(31,20,4,'admin','2024-12-27 02:07:15'),(32,20,5,'admin','2024-12-27 02:07:15'),(33,20,6,'admin','2024-12-27 02:07:15'),(34,20,7,'admin','2024-12-27 02:07:15'),(35,20,8,'admin','2024-12-27 02:07:15'),(36,4,4,'admin','2024-12-27 02:07:24'),(37,4,5,'admin','2024-12-27 02:07:24'),(38,4,6,'admin','2024-12-27 02:07:24'),(39,4,7,'admin','2024-12-27 02:07:24'),(40,12,1,'admin','2024-12-27 02:08:08'),(41,12,2,'admin','2024-12-27 02:08:08'),(42,12,3,'admin','2024-12-27 02:08:08'),(43,21,2,'admin','2024-12-27 06:51:03'),(44,21,3,'admin','2024-12-27 06:51:03'),(45,22,1,'admin','2024-12-27 06:57:02'),(46,23,1,'admin','2024-12-27 07:28:09'),(48,23,3,'admin','2024-12-27 07:28:09'),(49,24,1,'admin','2024-12-30 04:24:46'),(51,25,1,'admin','2024-12-30 04:28:54'),(52,25,2,'admin','2024-12-30 04:29:14');
/*!40000 ALTER TABLE `tbpurchaseordershippingtermsremarks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaserequest`
--

DROP TABLE IF EXISTS `tbpurchaserequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaserequest` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `pr_sequence` varchar(32) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `request_date` date DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaserequest`
--

LOCK TABLES `tbpurchaserequest` WRITE;
/*!40000 ALTER TABLE `tbpurchaserequest` DISABLE KEYS */;
INSERT INTO `tbpurchaserequest` VALUES (1,'S-0000001',3,'','2024-11-15','2024-11-15',NULL,'admin','2024-11-15 06:15:26','admin','2024-11-15 06:20:35'),(2,'S-0000002',4,'','2024-11-15','2024-11-15',NULL,'admin','2024-11-15 07:11:38','admin','2024-11-18 07:34:22'),(3,'S-0000003',3,'','2024-11-18','2024-11-18',NULL,'admin','2024-11-18 07:38:50','admin','2024-11-18 07:39:10'),(4,'S-0000004',3,'','2024-11-18','2024-11-18',NULL,'admin','2024-11-18 07:47:45','admin','2024-11-18 07:50:41'),(5,'S-0000005',3,'','2024-11-18','2024-11-18',NULL,'admin','2024-11-18 07:55:53','admin','2024-11-18 07:56:19'),(6,'S-0000006',3,'','2024-11-21','2024-11-21',NULL,'admin','2024-11-21 02:53:40','admin','2024-11-21 02:57:19'),(7,'S-0000007',3,'','2024-11-21','2024-11-21',NULL,'admin','2024-11-21 02:53:51','admin','2024-11-21 02:57:41'),(8,'S-0000008',2,'','2024-11-22','2024-11-22',NULL,'admin','2024-11-21 02:54:02',NULL,'2024-11-21 02:54:02'),(9,'S-0000009',2,'','2024-11-23','2024-11-23',NULL,'admin','2024-11-21 02:55:05',NULL,'2024-11-21 02:55:05'),(10,'S-0000010',3,'','2024-11-22','2024-11-22',NULL,'admin','2024-11-21 02:56:08','admin','2024-11-21 02:58:52'),(11,'S-0000011',3,'','2024-11-21','2024-11-21',NULL,'admin','2024-11-21 02:56:29','admin','2024-11-21 02:59:11'),(12,'S-0000012',3,'','2024-11-21','2024-11-21',NULL,'admin','2024-11-21 02:56:48','admin','2024-11-21 02:59:36'),(13,'S-0000013',3,'','2024-11-26','2024-11-26',NULL,'admin','2024-11-26 04:20:47','admin','2024-11-26 04:23:20'),(14,'S-0000014',3,'','2024-01-09','2024-12-31',NULL,'admin','2024-11-26 06:20:39','admin','2024-11-26 06:21:34'),(15,'S-0000015',3,'','2024-11-18','2024-11-18',NULL,'admin','2024-11-28 06:05:37','admin','2024-11-28 06:06:15'),(16,'S-0000016',3,'','2024-12-12','2024-12-19',NULL,'admin','2024-12-05 03:52:51','admin','2024-12-05 08:19:28'),(17,'S-0000017',3,'','2024-12-12','2024-12-05',NULL,'admin','2024-12-12 06:58:55','admin','2024-12-12 07:02:53'),(18,'S-0000018',3,'','2024-12-16','2024-12-25',NULL,'admin','2024-12-16 02:06:24','admin','2024-12-16 02:22:31'),(19,'S-0000019',3,'','2024-12-17','2024-12-28','','admin','2024-12-17 03:55:54','admin','2024-12-17 03:57:50'),(20,'S-0000020',3,'','2024-12-19','2024-12-21',NULL,'admin','2024-12-17 04:41:10','admin','2024-12-17 04:42:14'),(21,'S-0000021',3,'','2024-12-18','2024-12-27','','admin','2024-12-18 08:04:02','admin','2024-12-19 03:42:17'),(22,'S-0000022',3,'','2024-12-26','2024-12-31','','admin','2024-12-23 06:27:07','admin','2024-12-23 06:31:11'),(23,'S-0000023',3,'','2024-12-26','2024-12-30',NULL,'admin','2024-12-27 06:50:33','admin','2024-12-27 06:51:03'),(24,'S-0000024',3,'','2024-12-27','2024-12-27',NULL,'admin','2024-12-27 06:56:30','admin','2024-12-27 06:57:02'),(25,'S-0000025',3,'','2024-12-27','2024-12-31',NULL,'admin','2024-12-27 07:27:07','admin','2024-12-27 07:28:09'),(26,'S-0000026',3,'','2024-12-30','2024-12-31',NULL,'admin','2024-12-30 04:24:00','admin','2024-12-30 04:24:46'),(27,'S-0000027',3,'','2024-12-30','2024-12-31',NULL,'admin','2024-12-30 04:28:13','admin','2024-12-30 04:28:54');
/*!40000 ALTER TABLE `tbpurchaserequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpurchaserequestitem`
--

DROP TABLE IF EXISTS `tbpurchaserequestitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbpurchaserequestitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_request` bigint NOT NULL,
  `raw_material` bigint NOT NULL,
  `supplier` bigint DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-supplierForPrI` (`supplier`),
  KEY `fk-rmForPrI` (`raw_material`),
  KEY `fk-prForPrI` (`purchase_request`),
  CONSTRAINT `fk-prForPrI` FOREIGN KEY (`purchase_request`) REFERENCES `tbpurchaserequest` (`id`),
  CONSTRAINT `fk-rmForPrI` FOREIGN KEY (`raw_material`) REFERENCES `tbrawmaterials` (`id`),
  CONSTRAINT `fk-supplierForPrI` FOREIGN KEY (`supplier`) REFERENCES `tbsupplier` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpurchaserequestitem`
--

LOCK TABLES `tbpurchaserequestitem` WRITE;
/*!40000 ALTER TABLE `tbpurchaserequestitem` DISABLE KEYS */;
INSERT INTO `tbpurchaserequestitem` VALUES (1,1,2,1,2,3,'2024-11-15'),(2,1,4,1,2,3,'2024-11-15'),(3,2,1,2,2,4,'2024-11-15'),(4,3,1,2,2,3,'2024-11-18'),(5,4,1,2,1,3,'2024-11-18'),(6,5,1,2,1,3,'2024-11-18'),(7,6,2,1,2,3,'2024-11-21'),(8,7,4,1,2,3,'2024-11-21'),(9,9,1,NULL,2,2,'2024-11-21'),(10,10,2,1,2,3,'2024-11-21'),(11,11,4,1,2,3,'2024-11-21'),(12,12,4,1,1,3,'2024-11-21'),(13,13,26,29,2,3,'2024-11-27'),(14,13,17,29,3,3,'2024-11-27'),(15,13,19,29,2,3,'2024-11-27'),(16,13,21,29,5,3,'2024-11-27'),(17,14,16,29,2,3,'2024-11-29'),(18,14,20,29,2,3,'2024-11-29'),(19,14,21,29,5,3,'2024-11-29'),(20,14,26,29,3,3,'2024-11-29'),(21,15,1,2,1,3,'2024-11-28'),(22,16,4,1,5,3,'2024-12-05'),(23,17,20,3,1,3,'2024-12-12'),(24,17,4,3,1,3,'2024-12-12'),(25,17,3,3,1,3,'2024-12-12'),(26,18,20,3,2,3,'2024-12-16'),(27,18,2,3,2,3,'2024-12-16'),(28,19,4,1,3,3,'2024-12-26'),(29,19,2,1,3,3,'2024-12-26'),(30,20,1,2,2,3,'2024-12-26'),(31,21,20,3,1,3,'2024-12-18'),(32,22,20,3,2,3,'2024-12-23'),(33,22,21,3,6,3,'2024-12-23'),(34,23,20,3,2,3,'2024-12-27'),(35,24,2,3,2,3,'2024-12-27'),(36,25,20,3,2,3,'2024-12-27'),(37,26,20,3,2,3,'2024-12-30'),(38,27,1,2,2,3,'2024-12-30');
/*!40000 ALTER TABLE `tbpurchaserequestitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbqcinspectioncriteria`
--

DROP TABLE IF EXISTS `tbqcinspectioncriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbqcinspectioncriteria` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `position` bigint DEFAULT NULL,
  `inspected` varchar(64) DEFAULT NULL,
  `parameter` varchar(64) DEFAULT NULL,
  `standard` varchar(64) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT 'SQL',
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT 'SQL',
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbqcinspectioncriteria`
--

LOCK TABLES `tbqcinspectioncriteria` WRITE;
/*!40000 ALTER TABLE `tbqcinspectioncriteria` DISABLE KEYS */;
INSERT INTO `tbqcinspectioncriteria` VALUES (1,1,'Alat Transportasi','Keamanan Barang','Pintu Terkunci (terdapat gembok)','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(2,2,'Alat Transportasi','Kebersihan Transportasi','Bersih dan tidak berbau','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(3,3,'Alat Transportasi','Suhu Pendingin','Max. -12 C','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(4,4,'Alat Transportasi','Suhu Produk','Max. -12 C','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(5,5,'Alat Transportasi','Keberadaan Hama','Tidak ada hama','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(6,6,'Alat Transportasi','Kesesuaian dengan Product Spec','Sesuai product spec','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(7,1,'Produk / Barang','Nama Produk','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(8,2,'Produk / Barang','Nama Supplier','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(9,3,'Produk / Barang','Nama Produsen','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(10,4,'Produk / Barang','Negara Asal','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(11,5,'Produk / Barang','Jumlah Produk / Barang','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(12,6,'Produk / Barang','No. Surat Jalan','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(13,7,'Produk / Barang','No. Purchase Order','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(14,8,'Produk / Barang','No. Lot / Batch','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(15,9,'Produk / Barang','No. COA','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(16,10,'Produk / Barang','Kode Produksi','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(17,11,'Produk / Barang','Jumlah Kemasan','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(18,12,'Produk / Barang','S/L Date','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(19,13,'Produk / Barang','Production Date','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(20,14,'Produk / Barang','Expired Date','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(21,15,'Produk / Barang','Sertifikasi Halal','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(22,16,'Produk / Barang','Lembaga Sertifikasi','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(23,17,'Produk / Barang','Berlaku s/d','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(24,18,'Produk / Barang','No Sertifikat','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(25,1,'Pemeriksaan Produk (BB Daging)','Bentuk Produk - S1','Sesuai','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(26,2,'Pemeriksaan Produk (BB Daging)','Bentuk Produk - S2','Sesuai','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(27,3,'Pemeriksaan Produk (BB Daging)','Bentuk Produk - S3','Sesuai','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(28,4,'Pemeriksaan Produk (BB Daging)','Bentuk Produk - S4','Sesuai','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(29,5,'Pemeriksaan Produk (BB Daging)','Bentuk Produk - S5','Sesuai','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(30,6,'Pemeriksaan Produk (BB Daging)','Warna Produk - S1','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(31,7,'Pemeriksaan Produk (BB Daging)','Warna Produk - S2','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(32,8,'Pemeriksaan Produk (BB Daging)','Warna Produk - S3','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(33,9,'Pemeriksaan Produk (BB Daging)','Warna Produk - S4','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(34,10,'Pemeriksaan Produk (BB Daging)','Warna Produk - S5','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(35,11,'Pemeriksaan Produk (BB Daging)','Penampakan - S1','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(36,12,'Pemeriksaan Produk (BB Daging)','Penampakan - S2','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(37,13,'Pemeriksaan Produk (BB Daging)','Penampakan - S3','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(38,14,'Pemeriksaan Produk (BB Daging)','Penampakan - S4','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(39,15,'Pemeriksaan Produk (BB Daging)','Penampakan - S5','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(40,16,'Pemeriksaan Produk (BB Daging)','Aroma - S1','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(41,17,'Pemeriksaan Produk (BB Daging)','Aroma - S2','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(42,18,'Pemeriksaan Produk (BB Daging)','Aroma - S3','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(43,19,'Pemeriksaan Produk (BB Daging)','Aroma - S4','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(44,20,'Pemeriksaan Produk (BB Daging)','Aroma - S5','Normal','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(45,21,'Pemeriksaan Produk (BB Daging)','Temuan Benda Asing - S1','Tidak Ada','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(46,22,'Pemeriksaan Produk (BB Daging)','Temuan Benda Asing - S2','Tidak Ada','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(47,23,'Pemeriksaan Produk (BB Daging)','Temuan Benda Asing - S3','Tidak Ada','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(48,24,'Pemeriksaan Produk (BB Daging)','Temuan Benda Asing - S4','Tidak Ada','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(49,25,'Pemeriksaan Produk (BB Daging)','Temuan Benda Asing - S5','Tidak Ada','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(50,1,'Hasil','Hasil Pemeriksaan','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(51,2,'Hasil','Tanggal Pemeriksaan','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(52,3,'Hasil','Lokasi Pemeriksaan','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(53,1,'Inspector','Pemeriksa','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03'),(54,2,'Inspector','Supervisor Gudang','','SQL','2024-11-14 03:49:03','SQL','2024-11-14 03:49:03');
/*!40000 ALTER TABLE `tbqcinspectioncriteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbqualitycontrol`
--

DROP TABLE IF EXISTS `tbqualitycontrol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbqualitycontrol` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `purchase_order` bigint NOT NULL,
  `shfacilityid` bigint NOT NULL,
  `status` int DEFAULT NULL,
  `file` bigint DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-poForQc` (`purchase_order`),
  KEY `fk-shForQc` (`shfacilityid`),
  KEY `fk-fileForQc` (`file`),
  CONSTRAINT `fk-fileForQc` FOREIGN KEY (`file`) REFERENCES `tbfile` (`id`),
  CONSTRAINT `fk-poForQc` FOREIGN KEY (`purchase_order`) REFERENCES `tbpurchaseorder` (`id`),
  CONSTRAINT `fk-shForQc` FOREIGN KEY (`shfacilityid`) REFERENCES `tbstorehousefacility` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbqualitycontrol`
--

LOCK TABLES `tbqualitycontrol` WRITE;
/*!40000 ALTER TABLE `tbqualitycontrol` DISABLE KEYS */;
INSERT INTO `tbqualitycontrol` VALUES (1,1,2,1,NULL,NULL,NULL,'admin','2024-11-15 06:24:38'),(2,2,1,1,NULL,'admin','2024-11-15 07:12:22','admin','2024-11-15 07:14:05'),(3,3,1,1,NULL,'admin','2024-11-21 09:03:11','admin','2024-11-21 09:03:38'),(4,11,1,0,NULL,'admin','2024-12-03 07:25:25','admin','2024-12-18 08:13:55');
/*!40000 ALTER TABLE `tbqualitycontrol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbqualitycontrolitem`
--

DROP TABLE IF EXISTS `tbqualitycontrolitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbqualitycontrolitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `quality_control` bigint NOT NULL,
  `purchase_order_item` bigint NOT NULL,
  `first_qc_item_id_before_split` bigint DEFAULT NULL,
  `status` int DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-qcForQcItem` (`quality_control`),
  KEY `fk-poiForQcItem` (`purchase_order_item`),
  CONSTRAINT `fk-poiForQcItem` FOREIGN KEY (`purchase_order_item`) REFERENCES `tbpurchaseorderitem` (`id`),
  CONSTRAINT `fk-qcForQcItem` FOREIGN KEY (`quality_control`) REFERENCES `tbqualitycontrol` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbqualitycontrolitem`
--

LOCK TABLES `tbqualitycontrolitem` WRITE;
/*!40000 ALTER TABLE `tbqualitycontrolitem` DISABLE KEYS */;
INSERT INTO `tbqualitycontrolitem` VALUES (1,1,1,1,1,'admin','2024-11-15 06:22:34','admin','2024-11-15 06:23:41'),(2,1,2,2,1,'admin','2024-11-15 06:22:34','admin','2024-11-15 06:24:38'),(3,2,3,3,1,'admin','2024-11-15 07:12:22','admin','2024-11-15 07:14:05'),(4,3,4,4,1,'admin','2024-11-21 09:03:11','admin','2024-11-21 09:03:38'),(5,4,12,5,1,'admin','2024-12-03 07:25:25','admin','2024-12-18 08:13:55'),(6,4,13,6,1,'admin','2024-12-03 07:25:25','admin','2024-12-18 08:15:57'),(7,4,14,7,2,'admin','2024-12-03 07:25:25','admin','2024-12-03 07:25:25'),(8,4,15,8,2,'admin','2024-12-03 07:25:25','admin','2024-12-03 07:25:25');
/*!40000 ALTER TABLE `tbqualitycontrolitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbqualitycontroliteminspection`
--

DROP TABLE IF EXISTS `tbqualitycontroliteminspection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbqualitycontroliteminspection` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `quality_control_item` bigint NOT NULL,
  `quality_control_no` varchar(32) DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `quality_control_date` date DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `arrival_quantity` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-qciForQciInspection` (`quality_control_item`),
  CONSTRAINT `fk-qciForQciInspection` FOREIGN KEY (`quality_control_item`) REFERENCES `tbqualitycontrolitem` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbqualitycontroliteminspection`
--

LOCK TABLES `tbqualitycontroliteminspection` WRITE;
/*!40000 ALTER TABLE `tbqualitycontroliteminspection` DISABLE KEYS */;
INSERT INTO `tbqualitycontroliteminspection` VALUES (1,1,'2400001','2024-11-15','2024-11-15','2025-11-15',2,1,'','admin','2024-11-15 06:23:41',NULL,NULL),(2,2,'2400002','2024-11-15','2024-11-15','2025-11-15',2,0,'','admin','2024-11-15 06:24:11',NULL,NULL),(3,2,'2400003','2024-11-15','2024-11-15','2025-11-15',2,1,'','admin','2024-11-15 06:24:38',NULL,NULL),(4,3,'2400004','2024-11-15','2024-11-15','2025-11-15',2,1,'','admin','2024-11-15 07:14:05',NULL,NULL),(5,4,'2400005','2024-11-21','2024-11-21','2025-11-21',2,1,'','admin','2024-11-21 09:03:38',NULL,NULL),(6,5,'2400006','2024-12-18','2024-12-18','2025-12-18',2,0,'','admin','2024-12-18 08:13:31',NULL,NULL),(7,5,'2400007','2024-12-18','2024-12-18','2025-12-18',2,1,'','admin','2024-12-18 08:13:55',NULL,NULL),(8,6,'2400008','2024-12-18','2024-12-18','2025-12-18',2,1,'','admin','2024-12-18 08:15:02',NULL,NULL),(9,6,'2400009','2024-12-18','2024-12-18','2025-12-18',1,1,'','admin','2024-12-18 08:15:57',NULL,NULL);
/*!40000 ALTER TABLE `tbqualitycontroliteminspection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbqualitycontroliteminspectionresult`
--

DROP TABLE IF EXISTS `tbqualitycontroliteminspectionresult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbqualitycontroliteminspectionresult` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `quality_control_item_inspection` bigint NOT NULL,
  `inspection_criteria` bigint NOT NULL,
  `result` varchar(64) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-criteriaForResult` (`inspection_criteria`),
  KEY `fk-qciiForResult` (`quality_control_item_inspection`),
  CONSTRAINT `fk-criteriaForResult` FOREIGN KEY (`inspection_criteria`) REFERENCES `tbqcinspectioncriteria` (`id`),
  CONSTRAINT `fk-qciiForResult` FOREIGN KEY (`quality_control_item_inspection`) REFERENCES `tbqualitycontroliteminspection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=487 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbqualitycontroliteminspectionresult`
--

LOCK TABLES `tbqualitycontroliteminspectionresult` WRITE;
/*!40000 ALTER TABLE `tbqualitycontroliteminspectionresult` DISABLE KEYS */;
INSERT INTO `tbqualitycontroliteminspectionresult` VALUES (1,1,1,'','admin','2024-11-15 06:23:41',NULL,NULL),(2,1,2,'','admin','2024-11-15 06:23:41',NULL,NULL),(3,1,3,'','admin','2024-11-15 06:23:41',NULL,NULL),(4,1,4,'','admin','2024-11-15 06:23:41',NULL,NULL),(5,1,5,'','admin','2024-11-15 06:23:41',NULL,NULL),(6,1,6,'','admin','2024-11-15 06:23:41',NULL,NULL),(7,1,7,'002 - AYAM','admin','2024-11-15 06:23:41',NULL,NULL),(8,1,8,'1178 - SUPPLIER DAGING','admin','2024-11-15 06:23:41',NULL,NULL),(9,1,9,'','admin','2024-11-15 06:23:41',NULL,NULL),(10,1,10,'','admin','2024-11-15 06:23:41',NULL,NULL),(11,1,11,'','admin','2024-11-15 06:23:41',NULL,NULL),(12,1,12,'','admin','2024-11-15 06:23:41',NULL,NULL),(13,1,13,'S-0000001','admin','2024-11-15 06:23:41',NULL,NULL),(14,1,14,'','admin','2024-11-15 06:23:41',NULL,NULL),(15,1,15,'','admin','2024-11-15 06:23:41',NULL,NULL),(16,1,16,'','admin','2024-11-15 06:23:41',NULL,NULL),(17,1,17,'','admin','2024-11-15 06:23:41',NULL,NULL),(18,1,18,'2024-11-15','admin','2024-11-15 06:23:41',NULL,NULL),(19,1,19,'2024-11-15','admin','2024-11-15 06:23:41',NULL,NULL),(20,1,20,'2024-11-15','admin','2024-11-15 06:23:41',NULL,NULL),(21,1,21,'','admin','2024-11-15 06:23:41',NULL,NULL),(22,1,22,'','admin','2024-11-15 06:23:41',NULL,NULL),(23,1,23,'2024-11-15','admin','2024-11-15 06:23:41',NULL,NULL),(24,1,24,'','admin','2024-11-15 06:23:41',NULL,NULL),(25,1,25,'','admin','2024-11-15 06:23:41',NULL,NULL),(26,1,26,'','admin','2024-11-15 06:23:41',NULL,NULL),(27,1,27,'','admin','2024-11-15 06:23:41',NULL,NULL),(28,1,28,'','admin','2024-11-15 06:23:41',NULL,NULL),(29,1,29,'','admin','2024-11-15 06:23:41',NULL,NULL),(30,1,30,'','admin','2024-11-15 06:23:41',NULL,NULL),(31,1,31,'','admin','2024-11-15 06:23:41',NULL,NULL),(32,1,32,'','admin','2024-11-15 06:23:41',NULL,NULL),(33,1,33,'','admin','2024-11-15 06:23:41',NULL,NULL),(34,1,34,'','admin','2024-11-15 06:23:41',NULL,NULL),(35,1,35,'','admin','2024-11-15 06:23:41',NULL,NULL),(36,1,36,'','admin','2024-11-15 06:23:41',NULL,NULL),(37,1,37,'','admin','2024-11-15 06:23:41',NULL,NULL),(38,1,38,'','admin','2024-11-15 06:23:41',NULL,NULL),(39,1,39,'','admin','2024-11-15 06:23:41',NULL,NULL),(40,1,40,'','admin','2024-11-15 06:23:41',NULL,NULL),(41,1,41,'','admin','2024-11-15 06:23:41',NULL,NULL),(42,1,42,'','admin','2024-11-15 06:23:41',NULL,NULL),(43,1,43,'','admin','2024-11-15 06:23:41',NULL,NULL),(44,1,44,'','admin','2024-11-15 06:23:41',NULL,NULL),(45,1,45,'','admin','2024-11-15 06:23:41',NULL,NULL),(46,1,46,'','admin','2024-11-15 06:23:41',NULL,NULL),(47,1,47,'','admin','2024-11-15 06:23:41',NULL,NULL),(48,1,48,'','admin','2024-11-15 06:23:41',NULL,NULL),(49,1,49,'','admin','2024-11-15 06:23:41',NULL,NULL),(50,1,50,'Released','admin','2024-11-15 06:23:41',NULL,NULL),(51,1,51,'2024-11-15','admin','2024-11-15 06:23:41',NULL,NULL),(52,1,52,'GUNUNG PUTRI','admin','2024-11-15 06:23:41',NULL,NULL),(53,1,53,'ARIS RISMAWAN','admin','2024-11-15 06:23:41',NULL,NULL),(54,1,54,'ARIS RISMAWAN','admin','2024-11-15 06:23:41',NULL,NULL),(55,2,1,'','admin','2024-11-15 06:24:11',NULL,NULL),(56,2,2,'','admin','2024-11-15 06:24:11',NULL,NULL),(57,2,3,'','admin','2024-11-15 06:24:11',NULL,NULL),(58,2,4,'','admin','2024-11-15 06:24:11',NULL,NULL),(59,2,5,'','admin','2024-11-15 06:24:11',NULL,NULL),(60,2,6,'','admin','2024-11-15 06:24:11',NULL,NULL),(61,2,7,'004 - BABI','admin','2024-11-15 06:24:11',NULL,NULL),(62,2,8,'1178 - SUPPLIER DAGING','admin','2024-11-15 06:24:11',NULL,NULL),(63,2,9,'','admin','2024-11-15 06:24:11',NULL,NULL),(64,2,10,'','admin','2024-11-15 06:24:11',NULL,NULL),(65,2,11,'','admin','2024-11-15 06:24:11',NULL,NULL),(66,2,12,'','admin','2024-11-15 06:24:11',NULL,NULL),(67,2,13,'S-0000001','admin','2024-11-15 06:24:11',NULL,NULL),(68,2,14,'','admin','2024-11-15 06:24:11',NULL,NULL),(69,2,15,'','admin','2024-11-15 06:24:11',NULL,NULL),(70,2,16,'','admin','2024-11-15 06:24:11',NULL,NULL),(71,2,17,'','admin','2024-11-15 06:24:11',NULL,NULL),(72,2,18,'2024-11-15','admin','2024-11-15 06:24:11',NULL,NULL),(73,2,19,'2024-11-15','admin','2024-11-15 06:24:11',NULL,NULL),(74,2,20,'2024-11-15','admin','2024-11-15 06:24:11',NULL,NULL),(75,2,21,'','admin','2024-11-15 06:24:11',NULL,NULL),(76,2,22,'','admin','2024-11-15 06:24:11',NULL,NULL),(77,2,23,'2024-11-15','admin','2024-11-15 06:24:11',NULL,NULL),(78,2,24,'','admin','2024-11-15 06:24:11',NULL,NULL),(79,2,25,'','admin','2024-11-15 06:24:11',NULL,NULL),(80,2,26,'','admin','2024-11-15 06:24:11',NULL,NULL),(81,2,27,'','admin','2024-11-15 06:24:11',NULL,NULL),(82,2,28,'','admin','2024-11-15 06:24:11',NULL,NULL),(83,2,29,'','admin','2024-11-15 06:24:11',NULL,NULL),(84,2,30,'','admin','2024-11-15 06:24:11',NULL,NULL),(85,2,31,'','admin','2024-11-15 06:24:11',NULL,NULL),(86,2,32,'','admin','2024-11-15 06:24:11',NULL,NULL),(87,2,33,'','admin','2024-11-15 06:24:11',NULL,NULL),(88,2,34,'','admin','2024-11-15 06:24:11',NULL,NULL),(89,2,35,'','admin','2024-11-15 06:24:11',NULL,NULL),(90,2,36,'','admin','2024-11-15 06:24:11',NULL,NULL),(91,2,37,'','admin','2024-11-15 06:24:11',NULL,NULL),(92,2,38,'','admin','2024-11-15 06:24:11',NULL,NULL),(93,2,39,'','admin','2024-11-15 06:24:11',NULL,NULL),(94,2,40,'','admin','2024-11-15 06:24:11',NULL,NULL),(95,2,41,'','admin','2024-11-15 06:24:11',NULL,NULL),(96,2,42,'','admin','2024-11-15 06:24:11',NULL,NULL),(97,2,43,'','admin','2024-11-15 06:24:11',NULL,NULL),(98,2,44,'','admin','2024-11-15 06:24:11',NULL,NULL),(99,2,45,'','admin','2024-11-15 06:24:11',NULL,NULL),(100,2,46,'','admin','2024-11-15 06:24:11',NULL,NULL),(101,2,47,'','admin','2024-11-15 06:24:11',NULL,NULL),(102,2,48,'','admin','2024-11-15 06:24:11',NULL,NULL),(103,2,49,'','admin','2024-11-15 06:24:11',NULL,NULL),(104,2,50,'Rejected','admin','2024-11-15 06:24:11',NULL,NULL),(105,2,51,'2024-11-15','admin','2024-11-15 06:24:11',NULL,NULL),(106,2,52,'GUNUNG PUTRI','admin','2024-11-15 06:24:11',NULL,NULL),(107,2,53,'ARIS RISMAWAN','admin','2024-11-15 06:24:11',NULL,NULL),(108,2,54,'ARIS RISMAWAN','admin','2024-11-15 06:24:11',NULL,NULL),(109,3,1,'','admin','2024-11-15 06:24:38',NULL,NULL),(110,3,2,'','admin','2024-11-15 06:24:38',NULL,NULL),(111,3,3,'','admin','2024-11-15 06:24:38',NULL,NULL),(112,3,4,'','admin','2024-11-15 06:24:38',NULL,NULL),(113,3,5,'','admin','2024-11-15 06:24:38',NULL,NULL),(114,3,6,'','admin','2024-11-15 06:24:38',NULL,NULL),(115,3,7,'004 - BABI','admin','2024-11-15 06:24:38',NULL,NULL),(116,3,8,'1178 - SUPPLIER DAGING','admin','2024-11-15 06:24:38',NULL,NULL),(117,3,9,'','admin','2024-11-15 06:24:38',NULL,NULL),(118,3,10,'','admin','2024-11-15 06:24:38',NULL,NULL),(119,3,11,'','admin','2024-11-15 06:24:38',NULL,NULL),(120,3,12,'','admin','2024-11-15 06:24:38',NULL,NULL),(121,3,13,'S-0000001','admin','2024-11-15 06:24:38',NULL,NULL),(122,3,14,'','admin','2024-11-15 06:24:38',NULL,NULL),(123,3,15,'','admin','2024-11-15 06:24:38',NULL,NULL),(124,3,16,'','admin','2024-11-15 06:24:38',NULL,NULL),(125,3,17,'','admin','2024-11-15 06:24:38',NULL,NULL),(126,3,18,'2024-11-15','admin','2024-11-15 06:24:38',NULL,NULL),(127,3,19,'2024-11-15','admin','2024-11-15 06:24:38',NULL,NULL),(128,3,20,'2024-11-15','admin','2024-11-15 06:24:38',NULL,NULL),(129,3,21,'','admin','2024-11-15 06:24:38',NULL,NULL),(130,3,22,'','admin','2024-11-15 06:24:38',NULL,NULL),(131,3,23,'2024-11-15','admin','2024-11-15 06:24:38',NULL,NULL),(132,3,24,'','admin','2024-11-15 06:24:38',NULL,NULL),(133,3,25,'','admin','2024-11-15 06:24:38',NULL,NULL),(134,3,26,'','admin','2024-11-15 06:24:38',NULL,NULL),(135,3,27,'','admin','2024-11-15 06:24:38',NULL,NULL),(136,3,28,'','admin','2024-11-15 06:24:38',NULL,NULL),(137,3,29,'','admin','2024-11-15 06:24:38',NULL,NULL),(138,3,30,'','admin','2024-11-15 06:24:38',NULL,NULL),(139,3,31,'','admin','2024-11-15 06:24:38',NULL,NULL),(140,3,32,'','admin','2024-11-15 06:24:38',NULL,NULL),(141,3,33,'','admin','2024-11-15 06:24:38',NULL,NULL),(142,3,34,'','admin','2024-11-15 06:24:38',NULL,NULL),(143,3,35,'','admin','2024-11-15 06:24:38',NULL,NULL),(144,3,36,'','admin','2024-11-15 06:24:38',NULL,NULL),(145,3,37,'','admin','2024-11-15 06:24:38',NULL,NULL),(146,3,38,'','admin','2024-11-15 06:24:38',NULL,NULL),(147,3,39,'','admin','2024-11-15 06:24:38',NULL,NULL),(148,3,40,'','admin','2024-11-15 06:24:38',NULL,NULL),(149,3,41,'','admin','2024-11-15 06:24:38',NULL,NULL),(150,3,42,'','admin','2024-11-15 06:24:38',NULL,NULL),(151,3,43,'','admin','2024-11-15 06:24:38',NULL,NULL),(152,3,44,'','admin','2024-11-15 06:24:38',NULL,NULL),(153,3,45,'','admin','2024-11-15 06:24:38',NULL,NULL),(154,3,46,'','admin','2024-11-15 06:24:38',NULL,NULL),(155,3,47,'','admin','2024-11-15 06:24:38',NULL,NULL),(156,3,48,'','admin','2024-11-15 06:24:38',NULL,NULL),(157,3,49,'','admin','2024-11-15 06:24:38',NULL,NULL),(158,3,50,'Released','admin','2024-11-15 06:24:38',NULL,NULL),(159,3,51,'2024-11-15','admin','2024-11-15 06:24:38',NULL,NULL),(160,3,52,'GUNUNG PUTRI','admin','2024-11-15 06:24:38',NULL,NULL),(161,3,53,'ARIS','admin','2024-11-15 06:24:38',NULL,NULL),(162,3,54,'ARIS RISMAWAN','admin','2024-11-15 06:24:38',NULL,NULL),(163,4,1,'','admin','2024-11-15 07:14:05',NULL,NULL),(164,4,2,'','admin','2024-11-15 07:14:05',NULL,NULL),(165,4,3,'','admin','2024-11-15 07:14:05',NULL,NULL),(166,4,4,'','admin','2024-11-15 07:14:05',NULL,NULL),(167,4,5,'','admin','2024-11-15 07:14:05',NULL,NULL),(168,4,6,'','admin','2024-11-15 07:14:05',NULL,NULL),(169,4,7,'001 - GARAM','admin','2024-11-15 07:14:05',NULL,NULL),(170,4,8,'1179 - SUPPLIER BUMBU','admin','2024-11-15 07:14:05',NULL,NULL),(171,4,9,'','admin','2024-11-15 07:14:05',NULL,NULL),(172,4,10,'','admin','2024-11-15 07:14:05',NULL,NULL),(173,4,11,'','admin','2024-11-15 07:14:05',NULL,NULL),(174,4,12,'','admin','2024-11-15 07:14:05',NULL,NULL),(175,4,13,'S-0000002','admin','2024-11-15 07:14:05',NULL,NULL),(176,4,14,'','admin','2024-11-15 07:14:05',NULL,NULL),(177,4,15,'','admin','2024-11-15 07:14:05',NULL,NULL),(178,4,16,'','admin','2024-11-15 07:14:05',NULL,NULL),(179,4,17,'','admin','2024-11-15 07:14:05',NULL,NULL),(180,4,18,'2024-11-15','admin','2024-11-15 07:14:05',NULL,NULL),(181,4,19,'2024-11-15','admin','2024-11-15 07:14:05',NULL,NULL),(182,4,20,'2024-11-15','admin','2024-11-15 07:14:05',NULL,NULL),(183,4,21,'','admin','2024-11-15 07:14:05',NULL,NULL),(184,4,22,'','admin','2024-11-15 07:14:05',NULL,NULL),(185,4,23,'2024-11-15','admin','2024-11-15 07:14:05',NULL,NULL),(186,4,24,'','admin','2024-11-15 07:14:05',NULL,NULL),(187,4,25,'','admin','2024-11-15 07:14:05',NULL,NULL),(188,4,26,'','admin','2024-11-15 07:14:05',NULL,NULL),(189,4,27,'','admin','2024-11-15 07:14:05',NULL,NULL),(190,4,28,'','admin','2024-11-15 07:14:05',NULL,NULL),(191,4,29,'','admin','2024-11-15 07:14:05',NULL,NULL),(192,4,30,'','admin','2024-11-15 07:14:05',NULL,NULL),(193,4,31,'','admin','2024-11-15 07:14:05',NULL,NULL),(194,4,32,'','admin','2024-11-15 07:14:05',NULL,NULL),(195,4,33,'','admin','2024-11-15 07:14:05',NULL,NULL),(196,4,34,'','admin','2024-11-15 07:14:05',NULL,NULL),(197,4,35,'','admin','2024-11-15 07:14:05',NULL,NULL),(198,4,36,'','admin','2024-11-15 07:14:05',NULL,NULL),(199,4,37,'','admin','2024-11-15 07:14:05',NULL,NULL),(200,4,38,'','admin','2024-11-15 07:14:05',NULL,NULL),(201,4,39,'','admin','2024-11-15 07:14:05',NULL,NULL),(202,4,40,'','admin','2024-11-15 07:14:05',NULL,NULL),(203,4,41,'','admin','2024-11-15 07:14:05',NULL,NULL),(204,4,42,'','admin','2024-11-15 07:14:05',NULL,NULL),(205,4,43,'','admin','2024-11-15 07:14:05',NULL,NULL),(206,4,44,'','admin','2024-11-15 07:14:05',NULL,NULL),(207,4,45,'','admin','2024-11-15 07:14:05',NULL,NULL),(208,4,46,'','admin','2024-11-15 07:14:05',NULL,NULL),(209,4,47,'','admin','2024-11-15 07:14:05',NULL,NULL),(210,4,48,'','admin','2024-11-15 07:14:05',NULL,NULL),(211,4,49,'','admin','2024-11-15 07:14:05',NULL,NULL),(212,4,50,'Released','admin','2024-11-15 07:14:05',NULL,NULL),(213,4,51,'2024-11-15','admin','2024-11-15 07:14:05',NULL,NULL),(214,4,52,'GUNUNG PUTRI','admin','2024-11-15 07:14:05',NULL,NULL),(215,4,53,'ARIS RISMAWAN','admin','2024-11-15 07:14:05',NULL,NULL),(216,4,54,'ARIS RISMAWAN','admin','2024-11-15 07:14:05',NULL,NULL),(217,5,1,'','admin','2024-11-21 09:03:38',NULL,NULL),(218,5,2,'','admin','2024-11-21 09:03:38',NULL,NULL),(219,5,3,'','admin','2024-11-21 09:03:38',NULL,NULL),(220,5,4,'','admin','2024-11-21 09:03:38',NULL,NULL),(221,5,5,'','admin','2024-11-21 09:03:38',NULL,NULL),(222,5,6,'','admin','2024-11-21 09:03:38',NULL,NULL),(223,5,7,'001 - GARAM','admin','2024-11-21 09:03:38',NULL,NULL),(224,5,8,'1179 - SUPPLIER BUMBU','admin','2024-11-21 09:03:38',NULL,NULL),(225,5,9,'','admin','2024-11-21 09:03:38',NULL,NULL),(226,5,10,'','admin','2024-11-21 09:03:38',NULL,NULL),(227,5,11,'','admin','2024-11-21 09:03:38',NULL,NULL),(228,5,12,'','admin','2024-11-21 09:03:38',NULL,NULL),(229,5,13,'S-0000003','admin','2024-11-21 09:03:38',NULL,NULL),(230,5,14,'','admin','2024-11-21 09:03:38',NULL,NULL),(231,5,15,'','admin','2024-11-21 09:03:38',NULL,NULL),(232,5,16,'','admin','2024-11-21 09:03:38',NULL,NULL),(233,5,17,'','admin','2024-11-21 09:03:38',NULL,NULL),(234,5,18,'2024-11-21','admin','2024-11-21 09:03:38',NULL,NULL),(235,5,19,'2024-11-21','admin','2024-11-21 09:03:38',NULL,NULL),(236,5,20,'2024-11-21','admin','2024-11-21 09:03:38',NULL,NULL),(237,5,21,'','admin','2024-11-21 09:03:38',NULL,NULL),(238,5,22,'','admin','2024-11-21 09:03:38',NULL,NULL),(239,5,23,'2024-11-21','admin','2024-11-21 09:03:38',NULL,NULL),(240,5,24,'','admin','2024-11-21 09:03:38',NULL,NULL),(241,5,25,'','admin','2024-11-21 09:03:38',NULL,NULL),(242,5,26,'','admin','2024-11-21 09:03:38',NULL,NULL),(243,5,27,'','admin','2024-11-21 09:03:38',NULL,NULL),(244,5,28,'','admin','2024-11-21 09:03:38',NULL,NULL),(245,5,29,'','admin','2024-11-21 09:03:38',NULL,NULL),(246,5,30,'','admin','2024-11-21 09:03:38',NULL,NULL),(247,5,31,'','admin','2024-11-21 09:03:38',NULL,NULL),(248,5,32,'','admin','2024-11-21 09:03:38',NULL,NULL),(249,5,33,'','admin','2024-11-21 09:03:38',NULL,NULL),(250,5,34,'','admin','2024-11-21 09:03:38',NULL,NULL),(251,5,35,'','admin','2024-11-21 09:03:38',NULL,NULL),(252,5,36,'','admin','2024-11-21 09:03:38',NULL,NULL),(253,5,37,'','admin','2024-11-21 09:03:38',NULL,NULL),(254,5,38,'','admin','2024-11-21 09:03:38',NULL,NULL),(255,5,39,'','admin','2024-11-21 09:03:38',NULL,NULL),(256,5,40,'','admin','2024-11-21 09:03:38',NULL,NULL),(257,5,41,'','admin','2024-11-21 09:03:38',NULL,NULL),(258,5,42,'','admin','2024-11-21 09:03:38',NULL,NULL),(259,5,43,'','admin','2024-11-21 09:03:38',NULL,NULL),(260,5,44,'','admin','2024-11-21 09:03:38',NULL,NULL),(261,5,45,'','admin','2024-11-21 09:03:38',NULL,NULL),(262,5,46,'','admin','2024-11-21 09:03:38',NULL,NULL),(263,5,47,'','admin','2024-11-21 09:03:38',NULL,NULL),(264,5,48,'','admin','2024-11-21 09:03:38',NULL,NULL),(265,5,49,'','admin','2024-11-21 09:03:38',NULL,NULL),(266,5,50,'Released','admin','2024-11-21 09:03:38',NULL,NULL),(267,5,51,'2024-11-21','admin','2024-11-21 09:03:38',NULL,NULL),(268,5,52,'GUNUNG PUTRI','admin','2024-11-21 09:03:38',NULL,NULL),(269,5,53,'ARIS RISMAWAN','admin','2024-11-21 09:03:38',NULL,NULL),(270,5,54,'ARIS RISMAWAN','admin','2024-11-21 09:03:38',NULL,NULL),(271,6,1,'','admin','2024-12-18 08:13:31',NULL,NULL),(272,6,2,'','admin','2024-12-18 08:13:31',NULL,NULL),(273,6,3,'','admin','2024-12-18 08:13:31',NULL,NULL),(274,6,4,'','admin','2024-12-18 08:13:31',NULL,NULL),(275,6,5,'','admin','2024-12-18 08:13:31',NULL,NULL),(276,6,6,'','admin','2024-12-18 08:13:31',NULL,NULL),(277,6,7,'01366 - BIJI WIJEN','admin','2024-12-18 08:13:31',NULL,NULL),(278,6,8,'511192 - PASAR CIPINANG','admin','2024-12-18 08:13:31',NULL,NULL),(279,6,9,'','admin','2024-12-18 08:13:31',NULL,NULL),(280,6,10,'','admin','2024-12-18 08:13:31',NULL,NULL),(281,6,11,'','admin','2024-12-18 08:13:31',NULL,NULL),(282,6,12,'','admin','2024-12-18 08:13:31',NULL,NULL),(283,6,13,'S-0000011','admin','2024-12-18 08:13:31',NULL,NULL),(284,6,14,'','admin','2024-12-18 08:13:31',NULL,NULL),(285,6,15,'','admin','2024-12-18 08:13:31',NULL,NULL),(286,6,16,'','admin','2024-12-18 08:13:31',NULL,NULL),(287,6,17,'','admin','2024-12-18 08:13:31',NULL,NULL),(288,6,18,'2024-12-18','admin','2024-12-18 08:13:31',NULL,NULL),(289,6,19,'2024-12-18','admin','2024-12-18 08:13:31',NULL,NULL),(290,6,20,'2024-12-18','admin','2024-12-18 08:13:31',NULL,NULL),(291,6,21,'','admin','2024-12-18 08:13:31',NULL,NULL),(292,6,22,'','admin','2024-12-18 08:13:31',NULL,NULL),(293,6,23,'2024-12-28','admin','2024-12-18 08:13:31',NULL,NULL),(294,6,24,'','admin','2024-12-18 08:13:31',NULL,NULL),(295,6,25,'on','admin','2024-12-18 08:13:31',NULL,NULL),(296,6,26,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(297,6,27,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(298,6,28,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(299,6,29,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(300,6,30,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(301,6,31,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(302,6,32,'on','admin','2024-12-18 08:13:31',NULL,NULL),(303,6,33,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(304,6,34,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(305,6,35,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(306,6,36,'on','admin','2024-12-18 08:13:31',NULL,NULL),(307,6,37,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(308,6,38,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(309,6,39,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(310,6,40,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(311,6,41,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(312,6,42,'on','admin','2024-12-18 08:13:31',NULL,NULL),(313,6,43,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(314,6,44,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(315,6,45,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(316,6,46,'on','admin','2024-12-18 08:13:31',NULL,NULL),(317,6,47,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(318,6,48,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(319,6,49,NULL,'admin','2024-12-18 08:13:31',NULL,NULL),(320,6,50,'Rejected','admin','2024-12-18 08:13:31',NULL,NULL),(321,6,51,'2024-12-18','admin','2024-12-18 08:13:31',NULL,NULL),(322,6,52,'GUNUNG PUTRI','admin','2024-12-18 08:13:31',NULL,NULL),(323,6,53,'TIDAK','admin','2024-12-18 08:13:31',NULL,NULL),(324,6,54,'ARIS RISMAWAN','admin','2024-12-18 08:13:31',NULL,NULL),(325,7,1,'','admin','2024-12-18 08:13:55',NULL,NULL),(326,7,2,'','admin','2024-12-18 08:13:55',NULL,NULL),(327,7,3,'','admin','2024-12-18 08:13:55',NULL,NULL),(328,7,4,'','admin','2024-12-18 08:13:55',NULL,NULL),(329,7,5,'','admin','2024-12-18 08:13:55',NULL,NULL),(330,7,6,'','admin','2024-12-18 08:13:55',NULL,NULL),(331,7,7,'01366 - BIJI WIJEN','admin','2024-12-18 08:13:55',NULL,NULL),(332,7,8,'511192 - PASAR CIPINANG','admin','2024-12-18 08:13:55',NULL,NULL),(333,7,9,'','admin','2024-12-18 08:13:55',NULL,NULL),(334,7,10,'','admin','2024-12-18 08:13:55',NULL,NULL),(335,7,11,'','admin','2024-12-18 08:13:55',NULL,NULL),(336,7,12,'','admin','2024-12-18 08:13:55',NULL,NULL),(337,7,13,'S-0000011','admin','2024-12-18 08:13:55',NULL,NULL),(338,7,14,'','admin','2024-12-18 08:13:55',NULL,NULL),(339,7,15,'','admin','2024-12-18 08:13:55',NULL,NULL),(340,7,16,'','admin','2024-12-18 08:13:55',NULL,NULL),(341,7,17,'','admin','2024-12-18 08:13:55',NULL,NULL),(342,7,18,'2024-12-18','admin','2024-12-18 08:13:55',NULL,NULL),(343,7,19,'2024-12-18','admin','2024-12-18 08:13:55',NULL,NULL),(344,7,20,'2024-12-18','admin','2024-12-18 08:13:55',NULL,NULL),(345,7,21,'','admin','2024-12-18 08:13:55',NULL,NULL),(346,7,22,'','admin','2024-12-18 08:13:55',NULL,NULL),(347,7,23,'2024-12-28','admin','2024-12-18 08:13:55',NULL,NULL),(348,7,24,'','admin','2024-12-18 08:13:55',NULL,NULL),(349,7,25,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(350,7,26,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(351,7,27,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(352,7,28,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(353,7,29,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(354,7,30,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(355,7,31,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(356,7,32,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(357,7,33,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(358,7,34,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(359,7,35,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(360,7,36,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(361,7,37,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(362,7,38,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(363,7,39,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(364,7,40,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(365,7,41,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(366,7,42,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(367,7,43,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(368,7,44,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(369,7,45,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(370,7,46,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(371,7,47,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(372,7,48,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(373,7,49,NULL,'admin','2024-12-18 08:13:55',NULL,NULL),(374,7,50,'Released','admin','2024-12-18 08:13:55',NULL,NULL),(375,7,51,'2024-12-18','admin','2024-12-18 08:13:55',NULL,NULL),(376,7,52,'GUNUNG PUTRI','admin','2024-12-18 08:13:55',NULL,NULL),(377,7,53,'ARIS RISMAWAN','admin','2024-12-18 08:13:55',NULL,NULL),(378,7,54,'ARIS RISMAWAN','admin','2024-12-18 08:13:55',NULL,NULL),(379,8,1,'','admin','2024-12-18 08:15:02',NULL,NULL),(380,8,2,'','admin','2024-12-18 08:15:02',NULL,NULL),(381,8,3,'','admin','2024-12-18 08:15:02',NULL,NULL),(382,8,4,'','admin','2024-12-18 08:15:02',NULL,NULL),(383,8,5,'','admin','2024-12-18 08:15:02',NULL,NULL),(384,8,6,'','admin','2024-12-18 08:15:02',NULL,NULL),(385,8,7,'0044 - MECIN','admin','2024-12-18 08:15:02',NULL,NULL),(386,8,8,'511192 - PASAR CIPINANG','admin','2024-12-18 08:15:02',NULL,NULL),(387,8,9,'','admin','2024-12-18 08:15:02',NULL,NULL),(388,8,10,'','admin','2024-12-18 08:15:02',NULL,NULL),(389,8,11,'','admin','2024-12-18 08:15:02',NULL,NULL),(390,8,12,'','admin','2024-12-18 08:15:02',NULL,NULL),(391,8,13,'S-0000011','admin','2024-12-18 08:15:02',NULL,NULL),(392,8,14,'','admin','2024-12-18 08:15:02',NULL,NULL),(393,8,15,'','admin','2024-12-18 08:15:02',NULL,NULL),(394,8,16,'','admin','2024-12-18 08:15:02',NULL,NULL),(395,8,17,'','admin','2024-12-18 08:15:02',NULL,NULL),(396,8,18,'2024-12-18','admin','2024-12-18 08:15:02',NULL,NULL),(397,8,19,'2024-12-18','admin','2024-12-18 08:15:02',NULL,NULL),(398,8,20,'2024-12-18','admin','2024-12-18 08:15:02',NULL,NULL),(399,8,21,'','admin','2024-12-18 08:15:02',NULL,NULL),(400,8,22,'','admin','2024-12-18 08:15:02',NULL,NULL),(401,8,23,'2024-12-27','admin','2024-12-18 08:15:02',NULL,NULL),(402,8,24,'','admin','2024-12-18 08:15:02',NULL,NULL),(403,8,25,'on','admin','2024-12-18 08:15:02',NULL,NULL),(404,8,26,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(405,8,27,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(406,8,28,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(407,8,29,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(408,8,30,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(409,8,31,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(410,8,32,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(411,8,33,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(412,8,34,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(413,8,35,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(414,8,36,'on','admin','2024-12-18 08:15:02',NULL,NULL),(415,8,37,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(416,8,38,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(417,8,39,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(418,8,40,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(419,8,41,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(420,8,42,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(421,8,43,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(422,8,44,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(423,8,45,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(424,8,46,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(425,8,47,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(426,8,48,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(427,8,49,NULL,'admin','2024-12-18 08:15:02',NULL,NULL),(428,8,50,'Released','admin','2024-12-18 08:15:02',NULL,NULL),(429,8,51,'2024-12-18','admin','2024-12-18 08:15:02',NULL,NULL),(430,8,52,'GUNUNG PUTRI','admin','2024-12-18 08:15:02',NULL,NULL),(431,8,53,'ARIS RISMAWAN','admin','2024-12-18 08:15:03',NULL,NULL),(432,8,54,'ARIS RISMAWAN','admin','2024-12-18 08:15:03',NULL,NULL),(433,9,1,'','admin','2024-12-18 08:15:57',NULL,NULL),(434,9,2,'','admin','2024-12-18 08:15:57',NULL,NULL),(435,9,3,'','admin','2024-12-18 08:15:57',NULL,NULL),(436,9,4,'','admin','2024-12-18 08:15:57',NULL,NULL),(437,9,5,'','admin','2024-12-18 08:15:57',NULL,NULL),(438,9,6,'','admin','2024-12-18 08:15:57',NULL,NULL),(439,9,7,'0044 - MECIN','admin','2024-12-18 08:15:57',NULL,NULL),(440,9,8,'511192 - PASAR CIPINANG','admin','2024-12-18 08:15:57',NULL,NULL),(441,9,9,'','admin','2024-12-18 08:15:57',NULL,NULL),(442,9,10,'','admin','2024-12-18 08:15:57',NULL,NULL),(443,9,11,'','admin','2024-12-18 08:15:57',NULL,NULL),(444,9,12,'','admin','2024-12-18 08:15:57',NULL,NULL),(445,9,13,'S-0000011','admin','2024-12-18 08:15:57',NULL,NULL),(446,9,14,'','admin','2024-12-18 08:15:57',NULL,NULL),(447,9,15,'','admin','2024-12-18 08:15:57',NULL,NULL),(448,9,16,'','admin','2024-12-18 08:15:57',NULL,NULL),(449,9,17,'','admin','2024-12-18 08:15:57',NULL,NULL),(450,9,18,'2024-12-18','admin','2024-12-18 08:15:57',NULL,NULL),(451,9,19,'2024-12-18','admin','2024-12-18 08:15:57',NULL,NULL),(452,9,20,'2024-12-18','admin','2024-12-18 08:15:57',NULL,NULL),(453,9,21,'','admin','2024-12-18 08:15:57',NULL,NULL),(454,9,22,'','admin','2024-12-18 08:15:57',NULL,NULL),(455,9,23,'2024-12-26','admin','2024-12-18 08:15:57',NULL,NULL),(456,9,24,'','admin','2024-12-18 08:15:57',NULL,NULL),(457,9,25,'on','admin','2024-12-18 08:15:57',NULL,NULL),(458,9,26,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(459,9,27,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(460,9,28,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(461,9,29,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(462,9,30,'on','admin','2024-12-18 08:15:57',NULL,NULL),(463,9,31,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(464,9,32,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(465,9,33,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(466,9,34,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(467,9,35,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(468,9,36,'on','admin','2024-12-18 08:15:57',NULL,NULL),(469,9,37,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(470,9,38,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(471,9,39,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(472,9,40,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(473,9,41,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(474,9,42,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(475,9,43,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(476,9,44,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(477,9,45,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(478,9,46,'on','admin','2024-12-18 08:15:57',NULL,NULL),(479,9,47,'on','admin','2024-12-18 08:15:57',NULL,NULL),(480,9,48,'on','admin','2024-12-18 08:15:57',NULL,NULL),(481,9,49,NULL,'admin','2024-12-18 08:15:57',NULL,NULL),(482,9,50,'Released','admin','2024-12-18 08:15:57',NULL,NULL),(483,9,51,'2024-12-18','admin','2024-12-18 08:15:57',NULL,NULL),(484,9,52,'GUNUNG PUTRI','admin','2024-12-18 08:15:57',NULL,NULL),(485,9,53,'ARIS RISMAWAN','admin','2024-12-18 08:15:57',NULL,NULL),(486,9,54,'ARIS RISMAWAN','admin','2024-12-18 08:15:57',NULL,NULL);
/*!40000 ALTER TABLE `tbqualitycontroliteminspectionresult` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbrawmaterials`
--

DROP TABLE IF EXISTS `tbrawmaterials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbrawmaterials` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(16) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `brand` varchar(300) DEFAULT NULL,
  `price` decimal(19,4) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `is_packaged` bit(1) DEFAULT NULL,
  `category` bigint DEFAULT NULL,
  `procurement_unit` bigint DEFAULT NULL,
  `storing_unit` bigint DEFAULT NULL,
  `conversion` decimal(19,4) DEFAULT NULL,
  `weight` decimal(19,4) DEFAULT NULL,
  `buffer_stock` decimal(19,2) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `remark` varchar(300) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-rmpunit` (`procurement_unit`),
  KEY `fk-rmskunit` (`storing_unit`),
  KEY `fk-rmcategory` (`category`),
  CONSTRAINT `fk-rmcategory` FOREIGN KEY (`category`) REFERENCES `tbrawmaterialscategory` (`id`),
  CONSTRAINT `fk-rmpunit` FOREIGN KEY (`procurement_unit`) REFERENCES `tbmasterunit` (`id`),
  CONSTRAINT `fk-rmskunit` FOREIGN KEY (`storing_unit`) REFERENCES `tbmasterunit` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbrawmaterials`
--

LOCK TABLES `tbrawmaterials` WRITE;
/*!40000 ALTER TABLE `tbrawmaterials` DISABLE KEYS */;
INSERT INTO `tbrawmaterials` VALUES (1,'001','GARAM','GARAM',NULL,50000.0000,_binary '',_binary '',2,3,3,5.0000,10.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(2,'002','AYAM','AYAM',NULL,50000.0000,_binary '',_binary '',1,2,4,5.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(3,'003','SAPI','SAPI',NULL,50000.0000,_binary '',_binary '',1,2,4,5.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(4,'004','BABI','BABI',NULL,50000.0000,_binary '',_binary '',1,2,4,5.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(5,'005','KOLAGEN','KOLAGEN',NULL,50000.0000,_binary '',_binary '',3,5,5,1.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(6,'006','KOLAGEN 2','KOLAGEN 2',NULL,50000.0000,_binary '',_binary '',3,5,5,1.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(7,'007','PLASTIK','PLASTIK',NULL,50000.0000,_binary '',_binary '',4,6,6,1.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(8,'008','PLASTIK 2','PLASTIK 2',NULL,50000.0000,_binary '',_binary '',4,6,6,1.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(9,'009','BAJU PELINDUNG','BAJU PELINDUNG',NULL,50000.0000,_binary '',_binary '',5,7,7,2.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(10,'010','MASKER PELINDUNG','MASKER PELINDUNG',NULL,50000.0000,_binary '',_binary '',5,7,7,2.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(11,'011','MESIN POTONG','MESIN POTONG',NULL,50000.0000,_binary '',_binary '',6,6,6,1.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(12,'012','MESIN PACKING','MESIN PACKING',NULL,50000.0000,_binary '',_binary '',6,6,6,1.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(13,'013','SERIVECE','SERIVECE',NULL,50000.0000,_binary '',_binary '',7,3,3,1.0000,1.0000,12.00,'',NULL,'2024-11-14 04:43:07',NULL),(14,'0012','BOMBAY','BOMBAY',NULL,55000.0000,_binary '',_binary '',2,3,3,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(15,'0022','SAGU GUNUNG','SAGU GUNUNG',NULL,45000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(16,'0033','BAWANG MERAH','BAWANG MERAH',NULL,32000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(17,'0044','MECIN','MECIN',NULL,25000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(18,'2345','MINYAK KITA','MINYAK KITA',NULL,65000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(19,'00653','MINYAK RIZKI','MINYAK RIZKI',NULL,22000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(20,'00722','KULIT AYAM','KULIT AYAM',NULL,20200.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(21,'00834','KULIT MANUSIA','KULIT MANUSIA',NULL,65000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(22,'00934','KULIT SINGA','KULIT SINGA',NULL,20200.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(23,'01011','KULIT BADAK','KULIT BADAK',NULL,25000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(24,'01144','CABAI','CABAI',NULL,45000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(25,'01222','BLACK PAPAER','BLACK PAPAER',NULL,55000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(26,'01366','BIJI WIJEN','BIJI WIJEN',NULL,50000.0000,_binary '',_binary '',2,3,4,1.0000,1.0000,NULL,'',NULL,'2024-11-26 04:17:26',NULL),(27,'88100001','MEAT BEEF FQ 95 CL_M.C HERD AUST [KG]','MEAT BEEF FQ 95 CL_M.C HERD AUST [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(28,'88100201','MEAT BEEF FQ 90 CL_FRIBOI [KG]','MEAT BEEF FQ 90 CL_FRIBOI [KG]',NULL,85000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(29,'88100401','MEAT BEEF FQ 85 CL_SPANYOL [KG]','MEAT BEEF FQ 85 CL_SPANYOL [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(30,'88100601','MEAT BEEF FQ 65 CL_HARVEY [KG]','MEAT BEEF FQ 65 CL_HARVEY [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(31,'88100801','MEAT BUFFALO [KG]','MEAT BUFFALO [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(32,'88101001','MEAT BEEF BRISKET BRAZIL_MARFRIG [KG]','MEAT BEEF BRISKET BRAZIL_MARFRIG [KG]',NULL,98000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(33,'88101201','MEAT BEEF TOPSIDE BRAZIL_MARFRIG [KG]','MEAT BEEF TOPSIDE BRAZIL_MARFRIG [KG]',NULL,106500.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(34,'88101401','MEAT BEEF BRISKET SHORT PLATE [KG]','MEAT BEEF BRISKET SHORT PLATE [KG]',NULL,130000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(35,'88101601','MEAT BEEF FAT - BODY FAT_JOHN DEE [KG]','MEAT BEEF FAT - BODY FAT_JOHN DEE [KG]',NULL,33000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(36,'88101801','MEAT BEEF FAT TRIMMING_HARDWICKS [KG]','MEAT BEEF FAT TRIMMING_HARDWICKS [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(37,'88102001','MEAT BEEF DMM_AMG [KG]','MEAT BEEF DMM_AMG [KG]',NULL,30000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(38,'88102201','MEAT MUTTON BONELESS_MIDFIELD [KG]','MEAT MUTTON BONELESS_MIDFIELD [KG]',NULL,140000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(39,'88102401','MEAT BEEF LIGAMENTUM NUCHAE (URAT MAYANG)_JOHN DEE [KG]','MEAT BEEF LIGAMENTUM NUCHAE (URAT MAYANG)_JOHN DEE [KG]',NULL,47000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(40,'88102601','MEAT BEEF OXTAIL_RIVERLANDS [KG]','MEAT BEEF OXTAIL_RIVERLANDS [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(41,'88120008','MEAT CHICKEN BREAST FRESH BLD_KARTIKA [KG]','MEAT CHICKEN BREAST FRESH BLD_KARTIKA [KG]',NULL,45300.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(42,'88120101','MEAT CHICKEN BREAST SKINLESS BLD_WALUYO [KG]','MEAT CHICKEN BREAST SKINLESS BLD_WALUYO [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(43,'88120205','MEAT CHICKEN WHOLE_SO GOOD[KG]','MEAT CHICKEN WHOLE_SO GOOD[KG]',NULL,30000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(44,'88120301','MEAT CHICKEN LEG SKINLESS BLP_WALUYO [KG]','MEAT CHICKEN LEG SKINLESS BLP_WALUYO [KG]',NULL,40000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(45,'88120302','MEAT CHICKEN LEG SKINLESS BLP_PPC [KG]','MEAT CHICKEN LEG SKINLESS BLP_PPC [KG]',NULL,40000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(46,'88120303','MEAT CHICKEN LEG SKINLESS BLP_AGRO [KG]','MEAT CHICKEN LEG SKINLESS BLP_AGRO [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(47,'88120305','MEAT CHICKEN LEG SKINLESS BLP_SREEYA [KG]','MEAT CHICKEN LEG SKINLESS BLP_SREEYA [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(48,'88120306','MEAT CHICKEN LEG SKINLESS BLP_SO GOOD [KG]','MEAT CHICKEN LEG SKINLESS BLP_SO GOOD [KG]',NULL,NULL,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(49,'88120406','MEAT CHICKEN SKIN / KULIT AYAM_RJN [KG]','MEAT CHICKEN SKIN / KULIT AYAM_RJN [KG]',NULL,26000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(50,'88120504','MEAT CHICKEN FAT / MINYAK AYAM_SREEYA [KG]','MEAT CHICKEN FAT / MINYAK AYAM_SREEYA [KG]',NULL,20000.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(51,'88120605','MEAT CHICKEN MDM_SO GOOD [KG]','MEAT CHICKEN MDM_SO GOOD [KG]',NULL,16775.0000,_binary '',_binary '',1,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(52,'88200001','BUMBU ANGKAK BUBUK @1 KG/PAK[KG]','BUMBU ANGKAK BUBUK @1 KG/PAK[KG]',NULL,85000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(53,'88200002','BUMBU PALA BUBUK @1 KG/PAK [KG]','BUMBU PALA BUBUK @1 KG/PAK [KG]',NULL,85000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(54,'88200003','BUMBU FENNEL SEED POWDER @1 KG/PAK [KG]','BUMBU FENNEL SEED POWDER @1 KG/PAK [KG]',NULL,85000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(55,'88200004','BUMBU JINTEN BUBUK @1 KG/PAK [KG]','BUMBU JINTEN BUBUK @1 KG/PAK [KG]',NULL,67000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(56,'88200005','BUMBU KETUMBAR BUBUK @1 KG/PAK [KG]','BUMBU KETUMBAR BUBUK @1 KG/PAK [KG]',NULL,35000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(57,'88200006','BUMBU LADA HITAM BUBUK @1 KG/PAK [KG]','BUMBU LADA HITAM BUBUK @1 KG/PAK [KG]',NULL,90000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(58,'88200007','BUMBU LADA PUTIH BUBUK @1 KG/PAK [KG]','BUMBU LADA PUTIH BUBUK @1 KG/PAK [KG]',NULL,85000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(59,'88200008','BUMBU JAHE BUBUK @ KG/PAK [KG]','BUMBU JAHE BUBUK @ KG/PAK [KG]',NULL,65000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(60,'88200009','BUMBU CABE BUBUK @1 KG/PAK [KG]','BUMBU CABE BUBUK @1 KG/PAK [KG]',NULL,87000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(61,'88200010','BUMBU STAR ANISE / PEKAK @1 KG/PAK [KG]','BUMBU STAR ANISE / PEKAK @1 KG/PAK [KG]',NULL,180000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(62,'88200101','BUMBU KETUMBAR BUTIR @1 KG/PAK [KG]','BUMBU KETUMBAR BUTIR @1 KG/PAK [KG]',NULL,27000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(63,'88200102','BUMBU LADA HITAM BUTIR @5 KG/PAK [KG]','BUMBU LADA HITAM BUTIR @5 KG/PAK [KG]',NULL,116000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(64,'88200103','BUMBU LADA PUTIH BUTIR @5 KG/PAK [KG]','BUMBU LADA PUTIH BUTIR @5 KG/PAK [KG]',NULL,156000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(65,'88200104','BUMBU LADA HITAM PECAH @5 KG/PAK [KG]','BUMBU LADA HITAM PECAH @5 KG/PAK [KG]',NULL,130000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(66,'88200105','BUMBU MUSTARD SEED WHOLE LOKAL @1 KG/PAK [KG]','BUMBU MUSTARD SEED WHOLE LOKAL @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(67,'88200201','BUMBU BAWANG BOMBAY @ KG/BAL [KG]','BUMBU BAWANG BOMBAY @ KG/BAL [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(68,'88200202','BUMBU BAWANG PUTIH @ KG/BAL [KG]','BUMBU BAWANG PUTIH @ KG/BAL [KG]',NULL,38000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(69,'88200203','BUMBU JAHE FRESH @ KG/PAK [KG]','BUMBU JAHE FRESH @ KG/PAK [KG]',NULL,42000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(70,'88200204','BUMBU PAPRIKA HIJAU SEGAR @ KG/PAK [KG]','BUMBU PAPRIKA HIJAU SEGAR @ KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(71,'88200205','BUMBU PAPRIKA MERAH SEGAR @ KG/PAK [KG]','BUMBU PAPRIKA MERAH SEGAR @ KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(72,'88200301','BUMBU GULA PASIR @1 KG/PAK [KG]','BUMBU GULA PASIR @1 KG/PAK [KG]',NULL,17500.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:03',NULL),(73,'88200302','BUMBU GARAM @50 KG/BAL [KG]','BUMBU GARAM @50 KG/BAL [KG]',NULL,3750.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(74,'88200303','BUMBU MSG SASA @25 KG/ZAK [KG]','BUMBU MSG SASA @25 KG/ZAK [KG]',NULL,31531.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(75,'88200401','BUMBU SAGU AREN @50 KG/BAL [KG]','BUMBU SAGU AREN @50 KG/BAL [KG]',NULL,10500.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(76,'88200402','BUMBU TERIGU SEGITIGA BIRU @25 KG/BAL [KG]','BUMBU TERIGU SEGITIGA BIRU @25 KG/BAL [KG]',NULL,8500.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(77,'88200403','BUMBU TAPIOKA STARCH SPM [KG]','BUMBU TAPIOKA STARCH SPM [KG]',NULL,9000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(78,'88200501','BUMBU ROYCO SAPI @ 1 KG/PAK [KG]','BUMBU ROYCO SAPI @ 1 KG/PAK [KG]',NULL,37000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(79,'88200502','BUMBU ROYCO AYAM @ 1 KG/PAK [KG]','BUMBU ROYCO AYAM @ 1 KG/PAK [KG]',NULL,37000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(80,'88200503','BUMBU SEMI REFINE KAPPA CARRAGEENAN KRI-02 @25 KG/SAK [KG]','BUMBU SEMI REFINE KAPPA CARRAGEENAN KRI-02 @25 KG/SAK [KG]',NULL,115000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(81,'88200504','BUMBU BRINE SEASONING @1 KG/PAK [KG]','BUMBU BRINE SEASONING @1 KG/PAK [KG]',NULL,8.4000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(82,'88200505','BUMBU KREATION 20 CL @25 KG/SAK [KG]','BUMBU KREATION 20 CL @25 KG/SAK [KG]',NULL,0.9300,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(83,'88200506','BUMBU MEAT EXTENDER @1 KG/PAK [KG]','BUMBU MEAT EXTENDER @1 KG/PAK [KG]',NULL,20.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(84,'88200507','BUMBU MUSTARD FLOUR IMPORT @1 KG/PAK [KG]','BUMBU MUSTARD FLOUR IMPORT @1 KG/PAK [KG]',NULL,12.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(85,'88200508','BUMBU NEW GUM 4208 @20 KG/DUS [KG]','BUMBU NEW GUM 4208 @20 KG/DUS [KG]',NULL,9.5000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(86,'88200509','BUMBU NEW GUM 6240 @20 KG/DUS [KG]','BUMBU NEW GUM 6240 @20 KG/DUS [KG]',NULL,13.5000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(87,'88200510','BUMBU NEW PRO 90 @20 KG/SAK [KG]','BUMBU NEW PRO 90 @20 KG/SAK [KG]',NULL,3.9000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(88,'88200511','BUMBU COAT ROLL @10 KG/DUS [KG]','BUMBU COAT ROLL @10 KG/DUS [KG]',NULL,7.7500,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(89,'88200512','BUMBU PAPRIKA DELICACY RIO @1 KG/PAK [KG]','BUMBU PAPRIKA DELICACY RIO @1 KG/PAK [KG]',NULL,17.5000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(90,'88200513','BUMBU RSC 7 ADDITIVE @1 KG/PAK [KG]','BUMBU RSC 7 ADDITIVE @1 KG/PAK [KG]',NULL,8.8000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(91,'88200514','BUMBU WENDAPHOS M11 @25KG/SAK [KG]','BUMBU WENDAPHOS M11 @25KG/SAK [KG]',NULL,59000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(92,'88200515','BUMBU WENDAPHOS M17 @25KG/SAK [KG]','BUMBU WENDAPHOS M17 @25KG/SAK [KG]',NULL,56000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(93,'88200516','BUMBU WENDA BRINE & CURE MIX C100 @ 25KG/SAK [KG]','BUMBU WENDA BRINE & CURE MIX C100 @ 25KG/SAK [KG]',NULL,63000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(94,'88200517','BUMBU WENDA BRINE & CURE MP8 @25KG/SAK [KG]','BUMBU WENDA BRINE & CURE MP8 @25KG/SAK [KG]',NULL,40000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(95,'88200518','BUMBU TRANSGLUTAMINASE - TIPE: PROLINK-BS @10KG/DUS [KG]','BUMBU TRANSGLUTAMINASE - TIPE: PROLINK-BS @10KG/DUS [KG]',NULL,180000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(96,'88200519','BUMBU VEGE 860 @25 KG/SAK [KG]','BUMBU VEGE 860 @25 KG/SAK [KG]',NULL,33500.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(97,'88200520','BUMBU TREHA @20 KG/SAK [KG]','BUMBU TREHA @20 KG/SAK [KG]',NULL,73000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(98,'88200521','BUMBU GARLIC POWDER @1 KG/PAK [KG]','BUMBU GARLIC POWDER @1 KG/PAK [KG]',NULL,34000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(99,'88200522','BUMBU ITALIAN - PIZZA SOBRA \"II\" @1.5 KG/PAK [KG]','BUMBU ITALIAN - PIZZA SOBRA \"II\" @1.5 KG/PAK [KG]',NULL,246667.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(100,'88200523','BUMBU ROSEMARY CUT DRY @1 KG/PAK [KG]','BUMBU ROSEMARY CUT DRY @1 KG/PAK [KG]',NULL,390500.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(101,'88200524','BUMBU MUSTARD SEED WHOLE IMPOR @1 KG/PAK [KG]','BUMBU MUSTARD SEED WHOLE IMPOR @1 KG/PAK [KG]',NULL,277000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(102,'88200601','BUMBU NATRIUM NITRIT @ 25 KG/PAIL [KG]','BUMBU NATRIUM NITRIT @ 25 KG/PAIL [KG]',NULL,29000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(103,'88200602','BUMBU POTASSIUM SORBATE @25 KG/DUS [KG]','BUMBU POTASSIUM SORBATE @25 KG/DUS [KG]',NULL,45000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(104,'88200603','BUMBU SODIUM BENZOATE @25 KG/SAK [KG]','BUMBU SODIUM BENZOATE @25 KG/SAK [KG]',NULL,54000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(105,'88200701','BUMBU KACANG PISTACIOS @ KG/PAK [KG]','BUMBU KACANG PISTACIOS @ KG/PAK [KG]',NULL,220000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(106,'88200702','BUMBU TELUR [KG]','BUMBU TELUR [KG]',NULL,30000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(107,'88200703','BUMBU KEJU @2 KG/DUS [KG]','BUMBU KEJU @2 KG/DUS [KG]',NULL,72500.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(108,'88200801','BUMBU MINYAK SAYUR NABATI @16 KG/CTN [KG]','BUMBU MINYAK SAYUR NABATI @16 KG/CTN [KG]',NULL,330000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(109,'88200802','BUMBU RED ARROW SMOKE C-10 @20 KG/JERIGEN [KG]','BUMBU RED ARROW SMOKE C-10 @20 KG/JERIGEN [KG]',NULL,123500.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(110,'88200803','BUMBU SYNTHITE OLEORESIN PAPRIKA 4010000534 @5 KG/PAIL [KG]','BUMBU SYNTHITE OLEORESIN PAPRIKA 4010000534 @5 KG/PAIL [KG]',NULL,15.5000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(111,'88200901','BUMBU RAUCHER GOLD @15 KG/SAK [KG]','BUMBU RAUCHER GOLD @15 KG/SAK [KG]',NULL,14500.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(112,'88200902','BUMBU RAUCHER GOLD KL 2 - 16 @15 KG/SAK [KG]','BUMBU RAUCHER GOLD KL 2 - 16 @15 KG/SAK [KG]',NULL,14000.0000,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(113,'88210001','BUMBU AVO BUAH JENIPER @1 KG/PAK [KG]','BUMBU AVO BUAH JENIPER @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(114,'88210002','BUMBU AVO DAUN MAYORAM @5 KG/PAK [KG]','BUMBU AVO DAUN MAYORAM @5 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(115,'88210003','BUMBU AVO AUFSCHNITT @1 KG/PAK [KG]','BUMBU AVO AUFSCHNITT @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(116,'88210004','BUMBU AVO BOCKWURST @1 KG/PAK [KG]','BUMBU AVO BOCKWURST @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(117,'88210005','BUMBU AVO FRANKFURTER STYLE @1 KG/PAK [KG]','BUMBU AVO FRANKFURTER STYLE @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(118,'88210006','BUMBU AVO KRAKAUER @1 KG/PAK [KG]','BUMBU AVO KRAKAUER @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(119,'88210007','BUMBU AVO BRAVO @1 KG/PAK [KG]','BUMBU AVO BRAVO @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(120,'88210008','BUMBU AVO MEAT LOAF @1 KG/PAK [KG]','BUMBU AVO MEAT LOAF @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(121,'88210009','BUMBU AVO PAPRIKA FLAKES GREEN @1 KG/PAK [KG]','BUMBU AVO PAPRIKA FLAKES GREEN @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(122,'88210010','BUMBU AVO PAPRIKA FLAKES RED @1 KG/PAK [KG]','BUMBU AVO PAPRIKA FLAKES RED @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(123,'88210011','BUMBU AVO SALAMI @1 KG/PAK [KG]','BUMBU AVO SALAMI @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(124,'88210012','BUMBU AVO WIENER @1 KG/PAK [KG]','BUMBU AVO WIENER @1 KG/PAK [KG]',NULL,NULL,_binary '',_binary '',2,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(125,'88300201','CASING FIBROUS (VISCO) CLEAR PK D60 - 600M/ROLL [PCS]','CASING FIBROUS (VISCO) CLEAR PK D60 - 600M/ROLL [PCS]',NULL,468.0000,_binary '',_binary '',3,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(126,'88300202','CASING FIBROUS (VISCO) REGULAR CLEAR D111 - 600M/ROLL [PCS]','CASING FIBROUS (VISCO) REGULAR CLEAR D111 - 600M/ROLL [PCS]',NULL,745.2000,_binary '',_binary '',3,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(127,'88300203','CASING FIBROUS (VISCO) REGULAR CLEAR WHITE D45 V-1S [PCS]','CASING FIBROUS (VISCO) REGULAR CLEAR WHITE D45 V-1S [PCS]',NULL,0.6000,_binary '',_binary '',3,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(128,'88300204','CASING FIBROUS (NALO) REGULAR CLEAR D110 - 600M/ROLL [PCS]','CASING FIBROUS (NALO) REGULAR CLEAR D110 - 600M/ROLL [PCS]',NULL,NULL,_binary '',_binary '',3,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(129,'88300301','CASING COLD CUT (NALO) BROWN D90 - 400M/ROLL [PCS]','CASING COLD CUT (NALO) BROWN D90 - 400M/ROLL [PCS]',NULL,NULL,_binary '',_binary '',3,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(130,'88300302','CASING COLD CUT (NALO) RED D90 - 400M/ROLL [PCS]','CASING COLD CUT (NALO) RED D90 - 400M/ROLL [PCS]',NULL,NULL,_binary '',_binary '',3,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(131,'88300303','CASING COLD CUT (NALO) YELLOW D90 - 400M/ROLL [PCS]','CASING COLD CUT (NALO) YELLOW D90 - 400M/ROLL [PCS]',NULL,NULL,_binary '',_binary '',3,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(132,'88400001','KEMASAN BAG NYLON DELIKATESSA 250 GR SIZE (170 x 240 mm) [PCS]','KEMASAN BAG NYLON DELIKATESSA 250 GR SIZE (170 x 240 mm) [PCS]',NULL,600.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(133,'88400002','KEMASAN BAG NYLON POLOS SIZE (165 x 270 x 2 mm) [PCS]','KEMASAN BAG NYLON POLOS SIZE (165 x 270 x 2 mm) [PCS]',NULL,500.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(134,'88400003','KEMASAN BAG NYLON POLOS SIZE (200 x 300 x 2 mm) [PCS]','KEMASAN BAG NYLON POLOS SIZE (200 x 300 x 2 mm) [PCS]',NULL,750.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(135,'88400004','KEMASAN BAG NYLON POLOS SIZE (300 x 250 x 2 mm) [PCS]','KEMASAN BAG NYLON POLOS SIZE (300 x 250 x 2 mm) [PCS]',NULL,900.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(136,'88400005','KEMASAN BAG NYLON POLOS SIZE (280 x 300 x 2 mm) [PCS]','KEMASAN BAG NYLON POLOS SIZE (280 x 300 x 2 mm) [PCS]',NULL,1000.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(137,'88400006','KEMASAN BAG NYLON POLOS SIZE (320 X 530 X 2 mm) [PCS]','KEMASAN BAG NYLON POLOS SIZE (320 X 530 X 2 mm) [PCS]',NULL,1750.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(138,'88400007','KEMASAN BAG NYLON POLOS SIZE (340 x 300 x 2 mm) [PCS]','KEMASAN BAG NYLON POLOS SIZE (340 x 300 x 2 mm) [PCS]',NULL,1200.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(139,'88400201','KEMASAN PLASTIK 15 x 30 [PAK]','KEMASAN PLASTIK 15 x 30 [PAK]',NULL,9500.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(140,'88400202','KEMASAN PLASTIK 20 x 40 [PAK]','KEMASAN PLASTIK 20 x 40 [PAK]',NULL,9500.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(141,'88400203','KEMASAN PLASTIK 30 x 45 [PAK]','KEMASAN PLASTIK 30 x 45 [PAK]',NULL,19000.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(142,'88400204','KEMASAN PLASTIK 40 x 60 [PAK]','KEMASAN PLASTIK 40 x 60 [PAK]',NULL,19000.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(143,'88400301','KEMASAN KARDUS DAGING (39x33x18.5) [PCS]','KEMASAN KARDUS DAGING (39x33x18.5) [PCS]',NULL,6463.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(144,'88400302','KEMASAN KARDUS PAKET (53.5x38.5x35.7) [PCS]','KEMASAN KARDUS PAKET (53.5x38.5x35.7) [PCS]',NULL,6440.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(145,'88400303','KEMASAN KARDUS SUPERMARKET (41x26x18.5) [PCS]','KEMASAN KARDUS SUPERMARKET (41x26x18.5) [PCS]',NULL,3623.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(146,'88400304','KEMASAN KARDUS QUIZNOS (53x35x23) [PCS]','KEMASAN KARDUS QUIZNOS (53x35x23) [PCS]',NULL,8913.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(147,'88400401','KEMASAN STYROFOAM BESAR [PCS]','KEMASAN STYROFOAM BESAR [PCS]',NULL,22000.0000,_binary '',_binary '',4,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(148,'88410001','STICKER KERTAS BEEF SAUSAGE [PCS]','STICKER KERTAS BEEF SAUSAGE [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(149,'88410002','STICKER KERTAS BEEF BREAKFAST [PCS]','STICKER KERTAS BEEF BREAKFAST [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(150,'88410003','STICKER KERTAS BEEF FRANKFURTER [PCS]','STICKER KERTAS BEEF FRANKFURTER [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(151,'88410004','STICKER KERTAS BEEF BOCKWURST [PCS]','STICKER KERTAS BEEF BOCKWURST [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(152,'88410005','STICKER KERTAS BEEF BRATWURST [PCS]','STICKER KERTAS BEEF BRATWURST [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(153,'88410006','STICKER KERTAS BEEF BLACK PEPPER [PCS]','STICKER KERTAS BEEF BLACK PEPPER [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(154,'88410007','STICKER KERTAS BEEF SPICY KRAKAUER [PCS]','STICKER KERTAS BEEF SPICY KRAKAUER [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(155,'88410008','STICKER KERTAS BEEF CHEESE [PCS]','STICKER KERTAS BEEF CHEESE [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(156,'88410101','STICKER KERTAS SMOKED BEEF RIB [PCS]','STICKER KERTAS SMOKED BEEF RIB [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(157,'88410201','STICKER KERTAS SMOKED COOKED BEEF [PCS]','STICKER KERTAS SMOKED COOKED BEEF [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(158,'88410202','STICKER KERTAS BEEF SANDWICH [PCS]','STICKER KERTAS BEEF SANDWICH [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(159,'88410203','STICKER KERTAS BEEF PASTRAMI [PCS]','STICKER KERTAS BEEF PASTRAMI [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:04',NULL),(160,'88410301','STICKER KERTAS BEEF MEAT LOAF [PCS]','STICKER KERTAS BEEF MEAT LOAF [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(161,'88410302','STICKER KERTAS BEEF LYONER [PCS]','STICKER KERTAS BEEF LYONER [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(162,'88410303','STICKER KERTAS BEEF JAGDWURST [PCS]','STICKER KERTAS BEEF JAGDWURST [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(163,'88410401','STICKER KERTAS BEEF MINCED [PCS]','STICKER KERTAS BEEF MINCED [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(164,'88410501','STICKER KERTAS BEEF SALAMI [PCS]','STICKER KERTAS BEEF SALAMI [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(165,'88410502','STICKER KERTAS BEEF PEPPERONI DELIKATESSA [PCS]','STICKER KERTAS BEEF PEPPERONI DELIKATESSA [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(166,'88410601','STICKER KERTAS BASO SAPI [PCS]','STICKER KERTAS BASO SAPI [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(167,'88420001','STICKER KERTAS CHICKEN SAUSAGE [PCS]','STICKER KERTAS CHICKEN SAUSAGE [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(168,'88420002','STICKER KERTAS CHICKEN BREAKFAST [PCS]','STICKER KERTAS CHICKEN BREAKFAST [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(169,'88420003','STICKER KERTAS CHICKEN FRANKFURTER [PCS]','STICKER KERTAS CHICKEN FRANKFURTER [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(170,'88420004','STICKER KERTAS CHICKEN BRATWURST [PCS]','STICKER KERTAS CHICKEN BRATWURST [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(171,'88420005','STICKER KERTAS CHICKEN CHEESE FRANKFURTER [PCS]','STICKER KERTAS CHICKEN CHEESE FRANKFURTER [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(172,'88420101','STICKER KERTAS CHICKEN RASHERS [PCS]','STICKER KERTAS CHICKEN RASHERS [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(173,'88420102','STICKER KERTAS CHICKEN SMOKED BREAST [PCS]','STICKER KERTAS CHICKEN SMOKED BREAST [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(174,'88420201','STICKER KERTAS SMOKED CHICKEN ROLL [PCS]','STICKER KERTAS SMOKED CHICKEN ROLL [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(175,'88420202','STICKER KERTAS CHICKEN PASTRAMI [PCS]','STICKER KERTAS CHICKEN PASTRAMI [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(176,'88420301','STICKER KERTAS CHICKEN MEAT LOAF [PCS]','STICKER KERTAS CHICKEN MEAT LOAF [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(177,'88420302','STICKER KERTAS CHICKEN LYONER [PCS]','STICKER KERTAS CHICKEN LYONER [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(178,'88420303','STICKER KERTAS CHICKEN JAGDWURST [PCS]','STICKER KERTAS CHICKEN JAGDWURST [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(179,'88420304','STICKER KERTAS CHICKEN MORTADELLA [PCS]','STICKER KERTAS CHICKEN MORTADELLA [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(180,'88420305','STICKER KERTAS CHICKEN MUSHROOM [PCS]','STICKER KERTAS CHICKEN MUSHROOM [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(181,'88420306','STICKER KERTAS CHICKEN PAPRIKA [PCS]','STICKER KERTAS CHICKEN PAPRIKA [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(182,'88420401','STICKER KERTAS CHICKEN MINCED [PCS]','STICKER KERTAS CHICKEN MINCED [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(183,'88420601','STICKER KERTAS BASO AYAM [PCS]','STICKER KERTAS BASO AYAM [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(184,'88430001','STICKER PLASTIK BEEF SAUSAGE [PCS]','STICKER PLASTIK BEEF SAUSAGE [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(185,'88430002','STICKER PLASTIK BEEF BREAKFAST [PCS]','STICKER PLASTIK BEEF BREAKFAST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(186,'88430003','STICKER PLASTIK BEEF FRANKFURTER [PCS]','STICKER PLASTIK BEEF FRANKFURTER [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(187,'88430004','STICKER PLASTIK BEEF BOCKWURST [PCS]','STICKER PLASTIK BEEF BOCKWURST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(188,'88430005','STICKER PLASTIK BEEF BRATWURST [PCS]','STICKER PLASTIK BEEF BRATWURST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(189,'88430006','STICKER PLASTIK BEEF BLACK PEPPER [PCS]','STICKER PLASTIK BEEF BLACK PEPPER [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(190,'88430007','STICKER PLASTIK BEEF SPICY KRAKAUER [PCS]','STICKER PLASTIK BEEF SPICY KRAKAUER [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(191,'88430008','STICKER PLASTIK BEEF CHEESE SAUSAGE [PCS]','STICKER PLASTIK BEEF CHEESE SAUSAGE [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(192,'88430009','STICKER PLASTIK BEEF CHEESE FRANKFURTER [PCS]','STICKER PLASTIK BEEF CHEESE FRANKFURTER [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(193,'88430010','STICKER PLASTIK BEEF CHEESE KREINER [PCS]','STICKER PLASTIK BEEF CHEESE KREINER [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(194,'88430101','STICKER PLASTIK SMOKED BEEF RIB [PCS]','STICKER PLASTIK SMOKED BEEF RIB [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(195,'88430201','STICKER PLASTIK BEEF COOKED SMOKED [PCS]','STICKER PLASTIK BEEF COOKED SMOKED [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(196,'88430202','STICKER PLASTIK BEEF COOKED FOR SANDWICH [PCS]','STICKER PLASTIK BEEF COOKED FOR SANDWICH [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(197,'88430203','STICKER PLASTIK BEEF PASTRAMI [PCS]','STICKER PLASTIK BEEF PASTRAMI [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(198,'88430301','STICKER PLASTIK BEEF MEAT LOAF [PCS]','STICKER PLASTIK BEEF MEAT LOAF [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(199,'88430302','STICKER PLASTIK BEEF LYONER [PCS]','STICKER PLASTIK BEEF LYONER [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(200,'88430303','STICKER PLASTIK BEEF JAGDWURST [PCS]','STICKER PLASTIK BEEF JAGDWURST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(201,'88430501','STICKER PLASTIK BEEF SALAMI [PCS]','STICKER PLASTIK BEEF SALAMI [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(202,'88430601','STICKER PLASTIK BASO SAPI [PCS]','STICKER PLASTIK BASO SAPI [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(203,'88440001','STICKER PLASTIK CHICKEN SAUSAGE [PCS]','STICKER PLASTIK CHICKEN SAUSAGE [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(204,'88440002','STICKER PLASTIK CHICKEN BREAKFAST [PCS]','STICKER PLASTIK CHICKEN BREAKFAST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(205,'88440003','STICKER PLASTIK CHICKEN FRANKFURTER [PCS]','STICKER PLASTIK CHICKEN FRANKFURTER [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(206,'88440004','STICKER PLASTIK CHICKEN HERBS KNACKWURST [PCS]','STICKER PLASTIK CHICKEN HERBS KNACKWURST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(207,'88440005','STICKER PLASTIK CHICKEN HOT DOG [PCS]','STICKER PLASTIK CHICKEN HOT DOG [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(208,'88440006','STICKER PLASTIK CHICKEN BRATWURST [PCS]','STICKER PLASTIK CHICKEN BRATWURST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(209,'88440007','STICKER PLASTIK CHICKEN CHEESE FRANKFURTER [PCS]','STICKER PLASTIK CHICKEN CHEESE FRANKFURTER [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(210,'88440008','STICKER PLASTIK CHICKEN CHEESE KNACKWURST [PCS]','STICKER PLASTIK CHICKEN CHEESE KNACKWURST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(211,'88440102','STICKER PLASTIK CHICKEN SMOKED BREAST [PCS]','STICKER PLASTIK CHICKEN SMOKED BREAST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(212,'88440201','STICKER PLASTIK CHICKEN SMOKED ROLL [PCS]','STICKER PLASTIK CHICKEN SMOKED ROLL [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(213,'88440301','STICKER PLASTIK CHICKEN MEAT LOAF [PCS]','STICKER PLASTIK CHICKEN MEAT LOAF [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(214,'88440302','STICKER PLASTIK CHICKEN LYONER [PCS]','STICKER PLASTIK CHICKEN LYONER [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(215,'88440303','STICKER PLASTIK CHICKEN JAGDWURST [PCS]','STICKER PLASTIK CHICKEN JAGDWURST [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(216,'88440304','STICKER PLASTIK CHICKEN MORTADELLA [PCS]','STICKER PLASTIK CHICKEN MORTADELLA [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(217,'88440305','STICKER PLASTIK CHICKEN MUSHROOM [PCS]','STICKER PLASTIK CHICKEN MUSHROOM [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(218,'88440306','STICKER PLASTIK CHICKEN PAPRIKA [PCS]','STICKER PLASTIK CHICKEN PAPRIKA [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(219,'88640601','STICKER PLASTIK BASO AYAM [PCS]','STICKER PLASTIK BASO AYAM [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(220,'88450001','STICKER KERTAS BEEF SAUSAGE MEATMAN [PCS]','STICKER KERTAS BEEF SAUSAGE MEATMAN [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(221,'88450002','STICKER KERTAS BEEF BREAKFAST MEATMAN [PCS]','STICKER KERTAS BEEF BREAKFAST MEATMAN [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(222,'88450201','STICKER KERTAS SMOKED BEEF MEATMAN [PCS]','STICKER KERTAS SMOKED BEEF MEATMAN [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(223,'88460001','STICKER KERTAS CHICKEN SAUSAGE MEATMAN [PCS]','STICKER KERTAS CHICKEN SAUSAGE MEATMAN [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(224,'88460002','STICKER KERTAS CHICKEN BREAKFAST MEATMAN [PCS]','STICKER KERTAS CHICKEN BREAKFAST MEATMAN [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(225,'88460201','STICKER KERTAS SMOKED COOKED CHICKEN MEATMAN [PCS]','STICKER KERTAS SMOKED COOKED CHICKEN MEATMAN [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(226,'88460202','STICKER KERTAS COOKED CHICKEN FOR SANDWICH MEATMAN [PCS]','STICKER KERTAS COOKED CHICKEN FOR SANDWICH MEATMAN [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(227,'88470001','STICKER KERTAS VEAL SAUSAGE [PCS]','STICKER KERTAS VEAL SAUSAGE [PCS]',NULL,500.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(228,'88470002','STICKER PLASTIK VEAL SAUSAGE [PCS]','STICKER PLASTIK VEAL SAUSAGE [PCS]',NULL,375.0000,_binary '',_binary '',8,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(229,'88500001','SABUN SANITIZER GP CLEAN @25 LTR/JRG [LITER]','SABUN SANITIZER GP CLEAN @25 LTR/JRG [LITER]',NULL,19683.4800,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(230,'88500002','SABUN SANITIZER KLEANAL-EX @25 LTR/JRG [LITER]','SABUN SANITIZER KLEANAL-EX @25 LTR/JRG [LITER]',NULL,54188.7600,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(231,'88500003','SABUN SANITIZER WASH SANITIZER @25 LTR/JRG [LITER]','SABUN SANITIZER WASH SANITIZER @25 LTR/JRG [LITER]',NULL,28416.0400,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(232,'88500004','SABUN SANITIZER PROVEN @25 LTR/JRG [LITER]','SABUN SANITIZER PROVEN @25 LTR/JRG [LITER]',NULL,42293.6800,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(233,'88500005','SABUN SANITIZER PROCESS @25 LTR/JRG [LITER]','SABUN SANITIZER PROCESS @25 LTR/JRG [LITER]',NULL,65931.5200,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(234,'88500006','SABUN SANITIZER QUEST @25 LTR/JRG [LITER]','SABUN SANITIZER QUEST @25 LTR/JRG [LITER]',NULL,39234.2000,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(235,'88500007','SABUN SANITIZER SHIELD @5 LTR/JRG [LITER]','SABUN SANITIZER SHIELD @5 LTR/JRG [LITER]',NULL,55898.4000,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(236,'88500008','SABUN SANITIZER SAFEWAY @5 LTR/JRG [LITER]','SABUN SANITIZER SAFEWAY @5 LTR/JRG [LITER]',NULL,18463.0000,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(237,'88500009','SABUN SANITIZER KLORIN [KG]','SABUN SANITIZER KLORIN [KG]',NULL,60000.0000,_binary '',_binary '',9,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(238,'88500051','SABUN SANITIZER PRIDE BUBUK @5 KG/DUS [KG]','SABUN SANITIZER PRIDE BUBUK @5 KG/DUS [KG]',NULL,41434.6000,_binary '',_binary '',9,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(239,'88500052','SABUN SANITIZER CARE @25 LTR/JRG [LITER]','SABUN SANITIZER CARE @25 LTR/JRG [LITER]',NULL,23790.2000,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(240,'88500053','SABUN SANITIZER VISION @5 LTR/JRG [LITER]','SABUN SANITIZER VISION @5 LTR/JRG [LITER]',NULL,16719.2000,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(241,'88500054','SABUN SANITIZER BOWL CIDE @5 LTR/JRG [LITER]','SABUN SANITIZER BOWL CIDE @5 LTR/JRG [LITER]',NULL,23117.2000,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(242,'88500055','SABUN SANITIZER PROTIK @5 LTR/JRG [LITER]','SABUN SANITIZER PROTIK @5 LTR/JRG [LITER]',NULL,26241.2000,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(243,'88500071','SABUN LAUNDRY SEARCH DETERGENT @5 LTR/JRG (PROTEK) [LTR]','SABUN LAUNDRY SEARCH DETERGENT @5 LTR/JRG (PROTEK) [LTR]',NULL,48866.6000,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(244,'88500072','SABUN LAUNDRY ACTION EMULSIFIER @5 LTR/JRG (PROTEK) [LTR]','SABUN LAUNDRY ACTION EMULSIFIER @5 LTR/JRG (PROTEK) [LTR]',NULL,48358.4000,_binary '',_binary '',9,8,8,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(245,'88500101','PERLENGKAPAN SOLVENT CLEANER / MEK 77001 - 00030 - 1 @ 5 ltr / Jrg [JRG]','PERLENGKAPAN SOLVENT CLEANER / MEK 77001 - 00030 - 1 @ 5 ltr / Jrg [JRG]',NULL,75.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(246,'88500102','PERLENGKAPAN SOLVENT STANDART 77001 - 00030 @ 950ml / Botol [BTL]','PERLENGKAPAN SOLVENT STANDART 77001 - 00030 @ 950ml / Botol [BTL]',NULL,35.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(247,'88500103','PERLENGKAPAN INK BLACK KETONE NPT with TAG 70000 - 00030@ 950ml / Botol [BTL]','PERLENGKAPAN INK BLACK KETONE NPT with TAG 70000 - 00030@ 950ml / Botol [BTL]',NULL,146.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(248,'88500104','PERLENGKAPAN TINER [TIN]','PERLENGKAPAN TINER [TIN]',NULL,NULL,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(249,'88500151','WATER OVERTON 08 - PH CONTROLLING [KG]','WATER OVERTON 08 - PH CONTROLLING [KG]',NULL,3500.0000,_binary '',_binary '',5,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:05',NULL),(250,'88500152','WATER OVERTON 09 - COAGULANT [KG]','WATER OVERTON 09 - COAGULANT [KG]',NULL,4000.0000,_binary '',_binary '',5,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(251,'88500153','WATER OVERTON 10 - FLOCULANT [KG]','WATER OVERTON 10 - FLOCULANT [KG]',NULL,6000.0000,_binary '',_binary '',5,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(252,'88500154','WATER HYPOCLORIDE - DISINFEKTAN SODIUM [KG]','WATER HYPOCLORIDE - DISINFEKTAN SODIUM [KG]',NULL,NULL,_binary '',_binary '',5,4,4,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(253,'88500201','PERLENGKAPAN LAKBAN 24 MM X 72 YARD [PCS]','PERLENGKAPAN LAKBAN 24 MM X 72 YARD [PCS]',NULL,2800.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(254,'88500202','PERLENGKAPAN LAKBAN 48 MM X 100 YARD [PCS]','PERLENGKAPAN LAKBAN 48 MM X 100 YARD [PCS]',NULL,7000.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(255,'88500203','PERLENGKAPAN LAKBAN 48 MM x 500 YARD [PCS]','PERLENGKAPAN LAKBAN 48 MM x 500 YARD [PCS]',NULL,35000.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(256,'88500231','PERLENGKAPAN KANTONG PLASTIK MERAH [PAK]','PERLENGKAPAN KANTONG PLASTIK MERAH [PAK]',NULL,NULL,_binary '',_binary '',5,1,1,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(257,'88500232','PERLENGKAPAN KANTONG PLASTIK PUTIH KECIL [PAK]','PERLENGKAPAN KANTONG PLASTIK PUTIH KECIL [PAK]',NULL,15000.0000,_binary '',_binary '',5,1,1,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(258,'88500251','PERLENGKAPAN PLASTIK SAMPAH 60 x 100 [PAK]','PERLENGKAPAN PLASTIK SAMPAH 60 x 100 [PAK]',NULL,15000.0000,_binary '',_binary '',5,1,1,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(259,'88500252','PERLENGKAPAN PLASTIK SAMPAH 90 x 120 [PAK]','PERLENGKAPAN PLASTIK SAMPAH 90 x 120 [PAK]',NULL,15000.0000,_binary '',_binary '',5,1,1,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(260,'88500271','PERLENGKAPAN CONTAINER PLASTIK BERLUBANG (CODE 2008) [PCS]','PERLENGKAPAN CONTAINER PLASTIK BERLUBANG (CODE 2008) [PCS]',NULL,121000.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(261,'88500272','PERLENGKAPAN CONTAINER PLASTIK RAPAT (CODE 2044) [PCS]','PERLENGKAPAN CONTAINER PLASTIK RAPAT (CODE 2044) [PCS]',NULL,120000.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(262,'88500301','PERLENGKAPAN GAS TABUNG LPG @50kg/TBG','PERLENGKAPAN GAS TABUNG LPG @50kg/TBG',NULL,900900.9000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(263,'88500302','PERLENGKAPAN GAS TABUNG LPG @12kg/TBG','PERLENGKAPAN GAS TABUNG LPG @12kg/TBG',NULL,191891.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(264,'88500331','PERLENGKAPAN POLY CLIP 735 [PAK]','PERLENGKAPAN POLY CLIP 735 [PAK]',NULL,NULL,_binary '',_binary '',5,1,1,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(265,'88500332','PERLENGKAPAN POLY CLIP 740 [PAK]','PERLENGKAPAN POLY CLIP 740 [PAK]',NULL,NULL,_binary '',_binary '',5,1,1,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(266,'88500351','PERLENGKAPAN WRAPPING FILM 30 [PCS]','PERLENGKAPAN WRAPPING FILM 30 [PCS]',NULL,11000.0000,_binary '',_binary '',5,3,3,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(267,'88500371','PERLENGKAPAN BENANG KASUR [PAK]','PERLENGKAPAN BENANG KASUR [PAK]',NULL,29000.0000,_binary '',_binary '',5,1,1,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL),(268,'88500372','PERLENGKAPAN SARUNG TANGAN PLASTIK [PAK]','PERLENGKAPAN SARUNG TANGAN PLASTIK [PAK]',NULL,4500.0000,_binary '',_binary '',5,1,1,1.0000,1.0000,100.00,'',NULL,'2025-02-16 18:44:06',NULL);
/*!40000 ALTER TABLE `tbrawmaterials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbrawmaterialscategory`
--

DROP TABLE IF EXISTS `tbrawmaterialscategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbrawmaterialscategory` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbrawmaterialscategory`
--

LOCK TABLES `tbrawmaterialscategory` WRITE;
/*!40000 ALTER TABLE `tbrawmaterialscategory` DISABLE KEYS */;
INSERT INTO `tbrawmaterialscategory` VALUES (1,'DAGING',''),(2,'BUMBU',''),(3,'CASING',''),(4,'KEMASAN',''),(5,'PERLENGKAPAN',''),(6,'SPAREPART',''),(7,'JASA',''),(8,'STICKER','STICKER'),(9,'SABUN','SABUN');
/*!40000 ALTER TABLE `tbrawmaterialscategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbrefund`
--

DROP TABLE IF EXISTS `tbrefund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbrefund` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `tbreturn` bigint DEFAULT NULL,
  `paymenttype` bigint DEFAULT NULL,
  `refunddate` datetime DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `remark` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-returnforrefund` (`tbreturn`),
  KEY `fk-typeforrefund` (`paymenttype`),
  CONSTRAINT `fk-returnforrefund` FOREIGN KEY (`tbreturn`) REFERENCES `tbreturn` (`id`),
  CONSTRAINT `fk-typeforrefund` FOREIGN KEY (`paymenttype`) REFERENCES `tbpaymenttype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbrefund`
--

LOCK TABLES `tbrefund` WRITE;
/*!40000 ALTER TABLE `tbrefund` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbrefund` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbrepackage`
--

DROP TABLE IF EXISTS `tbrepackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbrepackage` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `from_allocation_id` bigint DEFAULT NULL,
  `to_allocation_id` bigint DEFAULT NULL,
  `split` bigint DEFAULT NULL,
  `repackage` bigint DEFAULT NULL,
  `use_quantity` decimal(19,2) DEFAULT NULL,
  `waste_quantity` decimal(19,2) DEFAULT NULL,
  `target_quantity` decimal(19,2) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-toAllocation` (`to_allocation_id`),
  KEY `fk-fromAllocation` (`from_allocation_id`),
  KEY `fk-split` (`split`),
  KEY `fk-repackage` (`repackage`),
  CONSTRAINT `fk-fromAllocation` FOREIGN KEY (`from_allocation_id`) REFERENCES `tbinventoryallocation` (`id`),
  CONSTRAINT `fk-repackage` FOREIGN KEY (`repackage`) REFERENCES `tbinventorymovement` (`id`),
  CONSTRAINT `fk-split` FOREIGN KEY (`split`) REFERENCES `tbinventorymovement` (`id`),
  CONSTRAINT `fk-toAllocation` FOREIGN KEY (`to_allocation_id`) REFERENCES `tbinventoryallocation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbrepackage`
--

LOCK TABLES `tbrepackage` WRITE;
/*!40000 ALTER TABLE `tbrepackage` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbrepackage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbreport`
--

DROP TABLE IF EXISTS `tbreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbreport` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `value` varchar(512) DEFAULT NULL,
  `module` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbreport`
--

LOCK TABLES `tbreport` WRITE;
/*!40000 ALTER TABLE `tbreport` DISABLE KEYS */;
INSERT INTO `tbreport` VALUES (1,'reportserver.url','http://192.168.1.22:1601/reportserver/reportserver/httpauthexport','report'),(2,'reportserver.apikey','79PKXGScP8r8','report'),(3,'reportserver.report.all.customer.id','83275','customer'),(4,'reportserver.report.ambilstok.pindah.gudang.id','16056','despatch'),(5,'reportserver.report.despatch.all.id','83961','despatch'),(6,'reportserver.report.despatch.all.status.id','84007','despatch'),(7,'reportserver.report.item.barang.pindah.gudang.dengan.mfg','1160','despatch'),(8,'reportserver.report.item.barang.pindah.gudang.tanpa.mfg','1164','despatch'),(9,'reportserver.report.longlistpaper.pindah.gudang.id','16172','despatch'),(10,'reportserver.report.product.ada.lokasi.id','83065','despatch'),(11,'reportserver.report.product.tidak.ada.lokasi.id','83105','despatch'),(12,'reportserver.report.received.pindah.gudang.id','84538','despatch'),(13,'reportserver.report.siapkanorder.pindah.gudang.id','162671','despatch'),(14,'reportserver.report.truckdriverorder.pindah.gudang.id','16216','despatch'),(15,'reportserver.report.ambilstok.id','14787','dispatch'),(16,'reportserver.report.dispatch.all.id','83843','dispatch'),(17,'reportserver.report.dispatch.all.status.id','83890','dispatch'),(18,'reportserver.report.dispatch.id','5177','dispatch'),(19,'reportserver.report.item.barang.terjual.dengan.mfg','1159','dispatch'),(20,'reportserver.report.item.barang.terjual.semua.item.pergudang','1169','dispatch'),(21,'reportserver.report.item.barang.terjual.tanpa.mfg','1158','dispatch'),(22,'reportserver.report.item.global.id','534769','dispatch'),(23,'reportserver.report.longlistpaper.id','15777','dispatch'),(24,'reportserver.report.penjualan.item.percustomer','1170','dispatch'),(25,'reportserver.report.planambilstok.id','83739','dispatch'),(26,'reportserver.report.rekapkeluargudang.id','83668','dispatch'),(27,'reportserver.report.siapkanorder.id','15686','dispatch'),(28,'reportserver.report.suratjalan.id','84265','dispatch'),(29,'reportserver.report.truckdriverorder.id','20446','dispatch'),(30,'reportserver.report.buku.stok.produk.id','1214','inventory'),(31,'reportserver.report.bukustock.id','69421','inventory'),(32,'reportserver.report.history.product.id','82251','inventory'),(33,'reportserver.report.product.outofstock.id','123211','inventory'),(34,'reportserver.report.rakap.hasil.produksi','1157','inventory'),(35,'reportserver.report.stok.produk.semua.gudang','82995','inventory'),(37,'reportserver.report.fakturpajak.id','82023','invoice'),(38,'reportserver.report.invoice.invoiceitem.id','15777','invoice'),(39,'reportserver.report.longlistpaper.invoice.id','81951','invoice'),(40,'reportserver.report.paymentrecap.id','84060','invoice'),(41,'reportserver.report.recap.penjualan.percustomer.id','83210','invoice'),(42,'reportserver.report.rekapitulasi.penjualan.percustomer','1154','invoice'),(43,'reportserver.report.rincian.faktur.penjualan.item.percustomer','1156','invoice'),(44,'reportserver.report.rincian.outstanding.percustomer','1152','invoice'),(45,'reportserver.report.rincian.produk.per.item.id','1216','invoice'),(47,'reportserver.report.all.pricebook.id','82082','pricebook'),(48,'reportserver.report.pricebook.id','82159','pricebook'),(49,'reportserver.report.all.product.id','83582','product'),(50,'reportserver.report.item.global.reporting.id','1224','inventory'),(51,'reportserver.report.recap.pindah.gudang.id','1226','despatch'),(52,'reportserver.report.penjualan.item.perhari.id','1228','invoice'),(53,'reportserver.report.penjualan.item.perbulan.id','1229','invoice'),(54,'reportserver.report.omset.penjualan.customer.perhari.id','1230','invoice'),(55,'reportserver.report.omset.penjualan.customer.perbulan.id','1231','invoice'),(56,'reportserver.report.item.terjual.peritem.percustomer.id','1232','invoice'),(57,'reportserver.report.rekapitulasi.pembayaran.id','1233','invoice'),(58,'reportserver.report.pembayaran.percustomer.id','1234','invoice'),(59,'reportserver.report.outstanding.customer.id','1235','invoice'),(60,'reportserver.report.rekap.hasil.produksi.id','1249','invoice'),(61,'reportserver.report.product.report.by.search.location','1265','despatch'),(62,'reportserver.report.item.terjual.peritem.semua.customer','1280','invoice'),(63,'reportserver.report.rekapmasukgudang.id','1295','despatch'),(64,'reportserver.report.outstanding.customer','1326','invoice'),(65,'reportserver.report.rencana.pengiriman.product.id','1330','dispatch'),(66,'reportserver.report.rincian.pembayaran.perfaktur','1479','invoice'),(67,'reportserver.report.faktur.pajak.djp.id','1507','invoice'),(68,'reportserver.report.purchase.order.detail','150533','PurchaseOrder'),(69,'reportserver.report.tukar.faktur.raw.material.id','1577','PurchaseInvoiceExchange');
/*!40000 ALTER TABLE `tbreport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbreturn`
--

DROP TABLE IF EXISTS `tbreturn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbreturn` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `customer` bigint NOT NULL,
  `invoice` bigint NOT NULL,
  `total` decimal(19,2) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-returninvoice` (`invoice`),
  KEY `fk-customerforreturn` (`customer`),
  CONSTRAINT `fk-customerforreturn` FOREIGN KEY (`customer`) REFERENCES `tbcustomer` (`id`),
  CONSTRAINT `fk-returninvoice` FOREIGN KEY (`invoice`) REFERENCES `tbinvoice` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbreturn`
--

LOCK TABLES `tbreturn` WRITE;
/*!40000 ALTER TABLE `tbreturn` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbreturn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbreturnitem`
--

DROP TABLE IF EXISTS `tbreturnitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbreturnitem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `retur` bigint NOT NULL,
  `invoiceitem` bigint NOT NULL,
  `product` bigint NOT NULL,
  `quantity` decimal(19,2) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-returnitemforitem` (`invoiceitem`),
  KEY `fk-returnitemforreturn` (`retur`),
  KEY `fk-returnitemforproduct` (`product`),
  CONSTRAINT `fk-returnitemforitem` FOREIGN KEY (`invoiceitem`) REFERENCES `tbinvoiceitem` (`id`),
  CONSTRAINT `fk-returnitemforproduct` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`),
  CONSTRAINT `fk-returnitemforreturn` FOREIGN KEY (`retur`) REFERENCES `tbreturn` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbreturnitem`
--

LOCK TABLES `tbreturnitem` WRITE;
/*!40000 ALTER TABLE `tbreturnitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbreturnitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbrole`
--

DROP TABLE IF EXISTS `tbrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbrole` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbrole`
--

LOCK TABLES `tbrole` WRITE;
/*!40000 ALTER TABLE `tbrole` DISABLE KEYS */;
INSERT INTO `tbrole` VALUES (1,'SUPERADMIN');
/*!40000 ALTER TABLE `tbrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbrolepermission`
--

DROP TABLE IF EXISTS `tbrolepermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbrolepermission` (
  `role_id` bigint DEFAULT NULL,
  `action` varchar(32) DEFAULT NULL,
  `business_object` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbrolepermission`
--

LOCK TABLES `tbrolepermission` WRITE;
/*!40000 ALTER TABLE `tbrolepermission` DISABLE KEYS */;
INSERT INTO `tbrolepermission` VALUES (1,'CREATE','Customer'),(1,'CREATE','Customer Type'),(1,'CREATE','DeliveryArea'),(1,'CREATE','Dispatchment'),(1,'CREATE','Driver'),(1,'CREATE','Facility'),(1,'CREATE','File'),(1,'CREATE','Inventory'),(1,'CREATE','Invoice'),(1,'CREATE','Location'),(1,'CREATE','Navigation'),(1,'CREATE','Organisation'),(1,'CREATE','Payment'),(1,'CREATE','Payment Type'),(1,'CREATE','PriceBook'),(1,'CREATE','Product'),(1,'CREATE','ProductCategory'),(1,'CREATE','Role'),(1,'CREATE','Salesperson'),(1,'CREATE','Terms of Payment'),(1,'CREATE','Truck'),(1,'CREATE','Unit of Measurement'),(1,'CREATE','User'),(1,'DELETE','Customer'),(1,'DELETE','Dispatchment'),(1,'DELETE','File'),(1,'DELETE','Inventory'),(1,'DELETE','Payment'),(1,'DELETE','PriceBook'),(1,'DELETE','Role'),(1,'IMPORT','Customer'),(1,'IMPORT','Product'),(1,'OVERRIDE','Invoice'),(1,'READ','Customer'),(1,'READ','Customer Type'),(1,'READ','DeliveryArea'),(1,'READ','Dispatchment'),(1,'READ','Driver'),(1,'READ','Facility'),(1,'READ','File'),(1,'READ','Inventory'),(1,'READ','Invoice'),(1,'READ','Location'),(1,'READ','Navigation'),(1,'READ','Organisation'),(1,'READ','Payment Type'),(1,'READ','PriceBook'),(1,'READ','Product'),(1,'READ','ProductCategory'),(1,'READ','Report'),(1,'READ','Role'),(1,'READ','Salesperson'),(1,'READ','System Properties'),(1,'READ','Terms of Payment'),(1,'READ','Truck'),(1,'READ','Unit of Measurement'),(1,'READ','User'),(1,'RESTORE','Customer'),(1,'UPDATE','Customer'),(1,'UPDATE','Customer Type'),(1,'UPDATE','DeliveryArea'),(1,'UPDATE','Dispatchment'),(1,'UPDATE','Driver'),(1,'UPDATE','Facility'),(1,'UPDATE','File'),(1,'UPDATE','Inventory'),(1,'UPDATE','Invoice'),(1,'UPDATE','Location'),(1,'UPDATE','Organisation'),(1,'UPDATE','Payment Type'),(1,'UPDATE','PriceBook'),(1,'UPDATE','Product'),(1,'UPDATE','ProductCategory'),(1,'UPDATE','Role'),(1,'UPDATE','Salesperson'),(1,'UPDATE','System Properties'),(1,'UPDATE','Terms of Payment'),(1,'UPDATE','Truck'),(1,'UPDATE','Unit of Measurement'),(1,'UPDATE','User'),(1,'CREATE','Return'),(1,'READ','Return'),(1,'UPDATE','Return'),(1,'DELETE','Return'),(1,'READ','Dashboard'),(1,'CREATE','Dashboard'),(1,'UPDATE','Dashboard'),(1,'READ','Home'),(1,'CREATE','Refund'),(1,'READ','Refund'),(1,'UPDATE','Refund'),(1,'DELETE','Refund'),(1,'CREATE','ShipmentType'),(1,'READ','ShipmentType'),(1,'UPDATE','ShipmentType'),(1,'DELETE','ShipmentType'),(1,'APPROVE','Invoice'),(1,'EXPORT','PriceBook'),(1,'IMPORT','PriceBook'),(1,'READ','Payment'),(1,'CREATE','ProductAvailability'),(1,'READ','ProductAvailability'),(1,'UPDATE','ProductAvailability'),(1,'DELETE','ProductAvailability'),(1,'CREATE','MeatType'),(1,'READ','MeatType'),(1,'UPDATE','MeatType'),(1,'DELETE','MeatType'),(1,'CREATE','PackagingType'),(1,'READ','PackagingType'),(1,'UPDATE','PackagingType'),(1,'DELETE','PackagingType'),(1,'CREATE','CasingType'),(1,'READ','CasingType'),(1,'UPDATE','CasingType'),(1,'DELETE','CasingType'),(1,'CREATE','ProductGroup'),(1,'READ','ProductGroup'),(1,'UPDATE','ProductGroup'),(1,'DELETE','ProductGroup'),(1,'CREATE','ProductWeight'),(1,'READ','ProductWeight'),(1,'UPDATE','ProductWeight'),(1,'DELETE','ProductWeight'),(1,'CREATE','DeliveryColorCode'),(1,'READ','DeliveryColorCode'),(1,'UPDATE','DeliveryColorCode'),(1,'DELETE','DeliveryColorCode'),(1,'CREATE','ProductCasing'),(1,'READ','ProductCasing'),(1,'UPDATE','ProductCasing'),(1,'DELETE','ProductCasing'),(1,'CREATE','KilogramWeight'),(1,'READ','KilogramWeight'),(1,'UPDATE','KilogramWeight'),(1,'DELETE','KilogramWeight'),(1,'CREATE','DeliveryMethod'),(1,'READ','DeliveryMethod'),(1,'UPDATE','DeliveryMethod'),(1,'DELETE','DeliveryMethod'),(1,'CREATE','Settlement'),(1,'READ','Settlement'),(1,'DELETE','Settlement'),(1,'CASH','Settlement'),(1,'GIRO','Settlement'),(1,'TRANSFER','Settlement'),(1,'CREDIT','Settlement'),(1,'ADJUSTMENT','Settlement'),(1,'CREATE','DeliveryCity'),(1,'READ','DeliveryCity'),(1,'UPDATE','DeliveryCity'),(1,'DELETE','DeliveryCity'),(1,'CREATE','Despatchment'),(1,'READ','Despatchment'),(1,'UPDATE','Despatchment'),(1,'DELETE','Despatchment'),(1,'CREATE','Repackage'),(1,'READ','Repackage'),(1,'UPDATE','Repackage'),(1,'UPDATE','InvoiceItemWeight'),(1,'UPDATE','InvoiceItemQty'),(1,'CREATE','ReceiptReceivedDate'),(1,'UPDATE','ReceiptReceivedDate'),(1,'UPDATE','AssignTruck'),(1,'UPDATE','InvoiceItemUnitPrice'),(1,'CREATE','MasterAdjustment'),(1,'READ','MasterAdjustment'),(1,'UPDATE','MasterAdjustment'),(1,'DELETE','MasterAdjustment'),(1,'UPDATE','Payment'),(1,'UPDATE','Report'),(1,'READ','GlobalSales'),(1,'UPDATE','GlobalSales'),(1,'DELETE','GlobalSales'),(1,'CREATE','Currency'),(1,'READ','Currency'),(1,'UPDATE','Currency'),(1,'DELETE','Currency'),(1,'CREATE','RawMaterialsCategories'),(1,'READ','RawMaterialsCategories'),(1,'UPDATE','RawMaterialsCategories'),(1,'DELETE','RawMaterialsCategories'),(1,'CREATE','Storehouse'),(1,'READ','Storehouse'),(1,'UPDATE','Storehouse'),(1,'DELETE','Storehouse'),(1,'CREATE','RawMaterials'),(1,'READ','RawMaterials'),(1,'UPDATE','RawMaterials'),(1,'DELETE','RawMaterials'),(1,'CREATE','Supplier'),(1,'READ','Supplier'),(1,'UPDATE','Supplier'),(1,'DELETE','Supplier'),(1,'CREATE','SupplierCategories'),(1,'READ','SupplierCategories'),(1,'UPDATE','SupplierCategories'),(1,'DELETE','SupplierCategories'),(1,'CREATE','SupplierCatalog'),(1,'READ','SupplierCatalog'),(1,'UPDATE','SupplierCatalog'),(1,'DELETE','SupplierCatalog'),(1,'CREATE','PurchaseRequest'),(1,'READ','PurchaseRequest'),(1,'UPDATE','PurchaseRequest'),(1,'DELETE','PurchaseRequest'),(1,'CREATE','PurchaseOrder'),(1,'READ','PurchaseOrder'),(1,'UPDATE','PurchaseOrder'),(1,'DELETE','PurchaseOrder'),(1,'CREATE','PurchaseInvoice'),(1,'READ','PurchaseInvoice'),(1,'UPDATE','PurchaseInvoice'),(1,'DELETE','PurchaseInvoice'),(1,'CREATE','PurchaseInvoicePayment'),(1,'READ','PurchaseInvoicePayment'),(1,'UPDATE','PurchaseInvoicePayment'),(1,'DELETE','PurchaseInvoicePayment'),(1,'CREATE','QC'),(1,'READ','QC'),(1,'UPDATE','QC'),(1,'DELETE','QC'),(1,'CREATE','MaterialRequest'),(1,'READ','MaterialRequest'),(1,'UPDATE','MaterialRequest'),(1,'DELETE','MaterialRequest'),(1,'CREATE','Stock'),(1,'READ','Stock'),(1,'UPDATE','Stock'),(1,'DELETE','Stock'),(1,'CREATE','StagingArea'),(1,'READ','StagingArea'),(1,'UPDATE','StagingArea'),(1,'DELETE','StagingArea'),(1,'CREATE','MaterialRelease'),(1,'READ','MaterialRelease'),(1,'UPDATE','MaterialRelease'),(1,'DELETE','MaterialRelease'),(1,'CREATE','ShippingTerms'),(1,'READ','ShippingTerms'),(1,'UPDATE','ShippingTerms'),(1,'DELETE','ShippingTerms');
/*!40000 ALTER TABLE `tbrolepermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsalepricebook`
--

DROP TABLE IF EXISTS `tbsalepricebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsalepricebook` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `pricebook_reference` bigint DEFAULT NULL,
  `category` varchar(250) DEFAULT NULL,
  `tax_inclusive` bit(1) DEFAULT NULL,
  `type` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`),
  KEY `fk-spbreference` (`pricebook_reference`),
  CONSTRAINT `fk-spbreference` FOREIGN KEY (`pricebook_reference`) REFERENCES `tbsalepricebook` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsalepricebook`
--

LOCK TABLES `tbsalepricebook` WRITE;
/*!40000 ALTER TABLE `tbsalepricebook` DISABLE KEYS */;
INSERT INTO `tbsalepricebook` VALUES (1,NULL,'DEFAULT',_binary '\0',0);
/*!40000 ALTER TABLE `tbsalepricebook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsalepricebookprice`
--

DROP TABLE IF EXISTS `tbsalepricebookprice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsalepricebookprice` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `product` bigint DEFAULT NULL,
  `salepricebook` bigint DEFAULT NULL,
  `alternative_name` varchar(128) DEFAULT NULL,
  `sale_price` decimal(19,4) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `selectable_during_invoicing` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-productrelated` (`product`),
  KEY `fk-salepricebookrelated` (`salepricebook`),
  CONSTRAINT `fk-productrelated` FOREIGN KEY (`product`) REFERENCES `tbproduct` (`id`),
  CONSTRAINT `fk-salepricebookrelated` FOREIGN KEY (`salepricebook`) REFERENCES `tbsalepricebook` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsalepricebookprice`
--

LOCK TABLES `tbsalepricebookprice` WRITE;
/*!40000 ALTER TABLE `tbsalepricebookprice` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbsalepricebookprice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsalesperson`
--

DROP TABLE IF EXISTS `tbsalesperson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsalesperson` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `full_name` varchar(128) DEFAULT NULL,
  `short_name` varchar(128) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `join_date` datetime DEFAULT NULL,
  `work_area` varchar(128) DEFAULT NULL,
  `phone_number` varchar(64) DEFAULT NULL,
  `position` varchar(32) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsalesperson`
--

LOCK TABLES `tbsalesperson` WRITE;
/*!40000 ALTER TABLE `tbsalesperson` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbsalesperson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsettlement`
--

DROP TABLE IF EXISTS `tbsettlement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsettlement` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `receive_date` date DEFAULT NULL,
  `giro_date` date DEFAULT NULL,
  `payment_type_id` bigint DEFAULT '1',
  `amount` decimal(19,4) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `adjustment_description` varchar(255) DEFAULT NULL,
  `adjustment_amount` decimal(19,4) DEFAULT NULL,
  `remark` varchar(300) DEFAULT '',
  `status` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsettlement`
--

LOCK TABLES `tbsettlement` WRITE;
/*!40000 ALTER TABLE `tbsettlement` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbsettlement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsettlementadjustment`
--

DROP TABLE IF EXISTS `tbsettlementadjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsettlementadjustment` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `settlement_id` bigint NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-settlement` (`settlement_id`),
  CONSTRAINT `fk-settlement` FOREIGN KEY (`settlement_id`) REFERENCES `tbsettlement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsettlementadjustment`
--

LOCK TABLES `tbsettlementadjustment` WRITE;
/*!40000 ALTER TABLE `tbsettlementadjustment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbsettlementadjustment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsettlementoverpayment`
--

DROP TABLE IF EXISTS `tbsettlementoverpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsettlementoverpayment` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `settlement_id` bigint NOT NULL,
  `used_by_settlement_id` bigint DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-settlementoverused` (`used_by_settlement_id`),
  KEY `fk-settlementover` (`settlement_id`),
  CONSTRAINT `fk-settlementover` FOREIGN KEY (`settlement_id`) REFERENCES `tbsettlement` (`id`),
  CONSTRAINT `fk-settlementoverused` FOREIGN KEY (`used_by_settlement_id`) REFERENCES `tbsettlement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsettlementoverpayment`
--

LOCK TABLES `tbsettlementoverpayment` WRITE;
/*!40000 ALTER TABLE `tbsettlementoverpayment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbsettlementoverpayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbshipmenttype`
--

DROP TABLE IF EXISTS `tbshipmenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbshipmenttype` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbshipmenttype`
--

LOCK TABLES `tbshipmenttype` WRITE;
/*!40000 ALTER TABLE `tbshipmenttype` DISABLE KEYS */;
INSERT INTO `tbshipmenttype` VALUES (1,'Pickup',_binary ''),(2,'Delivery',_binary '');
/*!40000 ALTER TABLE `tbshipmenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbshippingterms`
--

DROP TABLE IF EXISTS `tbshippingterms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbshippingterms` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbshippingterms`
--

LOCK TABLES `tbshippingterms` WRITE;
/*!40000 ALTER TABLE `tbshippingterms` DISABLE KEYS */;
INSERT INTO `tbshippingterms` VALUES (1,'SUPPLIER DAGING',_binary '','admin','2024-12-05 03:55:00','admin','2024-12-12 04:23:36'),(2,'SUPPLIER DAGING 2',_binary '','admin','2024-12-05 03:55:20','admin','2024-12-12 06:56:35'),(3,'DGGGGG',_binary '','admin','2024-12-05 03:55:31','admin','2024-12-12 04:24:24');
/*!40000 ALTER TABLE `tbshippingterms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbshippingtermsremarks`
--

DROP TABLE IF EXISTS `tbshippingtermsremarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbshippingtermsremarks` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `shipping_terms` bigint NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-stForStr` (`shipping_terms`),
  CONSTRAINT `fk-stForStr` FOREIGN KEY (`shipping_terms`) REFERENCES `tbshippingterms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbshippingtermsremarks`
--

LOCK TABLES `tbshippingtermsremarks` WRITE;
/*!40000 ALTER TABLE `tbshippingtermsremarks` DISABLE KEYS */;
INSERT INTO `tbshippingtermsremarks` VALUES (1,1,'02 FLUID COMPONENTS INTL FURNISHED MATERIAL','admin','2024-12-05 03:55:00','admin','2024-12-12 04:23:36'),(2,1,'Woi 2','admin','2024-12-05 03:55:00','admin','2024-12-12 04:23:36'),(3,1,'03 SHIPMENT OF DISCREPANT MATERIALS','admin','2024-12-05 03:55:00','admin','2024-12-12 04:23:36'),(4,2,'Setiap pengiriman wajib disertakan COA','admin','2024-12-05 03:55:20','admin','2024-12-12 06:56:35'),(5,2,'Pengirim sebelun jam 15.00 WIB & Harus Menggunakan Mobil Pendiingin Kondisi Bersih, Temperatur mobil dan produk minimal -12*C','admin','2024-12-05 03:55:20','admin','2024-12-12 06:56:35'),(6,2,'Produk yang di kirimkan harus dalm kondisi bagus dan produk tidak boleh bersentuhan labgsung dengan lantai mobil','admin','2024-12-05 03:55:20','admin','2024-12-12 06:56:35'),(7,2,'pengiriman harus berapakian rapih, bersih dan memakai sepatu','admin','2024-12-05 03:55:20','admin','2024-12-12 06:56:35'),(8,3,'04 AGE CONTROLLED ITEM','admin','2024-12-05 03:55:31','admin','2024-12-12 04:24:24');
/*!40000 ALTER TABLE `tbshippingtermsremarks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstagingfacility`
--

DROP TABLE IF EXISTS `tbstagingfacility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbstagingfacility` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstagingfacility`
--

LOCK TABLES `tbstagingfacility` WRITE;
/*!40000 ALTER TABLE `tbstagingfacility` DISABLE KEYS */;
INSERT INTO `tbstagingfacility` VALUES (1,'Staging Facility','Pre-Production Facility');
/*!40000 ALTER TABLE `tbstagingfacility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstaginglocation`
--

DROP TABLE IF EXISTS `tbstaginglocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbstaginglocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `facility` bigint NOT NULL,
  `area` varchar(50) NOT NULL,
  `aisles` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-stagingLocationFacility` (`facility`),
  CONSTRAINT `fk-stagingLocationFacility` FOREIGN KEY (`facility`) REFERENCES `tbstagingfacility` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstaginglocation`
--

LOCK TABLES `tbstaginglocation` WRITE;
/*!40000 ALTER TABLE `tbstaginglocation` DISABLE KEYS */;
INSERT INTO `tbstaginglocation` VALUES (1,1,'Area','Aisles','Section','Level','Position');
/*!40000 ALTER TABLE `tbstaginglocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstockallocation`
--

DROP TABLE IF EXISTS `tbstockallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbstockallocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `stock_block` bigint NOT NULL,
  `raw_material` bigint NOT NULL,
  `location` bigint DEFAULT NULL,
  `staging_location` bigint DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-rmForStockAllocation` (`raw_material`),
  KEY `fk-sbForSa` (`stock_block`),
  KEY `fk-locationForSa` (`location`),
  KEY `fk-stagingLocationForSa` (`staging_location`),
  CONSTRAINT `fk-locationForSa` FOREIGN KEY (`location`) REFERENCES `tbstorehouselocation` (`id`),
  CONSTRAINT `fk-rmForStockAllocation` FOREIGN KEY (`raw_material`) REFERENCES `tbrawmaterials` (`id`),
  CONSTRAINT `fk-sbForSa` FOREIGN KEY (`stock_block`) REFERENCES `tbstockblock` (`id`),
  CONSTRAINT `fk-stagingLocationForSa` FOREIGN KEY (`staging_location`) REFERENCES `tbstaginglocation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstockallocation`
--

LOCK TABLES `tbstockallocation` WRITE;
/*!40000 ALTER TABLE `tbstockallocation` DISABLE KEYS */;
INSERT INTO `tbstockallocation` VALUES (1,1,2,5,NULL,4,0,'admin','2024-11-15 06:26:31','admin','2024-12-04 03:12:25'),(2,2,4,6,NULL,5,0,'admin','2024-11-15 06:26:31','admin','2024-12-04 03:12:25'),(3,1,2,NULL,1,1,1,'admin','2024-11-15 06:36:12','admin','2024-11-15 06:36:12'),(4,3,1,4,NULL,10,0,'admin','2024-11-15 07:14:26',NULL,NULL),(5,4,1,3,NULL,10,0,'admin','2024-11-21 09:05:22',NULL,NULL),(6,1,2,NULL,NULL,2,3,'admin','2024-12-04 03:09:27','admin','2024-12-04 03:10:36'),(7,2,4,NULL,NULL,1,3,'admin','2024-12-04 03:09:27','admin','2025-01-06 07:05:59'),(8,1,2,NULL,NULL,3,3,'admin','2024-12-04 03:12:25','admin','2025-01-06 07:05:59'),(9,2,4,NULL,NULL,3,3,'admin','2024-12-04 03:12:25','admin','2025-01-06 07:05:59'),(10,2,4,5,NULL,1,0,'admin','2024-12-04 03:12:49','admin','2024-12-04 03:12:49'),(11,5,20,5,NULL,50,0,'admin','2025-01-06 07:01:19','admin','2025-01-06 07:04:29'),(12,6,20,5,NULL,100,0,'admin','2025-01-06 07:01:19',NULL,NULL),(13,7,2,5,NULL,50,0,'admin','2025-01-06 07:01:19','admin','2025-01-06 07:04:29'),(14,8,4,5,NULL,100,0,'admin','2025-01-06 07:01:19',NULL,NULL),(15,9,4,5,NULL,50,0,'admin','2025-01-06 07:01:19','admin','2025-01-06 07:04:29'),(16,10,13,6,NULL,100,0,'admin','2025-01-06 07:01:19',NULL,NULL),(17,5,20,NULL,NULL,50,3,'admin','2025-01-06 07:04:29','admin','2025-01-06 07:05:59'),(18,7,2,NULL,NULL,50,3,'admin','2025-01-06 07:04:29','admin','2025-01-06 07:05:59'),(19,9,4,NULL,NULL,50,3,'admin','2025-01-06 07:04:29','admin','2025-01-06 07:05:59');
/*!40000 ALTER TABLE `tbstockallocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstockblock`
--

DROP TABLE IF EXISTS `tbstockblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbstockblock` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `quality_control_item_inspection` bigint DEFAULT NULL,
  `raw_material` bigint NOT NULL,
  `production_code` varchar(32) DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `conversion` decimal(19,4) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-qciiForStockBlock` (`quality_control_item_inspection`),
  KEY `fk-rmForStockBlock` (`raw_material`),
  CONSTRAINT `fk-qciiForStockBlock` FOREIGN KEY (`quality_control_item_inspection`) REFERENCES `tbqualitycontroliteminspection` (`id`),
  CONSTRAINT `fk-rmForStockBlock` FOREIGN KEY (`raw_material`) REFERENCES `tbrawmaterials` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstockblock`
--

LOCK TABLES `tbstockblock` WRITE;
/*!40000 ALTER TABLE `tbstockblock` DISABLE KEYS */;
INSERT INTO `tbstockblock` VALUES (1,1,2,'15112024','2025-11-15',10,5.0000,'admin','2024-11-15 06:26:31','admin','2025-01-06 07:05:50'),(2,3,4,'15112024','2025-11-15',10,5.0000,'admin','2024-11-15 06:26:31','admin','2025-01-06 07:05:50'),(3,4,1,'15112024',NULL,10,5.0000,'admin','2024-11-15 07:14:26',NULL,NULL),(4,5,1,'21112024',NULL,10,5.0000,'admin','2024-11-21 09:05:22',NULL,NULL),(5,NULL,20,'06012025','2025-01-17',100,1.0000,'admin','2025-01-06 07:01:19',NULL,NULL),(6,NULL,20,'08012025','2025-01-17',100,1.0000,'admin','2025-01-06 07:01:19',NULL,NULL),(7,NULL,2,'14012025','2025-01-17',100,5.0000,'admin','2025-01-06 07:01:19',NULL,NULL),(8,NULL,4,'28012025','2025-01-24',100,5.0000,'admin','2025-01-06 07:01:19',NULL,NULL),(9,NULL,4,'22012025','2025-01-24',100,5.0000,'admin','2025-01-06 07:01:19',NULL,NULL),(10,NULL,13,'08012025','2025-01-09',100,1.0000,'admin','2025-01-06 07:01:19',NULL,NULL);
/*!40000 ALTER TABLE `tbstockblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstockmovement`
--

DROP TABLE IF EXISTS `tbstockmovement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbstockmovement` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `stock_allocation` bigint NOT NULL,
  `from_location` bigint DEFAULT NULL,
  `to_location` bigint DEFAULT NULL,
  `to_staging_location` bigint DEFAULT NULL,
  `material_release_item` bigint DEFAULT NULL,
  `type` int DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-toStagingLocationForSm` (`to_staging_location`),
  KEY `fk-stockAllocationForSm` (`stock_allocation`),
  KEY `fk-fromLocationForSm` (`from_location`),
  KEY `fk-toLocationForSm` (`to_location`),
  KEY `fk-mriForSm` (`material_release_item`),
  CONSTRAINT `fk-fromLocationForSm` FOREIGN KEY (`from_location`) REFERENCES `tbstorehouselocation` (`id`),
  CONSTRAINT `fk-mriForSm` FOREIGN KEY (`material_release_item`) REFERENCES `tbmaterialreleaseitem` (`id`),
  CONSTRAINT `fk-stockAllocationForSm` FOREIGN KEY (`stock_allocation`) REFERENCES `tbstockallocation` (`id`),
  CONSTRAINT `fk-toLocationForSm` FOREIGN KEY (`to_location`) REFERENCES `tbstorehouselocation` (`id`),
  CONSTRAINT `fk-toStagingLocationForSm` FOREIGN KEY (`to_staging_location`) REFERENCES `tbstaginglocation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstockmovement`
--

LOCK TABLES `tbstockmovement` WRITE;
/*!40000 ALTER TABLE `tbstockmovement` DISABLE KEYS */;
INSERT INTO `tbstockmovement` VALUES (1,1,NULL,5,NULL,NULL,0,'New Stock','admin','2024-11-15 06:26:31',NULL,'2024-11-15 06:26:31'),(2,2,NULL,6,NULL,NULL,0,'New Stock','admin','2024-11-15 06:26:31',NULL,'2024-11-15 06:26:31'),(3,3,5,5,NULL,NULL,2,'Split to facilitate the creation of a raw material release, associated with MrDocSequence \'MR-Doc-0000001\' and MrBatchSequence \'MR-Batch-0000001\'.','admin','2024-11-15 06:36:12',NULL,'2024-11-15 06:36:12'),(4,3,5,NULL,1,1,4,'Release associated with MrDocSequence \'MR-Doc-0000001\' and MrBatchSequence \'MR-Batch-0000001\'.','admin','2024-11-15 06:36:12',NULL,'2024-11-15 06:36:12'),(5,4,NULL,4,NULL,NULL,0,'New Stock','admin','2024-11-15 07:14:26',NULL,'2024-11-15 07:14:26'),(6,5,NULL,3,NULL,NULL,0,'New Stock','admin','2024-11-21 09:05:22',NULL,'2024-11-21 09:05:22'),(7,6,5,5,NULL,NULL,2,'Split to facilitate the quarantine of the allocation, Split the quantity of 9 into two separate allocations of 7 and 2.','admin','2024-12-04 03:09:27',NULL,'2024-12-04 03:09:27'),(8,6,5,7,NULL,NULL,5,'Quarantined from GUDANG DAGING - 1-1-1-1-1 to GUDANG DAGING - -11-1-1-','admin','2024-12-04 03:09:27',NULL,'2024-12-04 03:09:27'),(9,7,6,6,NULL,NULL,2,'Split to facilitate the quarantine of the allocation, Split the quantity of 10 into two separate allocations of 8 and 2.','admin','2024-12-04 03:09:27',NULL,'2024-12-04 03:09:27'),(10,7,6,7,NULL,NULL,5,'Quarantined from GUDANG DAGING - 2-2-2-2-2 to GUDANG DAGING - -11-1-1-','admin','2024-12-04 03:09:27',NULL,'2024-12-04 03:09:27'),(11,6,7,NULL,NULL,NULL,7,'HILANG','admin','2024-12-04 03:10:36',NULL,'2024-12-04 03:10:36'),(12,7,7,6,NULL,NULL,6,'Dequarantined from GUDANG DAGING - -11-1-1- to GUDANG DAGING - 2-2-2-2-2','admin','2024-12-04 03:10:58',NULL,'2024-12-04 03:10:58'),(13,8,5,5,NULL,NULL,2,'Split to facilitate the quarantine of the allocation, Split the quantity of 7 into two separate allocations of 4 and 3.','admin','2024-12-04 03:12:25',NULL,'2024-12-04 03:12:25'),(14,8,5,7,NULL,NULL,5,'Quarantined from GUDANG DAGING - 1-1-1-1-1 to GUDANG DAGING - -11-1-1-','admin','2024-12-04 03:12:25',NULL,'2024-12-04 03:12:25'),(15,9,6,6,NULL,NULL,2,'Split to facilitate the quarantine of the allocation, Split the quantity of 8 into two separate allocations of 5 and 3.','admin','2024-12-04 03:12:25',NULL,'2024-12-04 03:12:25'),(16,9,6,7,NULL,NULL,5,'Quarantined from GUDANG DAGING - 2-2-2-2-2 to GUDANG DAGING - -11-1-1-','admin','2024-12-04 03:12:25',NULL,'2024-12-04 03:12:25'),(17,7,6,7,NULL,NULL,5,'Quarantined from GUDANG DAGING - 2-2-2-2-2 to GUDANG DAGING - -11-1-1-','admin','2024-12-04 03:12:25',NULL,'2024-12-04 03:12:25'),(18,10,7,7,NULL,NULL,2,'Split to facilitate the storing of the quarantined allocation, Split the quantity of 2 into two separate allocations of 1 and 1.','admin','2024-12-04 03:12:49',NULL,'2024-12-04 03:12:49'),(19,10,7,5,NULL,NULL,6,'Dequarantined from GUDANG DAGING - -11-1-1- to GUDANG DAGING - 1-1-1-1-1','admin','2024-12-04 03:12:49',NULL,'2024-12-04 03:12:49'),(20,11,NULL,5,NULL,NULL,0,'New Stock','admin','2025-01-06 07:01:19',NULL,'2025-01-06 07:01:19'),(21,12,NULL,5,NULL,NULL,0,'New Stock','admin','2025-01-06 07:01:19',NULL,'2025-01-06 07:01:19'),(22,13,NULL,5,NULL,NULL,0,'New Stock','admin','2025-01-06 07:01:19',NULL,'2025-01-06 07:01:19'),(23,14,NULL,5,NULL,NULL,0,'New Stock','admin','2025-01-06 07:01:19',NULL,'2025-01-06 07:01:19'),(24,15,NULL,5,NULL,NULL,0,'New Stock','admin','2025-01-06 07:01:19',NULL,'2025-01-06 07:01:19'),(25,16,NULL,6,NULL,NULL,0,'New Stock','admin','2025-01-06 07:01:19',NULL,'2025-01-06 07:01:19'),(26,17,5,5,NULL,NULL,2,'Split to facilitate the quarantine of the allocation, Split the quantity of 100 into two separate allocations of 50 and 50.','admin','2025-01-06 07:04:29',NULL,'2025-01-06 07:04:29'),(27,17,5,7,NULL,NULL,5,'Quarantined from GUDANG DAGING - 1-1-1-1-1 to GUDANG DAGING - -11-1-1-','admin','2025-01-06 07:04:29',NULL,'2025-01-06 07:04:29'),(28,18,5,5,NULL,NULL,2,'Split to facilitate the quarantine of the allocation, Split the quantity of 100 into two separate allocations of 50 and 50.','admin','2025-01-06 07:04:29',NULL,'2025-01-06 07:04:29'),(29,18,5,7,NULL,NULL,5,'Quarantined from GUDANG DAGING - 1-1-1-1-1 to GUDANG DAGING - -11-1-1-','admin','2025-01-06 07:04:29',NULL,'2025-01-06 07:04:29'),(30,19,5,5,NULL,NULL,2,'Split to facilitate the quarantine of the allocation, Split the quantity of 100 into two separate allocations of 50 and 50.','admin','2025-01-06 07:04:29',NULL,'2025-01-06 07:04:29'),(31,19,5,7,NULL,NULL,5,'Quarantined from GUDANG DAGING - 1-1-1-1-1 to GUDANG DAGING - -11-1-1-','admin','2025-01-06 07:04:29',NULL,'2025-01-06 07:04:29'),(32,7,7,NULL,NULL,NULL,7,'dsfsf','admin','2025-01-06 07:05:59',NULL,'2025-01-06 07:05:59'),(33,8,7,NULL,NULL,NULL,7,'fdsf','admin','2025-01-06 07:05:59',NULL,'2025-01-06 07:05:59'),(34,9,7,NULL,NULL,NULL,7,'sdfsd','admin','2025-01-06 07:05:59',NULL,'2025-01-06 07:05:59'),(35,17,7,NULL,NULL,NULL,7,'sdfsdf','admin','2025-01-06 07:05:59',NULL,'2025-01-06 07:05:59'),(36,18,7,NULL,NULL,NULL,7,'sdfsdf','admin','2025-01-06 07:05:59',NULL,'2025-01-06 07:05:59'),(37,19,7,NULL,NULL,NULL,7,'sdfsdf','admin','2025-01-06 07:05:59',NULL,'2025-01-06 07:05:59');
/*!40000 ALTER TABLE `tbstockmovement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstorehousefacility`
--

DROP TABLE IF EXISTS `tbstorehousefacility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbstorehousefacility` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstorehousefacility`
--

LOCK TABLES `tbstorehousefacility` WRITE;
/*!40000 ALTER TABLE `tbstorehousefacility` DISABLE KEYS */;
INSERT INTO `tbstorehousefacility` VALUES (1,'GUDANG BUMBU KERING',''),(2,'GUDANG DAGING','');
/*!40000 ALTER TABLE `tbstorehousefacility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstorehousefacilitycategory`
--

DROP TABLE IF EXISTS `tbstorehousefacilitycategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbstorehousefacilitycategory` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `shfacilityid` bigint NOT NULL,
  `rmcategoryid` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-rmcategoryid` (`rmcategoryid`),
  KEY `fk-shfacilityid` (`shfacilityid`),
  CONSTRAINT `fk-rmcategoryid` FOREIGN KEY (`rmcategoryid`) REFERENCES `tbrawmaterialscategory` (`id`),
  CONSTRAINT `fk-shfacilityid` FOREIGN KEY (`shfacilityid`) REFERENCES `tbstorehousefacility` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstorehousefacilitycategory`
--

LOCK TABLES `tbstorehousefacilitycategory` WRITE;
/*!40000 ALTER TABLE `tbstorehousefacilitycategory` DISABLE KEYS */;
INSERT INTO `tbstorehousefacilitycategory` VALUES (1,1,2),(2,2,1);
/*!40000 ALTER TABLE `tbstorehousefacilitycategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstorehouselocation`
--

DROP TABLE IF EXISTS `tbstorehouselocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbstorehouselocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `facility` bigint DEFAULT NULL,
  `area` varchar(50) NOT NULL,
  `aisles` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `quarantine` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `fk-shlocationfacility` (`facility`),
  CONSTRAINT `fk-shlocationfacility` FOREIGN KEY (`facility`) REFERENCES `tbstorehousefacility` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstorehouselocation`
--

LOCK TABLES `tbstorehouselocation` WRITE;
/*!40000 ALTER TABLE `tbstorehouselocation` DISABLE KEYS */;
INSERT INTO `tbstorehouselocation` VALUES (1,1,'Receiving','R','R','R','R',NULL),(2,2,'Receiving','R','R','R','R',NULL),(3,1,'1','1','1','1','1',_binary '\0'),(4,1,'12','12','12','12','21',_binary '\0'),(5,2,'1','1','1','1','1',_binary '\0'),(6,2,'2','2','2','2','2',_binary '\0'),(7,2,'','11','1','1','',_binary '');
/*!40000 ALTER TABLE `tbstorehouselocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsupplier`
--

DROP TABLE IF EXISTS `tbsupplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsupplier` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `phone` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `bank_account` varchar(250) DEFAULT NULL,
  `bank_account_holder` varchar(128) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `note_po` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `office_address` varchar(250) DEFAULT NULL,
  `factory_address` varchar(250) DEFAULT NULL,
  `enable_tax` bit(1) DEFAULT NULL,
  `npwp` varchar(250) DEFAULT NULL,
  `npwp_name` varchar(250) DEFAULT NULL,
  `npwp_address` varchar(250) DEFAULT NULL,
  `email_order` varchar(250) DEFAULT NULL,
  `wa_order` varchar(250) DEFAULT NULL,
  `sales_name` varchar(250) DEFAULT NULL,
  `sales_phone` varchar(250) DEFAULT NULL,
  `finance_name` varchar(250) DEFAULT NULL,
  `finance_phone` varchar(250) DEFAULT NULL,
  `payment_term` bigint DEFAULT NULL,
  `payment_type` bigint DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `currency` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-supplierpaymenttype` (`payment_type`),
  KEY `fk-suppliertop` (`payment_term`),
  KEY `fk-currencyForSupplier` (`currency`),
  CONSTRAINT `fk-currencyForSupplier` FOREIGN KEY (`currency`) REFERENCES `tbcurrency` (`id`),
  CONSTRAINT `fk-supplierpaymenttype` FOREIGN KEY (`payment_type`) REFERENCES `tbpaymenttype` (`id`),
  CONSTRAINT `fk-suppliertop` FOREIGN KEY (`payment_term`) REFERENCES `tbtop` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsupplier`
--

LOCK TABLES `tbsupplier` WRITE;
/*!40000 ALTER TABLE `tbsupplier` DISABLE KEYS */;
INSERT INTO `tbsupplier` VALUES (1,'1178','SUPPLIER DAGING',_binary '','','','5445566777',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','JALAN CIPINANG MUARA',_binary '','12309090299212','ARIS RISMAWAN','Jln Abdul Halim','ari@gmail.com','09888777766','ASAWAWUH','','ASAWAWUH','088900999',5,3,'2024-12-06 06:16:03','admin',1),(2,'1179','SUPPLIER BUMBU',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299212','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(3,'1160','SUPPLIER DAGING DAN BUMBU',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299212','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(4,'1156','SUPPLIER JASA',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299212','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(5,'1111','SUPPLIER CASING',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299212','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(6,'1145','SUPPLIER KEMASAN',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299212','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(7,'1133','SUPPLIER PERLENGKAPAN',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299212','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(8,'1165','SUPPLIER SPAREPAT',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299212','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(9,'1123','SUPPLIER JASA',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299215','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(10,'1141','SUPPLIER JASA DAN SPAREPAT',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299212','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(11,'1146','SUPPLIER DAGING, BUMBU, CASING',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299214','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(12,'1147','SUPPLIER DAGING',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299211','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(13,'1148','SUPPLIER DAGING',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(14,'2212','SUPPLIER DAGING 20',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(15,'2232','SUPPLIER DAGING 67',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(16,'2210','SUPPLIER DAGING 43',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(17,'2590','SUPPLIER DAGING 95',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(18,'2878','SUPPLIER DAGING 20',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(19,'2980','SUPPLIER DAGING 32',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(20,'2647','SUPPLIER DAGING 50',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(21,'2461','SUPPLIER DAGING 100',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(22,'3212','SUPPLIER DAGING 0',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(23,'3430','SUPPLIER DAGING 1',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(24,'5433','SUPPLIER DAGING 2',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(25,'5565','SUPPLIER DAGING 3',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(26,'5432','SUPPLIER DAGING 4',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(27,'6054','SUPPLIER DAGING 5',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:54',NULL,1),(28,'2094','SUPPLIER DAGING 6',_binary '','','','',NULL,'','','Jlan Abdul Halim','Jlan Abdul Halim','',_binary '','12309090299213','ARIS RISMAWAN','Jln Abdul Halim','','','','','','',1,3,'2024-11-14 04:49:55',NULL,1),(29,'511192','PASAR CIPINANG',_binary '','','','',NULL,'','','Jln Cipinang Elok II Bana','','',_binary '\0','','','','','','','','','',1,1,'2024-11-26 04:10:09',NULL,1),(30,'0044','aaa',_binary '','','','',NULL,'','','Jalan Bali Kusuma Jaya','','',_binary '\0','','','','','','','','','',1,2,'2024-12-18 08:06:43',NULL,1),(31,'2333','aswerdd',_binary '','','','',NULL,'','','dfddfdf','dfdfdgd','',_binary '','','','','','','','','','',1,1,'2024-12-18 08:09:48',NULL,1),(32,'RM010003','PT ANZINDO GRATIA INTERNATIONAL',_binary '','021 - 29339465','','255-301-7700',NULL,'PT ANZINDO GRATIA INTERNATIONAL','BCA','Gedung Apl Tower L. 27 Unit T3. Jl Letjend S.Parmaukau 28 Tanjung Duren Selatan. Grogol Jakarta Barat','Gedung Apl Tower L. 27 Unit T3. Jl Letjend S.Parmaukau 28 Tanjung Duren Selatan. Grogol Jakarta Barat','Pergudangan Prima Center Blok B20-21 Jl. Pool Ppd Pesing Pobcar Jak-Bar',_binary '','02.125.146.7-062.000','PT ANZINDO GRATIA INTERNATIONAL','GEDUNG APL TOWER 1+ 27 UNIT TS.JL LETJEND.S.PARMANKAU 28 TANJUNG DUREN SELATAN. GROGOL JAK-BAR','albert@anzindo.co.id','8119277776','Josua','81220502454','Alex','81311019792',10,3,'2025-02-16 18:41:07',NULL,1),(33,'RM010004','PT. ASTAMULYA MANDIRI',_binary '','2163875240','salesastamulya@gmail.com','6930 883 881',NULL,'PT. ASTAMULYA MANDIRI','BCA','Mediterania Office R3 - M02. Jl. Gajah Mada No. 174, Jakarta 11130','Mediterania Office R3 - M02. Jl. Gajah Mada No. 174, Jakarta 11130','Kawasan Pergudangan Tunas Bitung Blok F7, Jl. Raya Serang KM 13,8,Kel. Sukadamai, Kec. Cikupa, Tangerang- Banten 15710',_binary '','02.189.614.7-037.000','PT. ASTAMULYA MANDIRI','APARTEMEN MEDITERANIA UNIT KIOS R3 M02. JL. GAJAHMADA, 174, KEAGUNGAN, TAMAN SARI, KOTAM ADM. JAKARTA BARAT, DKI JAKARTA 11130','salesastamulya@gmail.com','87889301283','Yohannes','87889301283','','',10,2,'2025-02-16 18:41:07',NULL,1),(34,'RM 010005','PT.ALTINDO MULIA',_binary '','021 - 6618088','salesalt@altindo.co.id','1155004007',NULL,'PT.ALTINDO MULIA','PANIN','Jl.Pluit Selatan Raya No.60 Rt 21 Rw 06 Pluit Kec Penjaringan Jakarta Utara DKI Jakarta 14450','Jl.Pluit Selatan Raya No.60 Rt 21 Rw 06 Pluit Kec Penjaringan Jakarta Utara DKI Jakarta 14450','Daan Mogot Rd No 5 Rt 001 Rw 004 Batu Ceper Tangerang City Banten 15122',_binary '','01.542.317.1-047.000','PT.ALTINDO MULIA','JL.PLUIT SELATAN RAYA NO.60 RT 021 RW 006 PLUIT PENJARINGAN KOTA ADM.JAKARTA UTARA DKI JAKARTA','salesalt@altindo.co.id','82118108626','Yoko','87742478452','Ria','85920574345',9,2,'2025-02-16 18:41:07',NULL,1),(35,'RM 010006','PT.ARTHA INTI MANUNGGAL',_binary '','021 - 29405555','aim@artha-inti.com','1495000991',NULL,'PT.ARTHA INTI MANUNGGAL','PANIN','Jl.Tanjung Pura No.3 RT 002 RW 002 Kp Maja Pegadungan Kalideres Jakarta Barat 11830','Jl.Tanjung Pura No.3 RT 002 RW 002 Kp Maja Pegadungan Kalideres Jakarta Barat 11830','Jl.Tanjung Pura No.3 RT 002 RW 002 Kp Maja Pegadungan Kalideres Jakarta Barat 11830',_binary '','02.288.023.1-085.000','PT.ARTHA INTI MANUNGGAL','PODOMORO CITY SOHO CAPITAL LT.38 JL.LETJEN S.PARMAN KAV 28 TANJUNG DUREN SELATAN GROGOL PETAMBURAN KOTA ADM.JAKARTA BARAT, DKI JAKARTA 11470','rita@artha-inti.com','83870001020','Yullius','816714154','Yana','81281939833',10,2,'2025-02-16 18:41:07',NULL,1),(36,'RM020001','PT BERKAT AMADEUS SUKSES SENTOSA',_binary '','021-80475599','berkatamadeussuksess@gmail.com','4280316054',NULL,'PT BERKAT AMADEUS SUKSES SENTOSA','BCA','Jl.Raya Cileungsi No.65B Mekarsari Cileongsi,Bogor','Jl.Raya Cileungsi No.65B Mekarsari Cileongsi,Bogor','Jl.Raya Cileungsi No 65B,Mekasari, Cilengsi, Bogor',_binary '','41.152.057.0-436.000','PT BERKAT AMADEUS SUKSES SENTOSA','JALAN RAYA CILENGSI NOMOR 65B,MERKASARI RT,OLO/RW.004','berkatamadeussuksess@gmail.com','81119925212','Doddy Supriadi','81119925203','Mita','81585030517',10,3,'2025-02-16 18:41:07',NULL,1),(37,'RM030001','PT CIPTA KEMAS GEMILANG MANDIRI',_binary '','021-29095569','pt.ckgm@gmail.com','8800 284 8888',NULL,'CIPTA KEMAS GEMILANG M P','BCA','Jl.Raya Tlajung Udik No.50 Rt.002 Rw.016 Gunung Putri,Bogor','Jl.Raya Tlajung Udik No.50 Rt.002 Rw.016 Gunung Putri,Bogor','Jl.Tlajung Udik No.50 Rt.002 Rw.016 Gunung Putri Bogor',_binary '','80.798.153.5-403.000','PT CIPTA KEMAS GEMILANG MANDIRI','JL.RAYA TLAJUNG UDIK KP.MOMONOT, 50.TLAJUNG UDIK,GUNUNG PUTI,KAB,BOGOR,JAWA BARAT,16962','sitickgm@gmail.com/pt.ckgm@gmail.com','85814901670','Siti Muhibah','85814901670','Iis Widia Ningsih','81310782045',10,2,'2025-02-16 18:41:07',NULL,1),(38,'RM040001','PT DHASS SUMBER TEKHNIK',_binary '','021-55743005','info@dhass.co.id','883 057 1010',NULL,'PT DHASS SUMBER TEKHNIK','BCA','Rukan Mahkota Mas Blok K No.28-29 Jl.MH.Thamrin Tangerang 15117','Rukan Mahkota Mas Blok K No.28-29 Jl.MH.Thamrin Tangerang 15117','',_binary '','0024-6482-3041-6000','PT DHASS SUMBER TEKHNIK','RUKAN MAHKOTA MAS BLOK E NO.35.JL.MH.THAMRIN CIKOKOL,TANGERANG,KOTA TANGERANG,BANTEN 15117','info@dhass.co.id','817797601','Budi Hianto','89634089098','Leni','89634089098',10,2,'2025-02-16 18:41:07',NULL,1),(39,'RM060002','PT.FOAMINDO ABADI',_binary '','021-8670406','foamindoabadi@yahoo.com','8690046637',NULL,'PT. FOAMINDO ABADI','BCA','Jl. Raya Wanaherang No. 284 Cicadas Kab Bogor Gunung Putri Jawa Barat 161964','Jl. Raya Wanaherang No. 284 Cicadas Kab Bogor Gunung Putri Jawa Barat 161964','Jl. Raya Wanaherang No. 284 Cicadas Kab Bogor Gunung Putri Jawa Barat 161964',_binary '','02.268.899.8-404.000','PT.FOAMINDO ABADI','Jl. Siliwangi 25 Sukasari Bogor Timur Kota Bogor 16142','foamindoabadi@yahoo.com','8988624106','Deka Shavira','8988624106','Yetty','81288190713',10,2,'2025-02-16 18:41:07',NULL,1),(40,'RM100001','PT. JINYOUNG',_binary '','217804527','admin@foodtech-jinyoung.com','800 1122 11700',NULL,'PT. JINYOUNG','CIMB Niaga','Gedung Satmarindo, Jl. Ampera Raya No. 5 , Cilandak Timur, Pasar Minggu, Jakarta Selatan 12560','Gedung Satmarindo, Jl. Ampera Raya No. 5 , Cilandak Timur, Pasar Minggu, Jakarta Selatan 12560','Jl. Pabrik Kawasan Industri Klpanunggal, Klapanunggal, Kota Bogor, Jawa Barat 16871',_binary '','02.026.584.9-052.000','PT. JINYOUNG','JL. PABRIK KAWASAN INDUSTRI KLAPANUNGGAL KM 26,, KLAPANUNGGAL, KLAPANUNGGAL, KAB.BOGOR, JAWA BARAT 16871','admin@foodtech-jinyoung.com','8118600633','Jordan Pelis','8118600633','Trimulyati','218234565',10,2,'2025-02-16 18:41:07',NULL,1),(41,'RM110001','PT. KARTIKA EKA DHARMA',_binary '','215851422','marketing@ayamkartika.com','1545001992',NULL,'PT. KARTIKA EKA DHARMA','PANIN','JL. Swadharma Raya No. 69 Srengseng, Jakarta Barat, 11630','JL. Swadharma Raya No. 69 Srengseng, Jakarta Barat, 11630','Jl. Swadharma Raya No, 69 Srengseng, Jakarta Barat 11630',_binary '','01.307.276.4-073.000','PT. KARTIKA EKA DHARMA','JL. SWADHARMA RAYA NO, 69 SRENGSENG, JAKARTA BARAT 11630','marketing@ayamkartika.com','08119008683, 087748319330','Maelina','81317901955','Rina','8128483577',10,2,'2025-02-16 18:41:07',NULL,1),(42,'RM130002','PT. MACROTAMA BINASANTIKA',_binary '','215874630','nyoman.wijaya@cimory.com','288 - 3043 -111',NULL,'PT. MACROTAMA BINASANTIKA','BCA','Rukan Taman Meruya N 27 - 28 Meruya, Jakarta 11620','Rukan Taman Meruya N 27 - 28 Meruya, Jakarta 11620','Pergudangan Bizlik Blok O.1/32, Citra Raya, Cikupa - Tangerang',_binary '','01.333.864.5-038.000','PT. MACROTAMA BINASANTIKA','RUKAN TAMAN MERUYA, MERUYA UTARA, KEMBANGAN, KOTA ADM JAKARTA BARAT, DKI JAKARTA 11620','nyoman.wiajay@cimory.com','87782511474','Nyoman Wijaya','87782511474','Lia Alida','215874630',12,2,'2025-02-16 18:41:07',NULL,1),(43,'RM160001','PT PRIMA SAMBARA PERSADA',_binary '','021-5552159','marketing@primasambara.com','126 55 77 999',NULL,'PT PRIMA SAMBARA PERSADA','PANIN','Jl Pantai Indah Utara2,A,28 Kapuk Muara ,Perjaringan,Jakarta Utara','Jl Pantai Indah Utara2,A,28 Kapuk Muara ,Perjaringan,Jakarta Utara','Pergudangan Miami,Jl Kayu Besar 9 Blok G No 4,Jakarta Barat',_binary '','81.584.976.5-047.000','PT PRIMA SAMBARA PERSADA','JL PANTAI INDAH UTARA 2,A,28 KAPUK MUARA,PENJARINGAN,JAKARTA UTARA','marketing.pcr1@gmail.com','87787150003','Vega Ai Sahra Putri Harnanto','89654378252','Devi Ana Safitri','81384912800',10,2,'2025-02-16 18:41:07',NULL,1),(44,'RM160002','CV PUTRA PERDANA',_binary '','21877116105','putraperdanachicken@yahoo.co.id','291 589 5555',NULL,'CV PUTRA PERDANA','BCA','Jl. IR. H Juanda No. 37, Kel. Baktijaya, Kec. Sukmajaya Depok','Jl. IR. H Juanda No. 37, Kel. Baktijaya, Kec. Sukmajaya Depok','MGM Bosco Jati Asih Jl. Satopati No. 77 Rt 004, RW 002, Bantar Gebang, Bekasi',_binary '','0620-8437-8941-20.00','CV PUTRA PERDANA','BUKIT CENGKEH 2 BLOK I 10 NO 3 RT 10 RW 16, KEL.TUGU KEC.CIMANGGIS','putraperdanachicken@yahoo.co.id','85694299524','Syifa','85694299524','Syifa','85694299524',10,3,'2025-02-16 18:41:07',NULL,1),(45,'RM160003','PT. PINTU MAS MULIA KIMIA',_binary '','021-3440862','','4840 - 148551',NULL,'PT. PINTU MAS MULIA KIMIA','BCA','Jl. Tanah Abang III No. 7, Jakarta Pusat 10160','Jl. Tanah Abang III No. 7, Jakarta Pusat 10160','Balaraja, Tangerang',_binary '','01.317.582.3-073.000','PT. PINTU MAS MULIA KIMIA','JL. TANAH ABANG III NO 7, PETOKO SELATAN, GAMBIR, JAKARTA PUSAT 10160','florensia.swastika@pmmk.com, timothy.garry@pmmk.com','81331202280','Timothy Garry','87771060858','Suharti Wijayanti','213440862',10,2,'2025-02-16 18:41:07',NULL,1),(46,'RM180001','PT. RAJA JEVA NISI',_binary '','','','001 738 3039',NULL,'PT. RAJA JEVA NISI','BCA','BSD Sektor I No. 6, Rawa Buntu, Serpong, Tangerang Selatan','BSD Sektor I No. 6, Rawa Buntu, Serpong, Tangerang Selatan','',_binary '','02.909.462.0-411.000','PT. RAJA JEVA NISI','BSD SEKTOR I NP 6, RAWA BUNTU, SERPONG - TANGERANG','admin.sales@rpanusantara.id','81219851001','Ayub Hendradi','81219851001','Selfi Pusparini','85716377357',10,2,'2025-02-16 18:41:07',NULL,1),(47,'RM230001','UD.WALUYO',_binary '','0217564818','rpa.udwaluyo@gmail.com','497 - 1444 - 694',NULL,'EKO DJIANTO PUTRO','BCA','Jl. Ciater Raya No. 1 Serpong','Jl. Ciater Raya No. 1 Serpong','Jl. Ciater Raya No. 1 Serpong',_binary '','678497140086000','UD.WALUYO','MEGA KEBON JERUK - JAKARTA BARAT','rpa.udwaluyo@gmail.com','81281949004','Ayu Nilawati','81281949004','Nadia Hikmah','81316831336',10,2,'2025-02-16 18:41:07',NULL,1);
/*!40000 ALTER TABLE `tbsupplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsuppliercatalog`
--

DROP TABLE IF EXISTS `tbsuppliercatalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsuppliercatalog` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `supplier` bigint NOT NULL,
  `raw_material` bigint NOT NULL,
  `currency` bigint NOT NULL,
  `price` decimal(19,4) DEFAULT NULL,
  `alternative_name` varchar(128) DEFAULT NULL,
  `coding` varchar(128) DEFAULT NULL,
  `packaging_weight` decimal(19,4) DEFAULT NULL,
  `selectable` bit(1) DEFAULT NULL,
  `last_modified_by` varchar(64) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-supplierforcatalog` (`supplier`),
  KEY `fk-currencyforsuppliercatalog` (`currency`),
  KEY `fk-rmforcatalog` (`raw_material`),
  CONSTRAINT `fk-currencyforsuppliercatalog` FOREIGN KEY (`currency`) REFERENCES `tbcurrency` (`id`),
  CONSTRAINT `fk-rmforcatalog` FOREIGN KEY (`raw_material`) REFERENCES `tbrawmaterials` (`id`),
  CONSTRAINT `fk-supplierforcatalog` FOREIGN KEY (`supplier`) REFERENCES `tbsupplier` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsuppliercatalog`
--

LOCK TABLES `tbsuppliercatalog` WRITE;
/*!40000 ALTER TABLE `tbsuppliercatalog` DISABLE KEYS */;
INSERT INTO `tbsuppliercatalog` VALUES (1,1,2,1,50000.0000,'AYAM','',NULL,_binary '',NULL,'2024-11-14 04:51:12'),(2,1,3,1,50000.0000,'SAPI','',NULL,_binary '',NULL,'2024-11-14 04:51:12'),(3,1,4,1,50000.0000,'BABI','',NULL,_binary '',NULL,'2024-11-14 04:51:12'),(4,2,1,1,50000.0000,'GARAM','',NULL,_binary '',NULL,'2024-11-14 04:51:27'),(5,4,13,1,50000.0000,'SERIVECE','',NULL,_binary '','admin','2025-01-08 04:02:33'),(6,3,2,1,50000.0000,'AYAM','',NULL,_binary '','admin','2025-01-08 04:02:07'),(7,3,3,1,50000.0000,'SAPI','',NULL,_binary '','admin','2025-01-08 04:02:07'),(8,3,4,1,50000.0000,'BABI','',NULL,_binary '','admin','2025-01-08 04:02:07'),(9,3,1,1,50000.0000,'GARAM','',NULL,_binary '','admin','2025-01-08 04:02:07'),(10,29,2,1,65000.0000,'AYAM','',NULL,_binary '','admin','2024-11-26 04:19:24'),(11,29,3,1,70000.0000,'SAPI','',NULL,_binary '','admin','2024-11-26 04:19:24'),(12,29,4,1,65200.0000,'BABI','',NULL,_binary '','admin','2024-11-26 04:19:24'),(13,29,1,1,73250.0000,'GARAM','',NULL,_binary '','admin','2024-11-26 04:19:24'),(14,29,14,1,55000.0000,'BOMBAY','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(15,29,15,1,45000.0000,'SAGU GUNUNG','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(16,29,16,1,32000.0000,'BAWANG MERAH','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(17,29,17,1,25500.0000,'MECIN','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(18,29,18,1,65500.0000,'MINYAK KITA','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(19,29,19,1,22200.0000,'MINYAK RIZKI','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(20,29,20,1,22200.0000,'KULIT AYAM','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(21,29,21,1,65000.0000,'KULIT MANUSIA','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(22,29,22,1,20200.0000,'KULIT SINGA','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(23,29,23,1,25000.0000,'KULIT BADAK','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(24,29,24,1,45000.0000,'CABAI','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(25,29,25,1,55000.0000,'BLACK PAPAER','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(26,29,26,1,50000.0000,'BIJI WIJEN','',NULL,_binary '',NULL,'2024-11-26 04:19:24'),(27,3,20,1,20200.0000,'KULIT AYAM','',NULL,_binary '','admin','2025-01-08 04:02:07'),(28,3,21,1,65000.0000,'KULIT MANUSIA','',NULL,_binary '','admin','2025-01-08 04:02:07'),(39,3,14,1,55000.0000,'BOMBAY','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(40,3,15,1,45000.0000,'SAGU GUNUNG','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(41,3,16,1,32000.0000,'BAWANG MERAH','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(42,3,17,1,25000.0000,'MECIN','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(43,3,18,1,65000.0000,'MINYAK KITA','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(44,3,19,1,22000.0000,'MINYAK RIZKI','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(45,3,22,1,20200.0000,'KULIT SINGA','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(46,3,23,1,25000.0000,'KULIT BADAK','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(47,3,24,1,45000.0000,'CABAI','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(48,3,25,1,55000.0000,'BLACK PAPAER','',NULL,_binary '',NULL,'2025-01-08 04:02:07'),(49,3,26,1,50000.0000,'BIJI WIJEN','',NULL,_binary '',NULL,'2025-01-08 04:02:07');
/*!40000 ALTER TABLE `tbsuppliercatalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsuppliercategory`
--

DROP TABLE IF EXISTS `tbsuppliercategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsuppliercategory` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `supplier` bigint NOT NULL,
  `raw_materials_category` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-rmcforsc` (`raw_materials_category`),
  KEY `fk-supplierforsc` (`supplier`),
  CONSTRAINT `fk-rmcforsc` FOREIGN KEY (`raw_materials_category`) REFERENCES `tbrawmaterialscategory` (`id`),
  CONSTRAINT `fk-supplierforsc` FOREIGN KEY (`supplier`) REFERENCES `tbsupplier` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsuppliercategory`
--

LOCK TABLES `tbsuppliercategory` WRITE;
/*!40000 ALTER TABLE `tbsuppliercategory` DISABLE KEYS */;
INSERT INTO `tbsuppliercategory` VALUES (1,1,1),(2,2,2),(3,3,1),(4,3,2),(5,4,7),(6,5,3),(7,6,4),(8,7,5),(9,8,6),(10,9,7),(11,10,7),(12,10,6),(13,11,1),(14,11,2),(15,11,3),(16,12,1),(17,13,1),(18,14,1),(19,15,1),(20,16,1),(21,17,1),(22,18,1),(23,19,1),(24,20,1),(25,21,1),(26,22,1),(27,23,1),(28,24,1),(29,25,1),(30,26,1),(31,27,1),(32,28,1),(33,29,1),(34,29,2),(35,30,1),(36,31,1),(37,32,1),(38,33,2),(39,34,5),(40,35,5),(41,36,1),(42,37,5),(43,38,5),(44,39,4),(45,40,2),(46,41,1),(47,42,3),(48,43,2),(49,44,1),(50,45,2),(51,46,1),(52,47,1);
/*!40000 ALTER TABLE `tbsuppliercategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsupplierpic`
--

DROP TABLE IF EXISTS `tbsupplierpic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsupplierpic` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `supplier` bigint NOT NULL,
  `pic_name` varchar(250) DEFAULT NULL,
  `pic_position` varchar(250) DEFAULT NULL,
  `pic_landline` varchar(250) DEFAULT NULL,
  `pic_mobile_phone` varchar(250) DEFAULT NULL,
  `pic_email` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-supplier` (`supplier`),
  CONSTRAINT `fk-supplier` FOREIGN KEY (`supplier`) REFERENCES `tbsupplier` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsupplierpic`
--

LOCK TABLES `tbsupplierpic` WRITE;
/*!40000 ALTER TABLE `tbsupplierpic` DISABLE KEYS */;
INSERT INTO `tbsupplierpic` VALUES (1,1,'Aris Rismawan','','089898898221','',''),(2,2,'Aris Rismawan','','089898898222','',''),(3,3,'Aris Rismawan','','089898898223','',''),(4,4,'Aris Rismawan','','08989889824','',''),(5,5,'Aris Rismawan','','089898898225','',''),(6,6,'Aris Rismawan','','089898898226','',''),(7,7,'Aris Rismawan','','089898898227','',''),(8,8,'Aris Rismawan','','089898898228','',''),(9,9,'Aris Rismawan','','089898898229','',''),(10,10,'Aris Rismawan','','089898898210','',''),(11,11,'Aris Rismawan','','089898898211','',''),(12,12,'Aris Rismawan','','089898898212','',''),(13,13,'Aris Rismawan','','089898898213','',''),(14,14,'Aris Rismawan','','089898898213','',''),(15,15,'Aris Rismawan','','089898898213','',''),(16,16,'Aris Rismawan','','089898898213','',''),(17,17,'Aris Rismawan','','089898898213','',''),(18,18,'Aris Rismawan','','089898898213','',''),(19,19,'Aris Rismawan','','089898898213','',''),(20,20,'Aris Rismawan','','089898898213','',''),(21,21,'Aris Rismawan','','089898898213','',''),(22,22,'Aris Rismawan','','089898898213','',''),(23,23,'Aris Rismawan','','089898898213','',''),(24,24,'Aris Rismawan','','089898898213','',''),(25,25,'Aris Rismawan','','089898898213','',''),(26,26,'Aris Rismawan','','089898898213','',''),(27,27,'Aris Rismawan','','089898898213','',''),(28,28,'Aris Rismawan','','089898898213','',''),(29,29,'ARIS RISMAWAN','','08997397333','',''),(30,30,'ARIS RISMAWAN','','0890978968966','',''),(31,31,'sdfsdfsdfs','','456456','',''),(32,32,'Ricky Wangidjaja','Direktur','8119277776','8119277776','James@anzindo.co.id'),(33,33,'Mulia Santoso Mandiro','Direktur','8159004362','8159004362','mmandiro@gmail.com'),(34,34,'Terismin','Direktur','82118108626','82118108626','salesalt@altindo.co.id'),(35,35,'Yullius Rinoldy Tjhin','Direktur','816714154','816714154','yullius@artha-inti.com'),(36,36,'Angga Kusumah','Direktur Utama','81119925203','81119925203',''),(37,37,'Fikry Haznul','Direktur','81290590870','81290590870','fhaznul@gmail.com'),(38,38,'Andreas Timoti','Direktur Utama','021-55743007','021-55743007','info@dhass.co.id'),(39,39,'Robertus Johan Wijaya','Direktur Utama','87878537899','87878537899',''),(40,40,'Lee Jin Ho','President Director','217804527','217804527','jinyoung@foodtech-jinyoung.com'),(41,41,'Djuandri Bunadi','Direktur','215851422','215851422','accounting@ayamkartika.com'),(42,42,'Bambang Sutantio','Direktur','215874630','215874630',''),(43,43,'Yenny Sutanto','Direktur','81932032408','81932032408','jennysutanto21@gmail.com'),(44,44,'Jessica Sulis Setyaningsih','Direktur','81317259646','81317259646','putraperdanachicken@yahoo.co.id'),(45,45,'Emil Unaja','Direktur','021-3440862','021-3440862','emil.unaja@pmmk.com'),(46,46,'Arifin','Direktur','81348088488','81348088488','arifien@rajajevanisi.id'),(47,47,'KO Djianto Putro','Pimpinan','8161822791','8161822791','ekodjiantoputro@gmail.com');
/*!40000 ALTER TABLE `tbsupplierpic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsuppliersfile`
--

DROP TABLE IF EXISTS `tbsuppliersfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsuppliersfile` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `supplier` bigint NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `file` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-rmfiletofile` (`file`),
  KEY `fk-supplierrelated` (`supplier`),
  CONSTRAINT `fk-rmfiletofile` FOREIGN KEY (`file`) REFERENCES `tbfile` (`id`),
  CONSTRAINT `fk-supplierrelated` FOREIGN KEY (`supplier`) REFERENCES `tbsupplier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsuppliersfile`
--

LOCK TABLES `tbsuppliersfile` WRITE;
/*!40000 ALTER TABLE `tbsuppliersfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbsuppliersfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbsysprop`
--

DROP TABLE IF EXISTS `tbsysprop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbsysprop` (
  `name` varchar(128) NOT NULL,
  `value` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbsysprop`
--

LOCK TABLES `tbsysprop` WRITE;
/*!40000 ALTER TABLE `tbsysprop` DISABLE KEYS */;
INSERT INTO `tbsysprop` VALUES ('dashboard.default','DEFAULT'),('default.dateformat','dd/MM/yyyy'),('default.invoice.days','[\"MONDAY\", \"TUESDAY\", \"WEDNESDAY\", \"THURSDAY\", \"FRIDAY\"]'),('default.invoice.lockolderthan','3'),('default.invoice.offset','1'),('default.taxpercent','11 %'),('efaktur.seq','0'),('invoice.draft.invoiceCode.sequence','0'),('kode.dokumen.kerja.kenek','0'),('material.release.batch.prefix','MR-Batch-'),('material.release.document.prefix','MR-Doc-'),('material.request.prefix','S-'),('organisation.addressLine1','Jl Cipinang Elok Pertama No.16A'),('organisation.addressLine2','Cipinang Muara, Jakarta 13420'),('organisation.fax','(021) 867 3422'),('organisation.logo','https://sosisdelikatessa.com/wp-content/themes/sosis/images/asdddd.png'),('organisation.name','CV SICMA'),('organisation.phone','(021) 867 2949, 867 3326'),('purchase.invoice.draft.prefix','DRAFT-INV-'),('purchase.invoice.draft.sequence','001'),('purchase.invoice.exchange.prefix','TF'),('purchase.invoice.prefix','S-'),('purchase.invoice.sequence','0000013'),('purchase.order.prefix','S-'),('purchase.order.sequence','0000026'),('purchase.request.prefix','S-'),('purchase.request.sequence','0000028'),('root.location','.');
/*!40000 ALTER TABLE `tbsysprop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtop`
--

DROP TABLE IF EXISTS `tbtop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbtop` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtop`
--

LOCK TABLES `tbtop` WRITE;
/*!40000 ALTER TABLE `tbtop` DISABLE KEYS */;
INSERT INTO `tbtop` VALUES (1,'Default - by Invoice Date','Offset according to Invoice Date',_binary ''),(2,'Fixed Next Month','Fixed Date of Month After Invoice',_binary ''),(3,'By Tukar Faktur - Absolute','Absolute Days After Tanggal Tukar Faktur',_binary ''),(4,'By Tukar Faktur - Cycle','Cycled by Tanggal Tukar Faktur',_binary ''),(5,'7DF - BY SUPPLIER DATE','7DF',_binary ''),(6,'BD','BEFORE DELIVERY',_binary ''),(7,'OD','ON DELVIERY',_binary ''),(8,'7 DF','7 HARI DARI TANGGAL PENERIMAAN',_binary ''),(9,'14 DF','14 HARI DARI TANGGAL PENERIMAAN',_binary ''),(10,'30 DF','30 HARI DARI TANGGAL PENERIMAAN',_binary ''),(11,'45 DF','45 HARI DARI TANGGAL PENERIMAAN',_binary ''),(12,'60 DF','60 HARI DARI TANGGAL PENERIMAAN',_binary ''),(13,'7 DTF','7 HARI DARI TANGGAL TUKAR FAKTUR',_binary ''),(14,'14 DTF','14 HARI DARI TANGGAL TUKAR FAKTUR',_binary ''),(15,'30 DTF','30 HARI DARI TANGGAL TUKAR FAKTUR',_binary ''),(16,'45 DTF','45 HARI DARI TANGGAL TUKAR FAKTUR',_binary ''),(17,'60 DTF','60 HARI DARI TANGGAL TUKAR FAKTUR',_binary '');
/*!40000 ALTER TABLE `tbtop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtruck`
--

DROP TABLE IF EXISTS `tbtruck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbtruck` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `truck_code` varchar(64) DEFAULT NULL,
  `number_plate` varchar(64) DEFAULT NULL,
  `number_plate_expiry` date DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  `periodic_inspection_expiry` date DEFAULT NULL,
  `delivery_color_code` bigint DEFAULT NULL,
  `delivery_area` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-truck-delivery-color-code` (`delivery_color_code`),
  KEY `fk-truck-delivery-area` (`delivery_area`),
  CONSTRAINT `fk-truck-delivery-area` FOREIGN KEY (`delivery_area`) REFERENCES `tbdeliveryarea` (`id`),
  CONSTRAINT `fk-truck-delivery-color-code` FOREIGN KEY (`delivery_color_code`) REFERENCES `tbdeliverycolorcode` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtruck`
--

LOCK TABLES `tbtruck` WRITE;
/*!40000 ALTER TABLE `tbtruck` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbtruck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbuser`
--

DROP TABLE IF EXISTS `tbuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbuser` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `enabled` bit(1) DEFAULT b'0',
  `dashboard_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbuser`
--

LOCK TABLES `tbuser` WRITE;
/*!40000 ALTER TABLE `tbuser` DISABLE KEYS */;
INSERT INTO `tbuser` VALUES (1,'SICMA Admin','admin','admin@kreasisentra.com','$2a$10$8n7Q3J/frlBWpjucaAa0duJhLNnqXocyQmPQDuqpHT7vpYifIdTM2',_binary '',NULL);
/*!40000 ALTER TABLE `tbuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbuserrole`
--

DROP TABLE IF EXISTS `tbuserrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbuserrole` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `userid` bigint NOT NULL,
  `roleid` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-role` (`roleid`),
  KEY `fk-user` (`userid`),
  CONSTRAINT `fk-role` FOREIGN KEY (`roleid`) REFERENCES `tbrole` (`id`),
  CONSTRAINT `fk-user` FOREIGN KEY (`userid`) REFERENCES `tbuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbuserrole`
--

LOCK TABLES `tbuserrole` WRITE;
/*!40000 ALTER TABLE `tbuserrole` DISABLE KEYS */;
INSERT INTO `tbuserrole` VALUES (1,1,1);
/*!40000 ALTER TABLE `tbuserrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbwarehousefacility`
--

DROP TABLE IF EXISTS `tbwarehousefacility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbwarehousefacility` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `organisation_id` bigint DEFAULT NULL,
  `point_type` int DEFAULT NULL,
  `prefix` varchar(16) DEFAULT NULL,
  `sequence` int DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk-facility-organisation` (`organisation_id`),
  CONSTRAINT `fk-facility-organisation` FOREIGN KEY (`organisation_id`) REFERENCES `tbmasterorganisation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbwarehousefacility`
--

LOCK TABLES `tbwarehousefacility` WRITE;
/*!40000 ALTER TABLE `tbwarehousefacility` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbwarehousefacility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbwarehouselocation`
--

DROP TABLE IF EXISTS `tbwarehouselocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbwarehouselocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `facility` bigint DEFAULT NULL,
  `area` varchar(250) DEFAULT NULL,
  `aisles` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `quarantine` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `fk-forwhatfacility` (`facility`),
  CONSTRAINT `fk-forwhatfacility` FOREIGN KEY (`facility`) REFERENCES `tbwarehousefacility` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbwarehouselocation`
--

LOCK TABLES `tbwarehouselocation` WRITE;
/*!40000 ALTER TABLE `tbwarehouselocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbwarehouselocation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-24  2:48:58
